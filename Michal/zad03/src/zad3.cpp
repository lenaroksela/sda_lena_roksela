#include <iostream>

// Je�eli argument jest 0 zwr�� 999, w przeciwnym wypadku zwr�� warto��
int foo(int nValue)
{
    if (nValue == 0)
        return 999;
    else
        return nValue;
}

int main()
{
    std::cout << foo(0) << std::endl;
    std::cout << foo(1) << std::endl;
    std::cout << foo(2) << std::endl;

    return 0;
}
