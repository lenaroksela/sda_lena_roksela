#include "board.hpp"
#include "draw.hpp"
#include "globals.hpp"
#include "gobject.hpp"
#include "score.hpp"

#include <iostream>
#include <cstdlib>

void printChar(char ch)
{
	std::cout << ch;
}

void drawNewLine()
{
	printChar('\n');
}

void drawBorderVertical(const board *board)
{
	for (int i = 0; i < board->border_thickness; ++i)
	{
		printChar(board->symbol);
	}
}

void drawBorderHorizontal(const board *board)
{
	for (int i = 0; i < board->border_thickness; ++i)
	{
		drawBorderVertical(board);

		for (int j = 0; j < board->length; ++j)
		{
			printChar(board->symbol);
		}

		drawBorderVertical(board);
		drawNewLine();
	}
}

void clear()
{
	system("clear");
}

void draw(const board *board, const gobject *snake, const gobject *fruit,
		const score *score)
{
	position pos;
	char ch;
	clear();

	drawBorderHorizontal(board);

	for (pos.y = 0; pos.y < board->width; ++pos.y)
	{
		drawBorderVertical(board);

		for (pos.x = 0; pos.x < board->length; ++pos.x)
		{
			if (gobject_occupying(fruit, &pos))
			{
				ch = fruit->symbol;
			}
			else if (gobject_occupying(snake, &pos))
			{
				ch = snake->symbol;
			}
			else
			{
				ch = ' ';
			}
			printChar(ch);
		}

		drawBorderVertical(board);
		drawNewLine();
	}

	drawBorderHorizontal(board);

	drawNewLine();

	std::cout<<"Wynik: "<< score_current(score);
	drawNewLine();
}
