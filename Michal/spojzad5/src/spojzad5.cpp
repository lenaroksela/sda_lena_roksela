#include <iostream>
using namespace std;

int main()
{

	int t;
	cin >> t;
	string slowo;
	string polslowa;

	if (t >= 1 && t <= 100)
	{

		for (int i = 0; i < t; i++)
		{
			cin >> slowo;

			if ((slowo.size() % 2) == 0)
			{

				for (unsigned int k = 0; k < (slowo.size() / 2); k++)
				{
					polslowa += slowo[k];
				}

				cout << polslowa << endl;
				polslowa = "";
			}
		}
	}
	return 0;
}
