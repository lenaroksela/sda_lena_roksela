#include <iostream>
using namespace std;

int main() {

	int t;
	cin >> t;

	int n;

	for (int i = 0; i < t; i++)
	{
		cin >> n;
		int tab[n];

		for (int k = 0; k < n; k++)
		{
			cin >> tab[k];
		}

		for (int j = n - 1; j >= 0; j--)
		{
			cout << tab[j] << endl;
		}
	}

	return 0;
}
