

#include <iostream>

int silnia(int n) {

	if (n == 1) {
		return 1;
	}
	return n * silnia(n - 1);
}

int main() {

	int n;
	std::cout << "Podaj n: " << std::endl;
	std::cin >> n;
	std::cout << silnia(n) << std::endl;

	return 0;
}
