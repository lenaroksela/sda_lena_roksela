#include <iostream>
#include <string>

using namespace std;

class Produkt
{
public:
	Produkt(float cena, string nazwa) :
			mCena(cena), mNazwa(nazwa)
	{

	}

	float mCena;
	string mNazwa;

	void wypisz()
	{
		cout << mNazwa << " " << mCena << endl;
	}

};

Produkt stworz(int klasa, string nazwa)
{
	switch (klasa)
	{
	case 1:
	{
		return Produkt(10.99f, nazwa);
		break;
	}

	case 2:
	{
		return Produkt(500.99f, nazwa);
		break;
	}

	case 3:
	{
		return Produkt(19.99f, nazwa);
		break;
	}
	default:
	{
		return Produkt(0, "zlom");
		break;
	}
	}
}

int main()
{

	Produkt nowy = stworz(1, "zabawka");
	nowy.wypisz();

	Produkt nowy2 = stworz(2, "noz");
	nowy2.wypisz();

	Produkt nowy3 = stworz(3, "kotek");
	nowy3.wypisz();

	return 0;
}
