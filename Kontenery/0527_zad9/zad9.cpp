#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <cmath>
#include <numeric>

using namespace std;

bool jestParzysta(int x)
{
	return x % 2 != 0;
}

bool jestTrzy(int x)
{
	return x % 3 != 0;
}

vector<int> vectorZliczbami(int n)
{
	vector<int> vector;
	for (int i = 0; i < n; i++)
	{
		vector.push_back(i + 1);
	}
	return vector;
}

int main()
{

	vector<int> vectorliczb = vectorZliczbami(20);
	vector<int> tylkoDwa(10);
	vector<int> tylkoTrzy(6);
	vector<int> dwaTrzy(tylkoDwa.size()<tylkoTrzy.size()?tylkoTrzy.size():tylkoDwa.size());

	for (unsigned int i = 0; i < vectorliczb.size(); i++)
	{
		cout << vectorliczb[i] << " ";
	}
	cout << endl;

	remove_copy_if(vectorliczb.begin(), vectorliczb.end(), tylkoDwa.begin(),jestParzysta);

	for (unsigned int i = 0; i < tylkoDwa.size(); i++)
	{
		cout << tylkoDwa[i] << " ";
	}
	cout << endl;

	remove_copy_if(vectorliczb.begin(), vectorliczb.end(), tylkoTrzy.begin(),jestTrzy);
	for (unsigned int i = 0; i < tylkoTrzy.size(); i++)
	{
		cout << tylkoTrzy[i] << " ";
	}
	cout << endl;

	vector<int>::iterator it = set_intersection(tylkoDwa.begin(),tylkoDwa.end(), tylkoTrzy.begin(), tylkoTrzy.end(),dwaTrzy.begin());
	dwaTrzy.resize(distance(dwaTrzy.begin(),it));


	for (unsigned int i = 0; i < dwaTrzy.size(); i++)
	{
		cout << dwaTrzy[i] << " ";
	}
	cout << endl;

	return 0;
}
