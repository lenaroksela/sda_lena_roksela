#include <list>
#include <string>
#include <iostream>
#include <algorithm>
#include <ctime>
#include <cstdlib>

#include "klasy_zad22.hpp"

using namespace std;

void wypisz(Animal* animal)
{
	animal->whoAreYou();
}

void usun(Animal *  animal)
{
	delete animal;
}
int main()
{
	srand(time(NULL));

	list<Animal*> listaZwierzakow;

	int sn = 1;
	for (int i = 0; i < 20; i++)
	{

		int x = rand() % 2 + 1;
		if (x == 1)
		{

			listaZwierzakow.push_back(new Dog(sn));
		}
		else if (x == 2)
		{

			listaZwierzakow.push_back(new Bird(sn));
		}
		sn++;
	}

	for_each(listaZwierzakow.begin(), listaZwierzakow.end(), wypisz);


	list<Dog*> listDog;
	list<Bird*> listBird;

	Dog *pies;

	for(list<Animal*>::iterator it = listaZwierzakow.begin();it!=listaZwierzakow.end();it++)
	{
		pies = dynamic_cast<Dog*>(*it);
		if(pies!=0)
		{
			listDog.push_back(dynamic_cast<Dog*>(*it));
		}
		else
		{
			listBird.push_back(dynamic_cast<Bird*>(*it));
		}
	}

	cout<<"Psy\n";
	for_each(listDog.begin(), listDog.end(), wypisz);

	cout<<"Ptaki\n";
	for_each(listBird.begin(), listBird.end(), wypisz);

	for_each(listaZwierzakow.begin(),listaZwierzakow.end(),usun);

	return 0;
}
