/*
 * klasy_zad22.hpp
 *
 *  Created on: 31.05.2017
 *      Author: RENT
 */

#ifndef KLASY_ZAD22_HPP_
#define KLASY_ZAD22_HPP_
#include <string>
#include <iostream>

using namespace std;

class Animal
{
protected:

	 int mSerialNr;

public:
	Animal(int x)
:mSerialNr(x)
	{


	}

	virtual void whoAreYou()
	{

		cout << "Animal" << endl;

	}
	int getSerialNumber() const
	{
		return mSerialNr;
	}

	virtual ~Animal()
	{

	}


};

class Bird: public Animal
{
private:


public:

	Bird(int x)
	:Animal(x)
	{

	}

	virtual void whoAreYou()
	{
		cout << "Bird. ";
		cout << "SN:" << getSerialNumber()<<endl;
	}
	int getSerialNumber() const
	{
		return mSerialNr;
	}

};

class Dog: public Animal
{

public:

	Dog(int x)
:Animal(x)

	{
	}

	virtual void whoAreYou()
	{

		cout << "Dog. ";
		cout << "SN:" << getSerialNumber()<<endl;
	}
	int getSerialNumber() const
	{
		return mSerialNr;
	}

};
#endif /* KLASY_ZAD22_HPP_ */
