#include <list>
#include <string>
#include <iostream>
#include <algorithm>

using namespace std;

bool porownaj(string & first, string&second)
{
	return first > second;
}
void policzSlowo(list<string>& lista)
{
	lista.sort();
	int tmp = 1;

	list<string>::iterator im;
	for (list<string>::iterator it = lista.begin(); it != lista.end(); it++)
	{
		im = it;
		im++;
		if (*it == *im)
		{
			tmp += 1;
			continue;
		}
		cout << "Slowo [" << *it << "] jest " << tmp << endl;
		tmp = 1;
	}

}

list<string> naListe(string& slowo)
{
	list<string> listaSlow;
	string tmp;
//while od  find pos
	int k = count(slowo.begin(), slowo.end(), ' ') + 1;
	for (int i = 0; i < k; i++)
	{
		int pos = slowo.find(' ');
		tmp = slowo.substr(0, pos);
		slowo.erase(0, pos + 1);
		listaSlow.push_back(tmp);
	}
	return listaSlow;
}

void usunKoniec(string & slowo)
{
	while (slowo[slowo.size() - 1] == ' ')
	{
		slowo.erase(slowo.end() - 1);
	}
}

void usunPoczatek(string& slowo)
{
	while (slowo[0] == ' ')
	{
		slowo.erase(slowo.begin());
	}
}

void usunDwieSpacje(string & slowo)
{
	for (size_t i = 0; i < slowo.size(); i++)
	{
			if (slowo[i] == ' ' && slowo[i] == slowo[i + 1])
			{

				slowo.erase(i, 1);
				i--;
			}
	}
}

void usunWszystkieSpacje(string & slowo)
{
	usunKoniec(slowo);
	usunPoczatek(slowo);
	usunDwieSpacje(slowo);
}

int main()
{

	string slowo = "  Kot na   klawiaturze kot kot ";
	cout << slowo << endl;
	usunWszystkieSpacje(slowo);

	cout << slowo << endl;
	list<string> lista = naListe(slowo);

	for (list<string>::iterator it = lista.begin(); it != lista.end(); it++)
	{
		cout << *it << endl;
	}

	policzSlowo(lista);

//	for(list<string>::iterator it=lista.begin();it!=lista.end();it++)
//		{
//			cout<<*it<<endl;
//		}

	return 0;
}

