
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>

using namespace std;

int op_increase(int i)
{
	return ++i;
}

bool iftrue(char first, char second)
{
	return first == second;
}
bool czyPalindrom(string & slowo)
{
	bool zmienna=true;
	vector<char> napis;
	for (size_t i = 0; i < slowo.size(); i++)
	{
		napis.push_back(slowo[i]);
	}


	vector<char>::reverse_iterator irt = napis.rbegin();

	for(vector<char>::iterator it=napis.begin(); it!=napis.end();it++,irt++)
	{
		if(*it!=*irt)
		{
			zmienna=false;
		}

	}

	return zmienna;

}

int main()
{

	string slowo = "kajak";

	if(czyPalindrom(slowo))
	{
		cout<<"tak";
	}
	else
	{
		cout<<"Nie";
	}
	return 0;
}
