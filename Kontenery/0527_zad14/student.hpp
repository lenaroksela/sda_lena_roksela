/*
 * student.hpp
 *
 *  Created on: 29.05.2017
 *      Author: RENT
 */

#ifndef STUDENT_HPP_
#define STUDENT_HPP_

#include <iostream>
#include <string>
#include <list>
#include "godzina.hpp"

using namespace std;



bool sortDniTygodniaDwa(Kurs& first, Kurs& second)
{
	return first.mDzien.getDz() > second.mDzien.getDz() ? false : true;
}


class Student
{
private:
	string mImie;
	list<Kurs> mListaKursow;
	unsigned int mIloscKursow;
public:

	Student(string imie, int ilosc) :
			mImie(imie), mListaKursow(), mIloscKursow(ilosc)
	{

	}

	Student() :
			mImie(), mListaKursow(), mIloscKursow()
	{

	}

	unsigned int getIloscKursow() const
	{
		return mIloscKursow;
	}

	const string& getImie() const
	{
		return mImie;
	}

	const list<Kurs>& getListaKursow() const
	{
		return mListaKursow;
	}

	void dodajKurs(Kurs & kurs)
	{
		mListaKursow.push_back(kurs);
	}

	void pokazStudenta()
	{
		cout << "Imie: " << getImie() << " ma kursow " << getIloscKursow()
				<< " : ";
		pokazKursy();
	}

	void pokazKursy()
	{
		mListaKursow.sort(sortDniTygodniaDwa);
		cout << getImie();
		for (list<Kurs>::iterator it = mListaKursow.begin();
				it != mListaKursow.end(); it++)
		{

			list<Kurs>::iterator tmp=it;
			tmp++;
			if(*it==*tmp)
			{
				cerr<<"Konflikt zajec: "<<endl;
				it->pokaz();

			}
			else
			{
				it->pokaz();
			}


		}
	}

};

#endif /* STUDENT_HPP_ */
