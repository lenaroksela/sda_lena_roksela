/*
 * godzina.hpp
 *
 *  Created on: 27.05.2017
 *      Author: RENT
 */

#ifndef GODZINA_HPP_
#define GODZINA_HPP_
#include <string>
#include <map>
#include <utility>
#include <algorithm>
#include <list>
using namespace std;



class Godzina
{
	int mGodzina;
	int mMinuta;
public:
	Godzina(std::string godzina) :
			mGodzina(0), mMinuta(0)
	{
		mGodzina = (godzina[0] - '0') * 10;
		mGodzina += godzina[1] - '0';

		mMinuta = (godzina[2] - '0') * 10;
		mMinuta += godzina[3] - '0';
	}

	int getGodzina() const
	{
		return mGodzina;
	}

	int getMinuta() const
	{
		return mMinuta;
	}

//	bool operator==(const Godzina & godzina)
//	{
//		if (this->getGodzina() == godzina.getGodzina())
//		{
//			{
//				return this->getMinuta() == godzina.getMinuta() ? true : false;
//			}
//		}
//
//		else
//		{
//			return false;
//		}
//	}

	bool operator==(const Godzina & godzina)
	{
		if (this->getGodzina() == godzina.getGodzina())
		{
			{
				return this->getMinuta() == godzina.getMinuta() ? true : false;
			}
		}
		else
		{
			return false;
		}
	}

	bool operator>=(const Godzina & godzina)
	{

		if (this->getGodzina() > godzina.getGodzina())
		{
			return (this->getGodzina() - godzina.getGodzina()) < 2 ?
					true : false;
		}
		else if (this->getGodzina() < godzina.getGodzina())
		{
			return (godzina.getGodzina() - this->getGodzina()) < 2 ?
					true : false;
		}
		else
			return false;
	}

};

class DzienTygodnia
{
public:
	string mDzien;
	int mDz;

	DzienTygodnia(int dzien)
	{

		map<int, string> tydzien;
		tydzien.insert(make_pair(1, "poniedzialek"));
		tydzien.insert(make_pair(2, "wtorek"));
		tydzien.insert(make_pair(3, "sroda"));
		tydzien.insert(make_pair(4, "czwartek"));
		tydzien.insert(make_pair(5, "piatek"));
		tydzien.insert(make_pair(6, "sobota"));
		tydzien.insert(make_pair(7, "niedziela"));
		mDz = dzien;

		mDzien = tydzien.find(dzien)->second;
	}

	const string getDzien() const
	{
		return mDzien;
	}

	bool operator >(const DzienTygodnia & dzien)
	{
		return this->getDz() > getDz() ? true : false;
	}

	int getDz() const
	{
		return mDz;
	}

	bool operator==(const DzienTygodnia & dzien)
	{
		return (this->getDz() == dzien.getDz()) ? true : false;
	}

};



class Kurs
{
public:
	string mNazwa;
	DzienTygodnia mDzien;
	Godzina mPoczatek;
	Godzina mKoniec;
	int mSala;

	Kurs(string nazwa, int dzien, string poczatek, string koniec, int sala) :
			mNazwa(nazwa), mDzien(dzien), mPoczatek(poczatek), mKoniec(koniec), mSala(
					sala)
	{

	}

	Kurs(string nazwa, int dzien, string poczatek, string koniec) :
			mNazwa(nazwa), mDzien(dzien), mPoczatek(poczatek), mKoniec(koniec), mSala(
					0)
	{

	}

	const DzienTygodnia& getDzien() const
	{
		return mDzien;
	}

	const Godzina& getKoniec() const
	{
		return mKoniec;
	}

	const string& getNazwa() const
	{
		return mNazwa;
	}

	const Godzina& getPoczatek() const
	{
		return mPoczatek;
	}

	void pokaz()
	{
		cout << "....................." << endl;
		cout << "Nazwa kursu: " << getNazwa() << endl;
		cout << "Dzien tygodnia: " << getDzien().getDzien() << endl;
		cout << "Godzina: poczatek: " << mPoczatek.getGodzina() << ":"
				<< mPoczatek.getMinuta() << endl;
		cout << "Godzina: koniec: " << mKoniec.getGodzina() << ":"
				<< mKoniec.getMinuta() << endl;
		cout<< "Sala: "<< getSala()<<endl;

	}
	bool operator >(const Kurs & kurs)
	{
		return this->mDzien.getDz() > kurs.mDzien.getDz() ? true : false;
	}

	bool operator==(const Kurs & kurs)
	{

		return this->mDzien == kurs.getDzien()
				&& this->mPoczatek >= kurs.getPoczatek() ? true : false;

	}

	bool operator==(const string & kurs)
	{
//
//		return this->mDzien == kurs.getDzien()
//				&& this->mPoczatek >= kurs.getPoczatek() ? true : false;

		return(this->mNazwa.compare(kurs))?true:false;
	}




	int getSala() const
	{
		return mSala;
	}


};


#endif /* GODZINA_HPP_ */
