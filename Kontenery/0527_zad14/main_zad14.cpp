/*
 * main_zad14.cpp
 *
 *  Created on: 27.05.2017
 *      Author: RENT
 */

#include <fstream>
#include <iostream>

#include "godzina.hpp"
#include "student.hpp"
#include <list>
#include <vector>

using namespace std;

bool sortDniTygodnia(Kurs& first, Kurs& second)
{
	return first.mDzien.getDz() > second.mDzien.getDz() ? false : true;
}

bool konflikt(Kurs& first, Kurs& second)

{
	if (first.mDzien.getDz() == second.mDzien.getDz())
	{
		return true;
	}

	return false;
	//return first==second?true:false;
}

//class szukajKurs
//{
//    string mNazwa;
//public:
//    szukajKurs(string nazwa)
//    :mNazwa(nazwa)
//{
//}
//    bool operator()(Kurs& nowy)
//        {
//        return mNazwa.compare(nowy.getNazwa()) ? true:false;
//        }
//};
int main()
{
//	Godzina godzina("1450");
//	cout << godzina.getGodzina() << ":" << godzina.getMinuta();

	string nazwa;
	int dzien;
	string poczatek;
	string koniec;

	fstream plik;
	plik.open("zad14.txt", ios::in);
	if (!plik.good())
	{
		cout << "cos";
	}

	list<Kurs> kursy;
	while (!plik.eof())
	{

		plik >> nazwa >> dzien >> poczatek >> koniec;
		kursy.push_back(Kurs(nazwa, dzien, poczatek, koniec));

	}

//	for (list<Kurs>::iterator it = kursy.begin(); it != kursy.end(); it++)
//	{
//		it->pokaz();
//	}

	kursy.sort(sortDniTygodnia);
	cout << "-------------------" << endl;

//	for (list<Kurs>::iterator it = kursy.begin(); it != kursy.end(); it++)
//	{
//		it->pokaz();
//	}

//	for (list<Kurs>::iterator it = kursy.begin(); it != kursy.end(); it++)
//	{
//		list<Kurs>::iterator tmp = it;
//		tmp++;
//
//		if (*it == *tmp)
//		{
//			cout << "Konflikt!!!";
//			it->pokaz();
//			tmp->pokaz();
//
//		}
//	}

	fstream kursyStudenta;
	kursyStudenta.open("studenty.txt", ios::in);
	string imie;
	int ilosc;
	string nazwaKursu;

	list<Kurs>::iterator tymczas;
	list<Student> listaStudenta;

	while (!kursyStudenta.eof())
	{
		kursyStudenta >> imie >> ilosc;
		Student nowy(imie, ilosc);
			cout << imie << " " << ilosc << endl;
		for (int i = 0; i < ilosc; i++)
		{
			kursyStudenta >> nazwaKursu;

			tymczas = find(kursy.begin(), kursy.end(), nazwaKursu);
			tymczas->pokaz();
			cout<<tymczas->getNazwa()<<endl;
			nowy.dodajKurs((*tymczas));

		}
		listaStudenta.push_back(nowy);
	}

	int i=1;
	for (list<Student>::iterator it = listaStudenta.begin(); it != listaStudenta.end(); it++,i++)
	{
		it->pokazKursy();
		cout<<i<<endl;
	}

//	for (list<Student>::iterator it = listaStudenta.begin();
//			it != listaStudenta.end(); it++)
//	{
//		cout << "Konflikt1" << endl;
//		list<Kurs> tymczasowaLista = it->getListaKursow();
//
//
//		for (list<Kurs>::iterator im = tymczasowaLista.begin(); im != tymczasowaLista.end(); im++)
//		{
//			list<Kurs>::iterator tmp = im;
//			tmp++;
//			cout << "Konflikt2" << endl;
//			if (*im == *tmp)
//			{
//				cout << "Konflikt3" << endl;
//				it->pokazStudenta();
//			}
//		}
//	}

	return 0;
}
