#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

vector<int> intNaVector(int liczba)
{
	vector<int> wektor;

	while (liczba != 0)
	{
		int tmp = liczba % 10;
		wektor.push_back(tmp);
		liczba = liczba / 10;
	}

	return wektor;
}

int wektorNaInt(vector<int> wektor)
{
	int k = 1;
	int wynik = 0;
	for (unsigned int i = 0; i < wektor.size(); i++)
	{
		wynik += ((wektor[i]) * k);
		k *= 10;
	}
	return wynik;
}

bool czyPalindrom(int liczba)
{
	bool zmienna = true;
	vector<int> nowyWektor = intNaVector(liczba);
	vector<int>::reverse_iterator irt = nowyWektor.rbegin();

	for (vector<int>::iterator it = nowyWektor.begin(); it != nowyWektor.end();
			it++, irt++)
	{
		if (*it != *irt)
		{
			zmienna = false;
		}

	}
	return zmienna;
}

int czyPalindromX(vector<int> wektor)
{
	int tmp = wektorNaInt(wektor);
	while (!czyPalindrom(tmp))
	{
		tmp++;
	}
	return tmp;

}


int main()
{

	vector<int> nowy = intNaVector(4090087);
	for (unsigned int i = 0; i < nowy.size(); i++)
	{
		cout << nowy[i] << " ";
	}


	cout << czyPalindromX(nowy);

	return 0;
}
