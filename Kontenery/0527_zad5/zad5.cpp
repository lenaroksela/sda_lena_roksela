/*
 * zad5.cpp
 *
 *  Created on: 27.05.2017
 *      Author: RENT
 */

#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>

using namespace std;

int zlicz(string& slowo, char litera)
{
	vector<char> vectorSlowo;
	for(unsigned int i=0; i<slowo.size();i++)
	{
		vectorSlowo.push_back(slowo[i]);
	}

	return count(vectorSlowo.begin(),vectorSlowo.end(),litera);

}
int main()
{


	string slowo ="ala ma kota bla bla bla";
	char litera = 'a';
	cout<<"Litera: "<<litera<< " w wyrazeniu "<< slowo<< " jest tyle razy: "<<zlicz(slowo, litera);
	return 0;
}


