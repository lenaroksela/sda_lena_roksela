#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <ctime>
#include <cstdlib>


using namespace std;

bool sortowanieZAl(const char& first, const char& second)
{
	return first < second;
}


int randomF ()
{
	return rand()%10;
}

string posortujiodwroc(string& slowo)
{
	list<char> napis;
	for (unsigned int i = 0; i < slowo.size(); i++)
	{
		napis.push_back(slowo[i]);
	}

	napis.sort();
	int i = 0;
	for (list<char>::iterator it = napis.begin(); it != napis.end(); it++, i++)
	{
		slowo[i] = *it;
	}

	return slowo;
}

string pososortujLosowo(string & slowo)
{
	vector<char> napis;
	for (unsigned int i = 0; i < slowo.size(); i++)
	{
		napis.push_back(slowo[i]);
	}


	random_shuffle(napis.begin(), napis.end());
	//random_shuffle(napis.begin(), napis.end(),randomF);


	for (unsigned int i = 0; i < slowo.size(); i++)
	{
		slowo[i] = napis[i];
	}

	return slowo;
}
int main()
{

	srand(time(NULL));
	string slowo = "Czesc i dziekuje za rybe";
	posortujiodwroc(slowo);
	cout << slowo << endl;

	pososortujLosowo(slowo);
	cout << slowo;

	return 0;
}

