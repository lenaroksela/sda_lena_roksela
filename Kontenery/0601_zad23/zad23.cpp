#include <iostream>
#include <list>
#include <ctime>
#include <cstdlib>
#include <algorithm>

using namespace std;

int losuj()
{
	return rand() % 10 + 1;
}

ostream& operator<<(ostream & os, list<int>& lista)
{
	os << "[ ";
	for (list<int>::iterator it = lista.begin(); it != lista.end(); it++)
	{
		os << *it << " ";
	}
	os << "]";
	return os;
}

bool szukajParzyste(int & first, int& second)
{
	return second % 2 < first % 2;
}

bool szukajNieParzyste(int & first, int& second)
{
	return !(first % 2) > !(second % 2);
}

bool czyParzyste(int i)
{
	return (i % 2) == 0;
}

bool sortujOdwrotnie(int &first, int & second)
{
	return first > second;
}

int main()
{
	srand(time(NULL));

	list<int> losowaLista;

	for (int i = 0; i < 40; i++)
	{
		losowaLista.push_back(losuj());
		i++;
	}

	cout << "Randomowa lista:\n";
	cout << losowaLista << endl;

	losowaLista.sort();
	losowaLista.sort(szukajParzyste);

	cout << "Posortowana lista:\n";
	cout << losowaLista << endl;

	list<int> listaNieParzyste;
	list<int>::iterator it = find_if(losowaLista.begin(), losowaLista.end(),czyParzyste);

	listaNieParzyste.splice(listaNieParzyste.begin(), losowaLista, losowaLista.begin(), it);

	cout << "Lista nieparzyste:\n";
	cout << listaNieParzyste << endl;

	cout << "Lista parzysta:\n";
	cout << losowaLista << endl;

	cout << "Lista nieparzysta malejaco:\n";
	listaNieParzyste.sort(sortujOdwrotnie);
	cout << listaNieParzyste << endl;
//	losowaLista.merge(listaNieParzyste);

	cout << "Wynik:\n";
	losowaLista.splice(losowaLista.end(), listaNieParzyste);
	cout << losowaLista << endl;

	return 0;
}
