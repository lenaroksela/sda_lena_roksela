#include <iostream>
#include <string>
#include "zliczanieString.hpp"
using namespace std;

map<string,int> ZliczanieZnakow::mCache;
int main()
{
	string slowo = "Ala ma kota a kot ma ale";
	ZliczanieZnakow ciag(slowo);
	ciag.zlicz(slowo);
//	ciag.pokazMape();
	string noweslowo = "Abrakadabra bla";
	ZliczanieZnakow nowyciag(noweslowo);
	nowyciag.zlicz(noweslowo);
	nowyciag.pokazMape();


	return 0;
}
