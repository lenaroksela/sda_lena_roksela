/*
 * zliczanieString.hpp
 *
 *  Created on: 30.05.2017
 *      Author: RENT
 */

#ifndef ZLICZANIESTRING_HPP_
#define ZLICZANIESTRING_HPP_
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include <map>
using namespace std;


class ZliczanieZnakow
{

	string mCiagZnakow;
	int mLiczbaZnakow;
	vector<char> vectorCiag;
	static map<string, int> mCache;


public:
	ZliczanieZnakow()
	:mCiagZnakow(),mLiczbaZnakow(),vectorCiag()
	{

	}

	ZliczanieZnakow(string slowo)
		:mCiagZnakow(slowo),mLiczbaZnakow(),vectorCiag()
		{

		}

	void naVector()
		{
			for(unsigned int i=0;i<mCiagZnakow.size();i++)
			{
				vectorCiag.push_back(mCiagZnakow[i]);
			}

		}

	string naString(vector<char>& wektor)
	{
		string nowe;
		for(unsigned int i=0;i<wektor.size();i++)
		{
			nowe+=wektor[i];
		}
		return nowe;
	}
	const string& getCiagZnakow() const
	{
		return mCiagZnakow;
	}

	int getLiczbaZnakow() const
	{
		return mLiczbaZnakow;
	}

	const vector<char>& getVectorCiag() const
	{
		return vectorCiag;
	}

	vector<char> usunDuplikaty()
	{
		naVector();
		sort(vectorCiag.begin(),vectorCiag.end());
		vector<char>::iterator it =unique(vectorCiag.begin(),vectorCiag.end());
		vectorCiag.resize(distance(vectorCiag.begin(),it));

		return vectorCiag;
	}

	void zlicz(string& slowo)
	{
		vector<char>tmp=usunDuplikaty();
		mLiczbaZnakow=tmp.size();
		cout<<"Unikatowych znakow jest: "<<mLiczbaZnakow;
		cout<<" Unikatowe znaki: "<< naString(tmp)<<endl;
		dodajDoMapy(slowo,mLiczbaZnakow);

	}

	void dodajDoMapy(string slowo, int znaki)
	{
		mCache.insert(make_pair(slowo,znaki));
	}

	void pokazMape()
	{
		for(map<string,int>::iterator it=mCache.begin();it!=mCache.end();it++)
		{
			cout<<"wyrazenie: "<<it->first<<" liczba unikatowych znakow ";
			cout<<it->second<<endl;
		}
	}

};



#endif /* ZLICZANIESTRING_HPP_ */
