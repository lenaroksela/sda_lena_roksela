/*
 * Fish.hpp
 *
 *  Created on: 07.06.2017
 *      Author: RENT
 */

#ifndef FISH_HPP_
#define FISH_HPP_
#include <iostream>

class Fish
{
public:
	virtual void swim(int speed)
	{
		for (int i = 0; i < speed; i++)
		{
			std::cout << "Plask!" << std::endl;
		}
	}
	virtual ~Fish() = default;
};

 class  GoldFish final: public  Fish
{
public:
	void swim(int speed) override
	{
		for (int i = 0; i < speed; i++)
		{
			std::cout << "Chlup!" << std::endl;
		}
	}

};
//
// class WhiteGoldFish: public GoldFish
// {
//
// };

struct Osmiornica
{
	Osmiornica()=default;
	Osmiornica(const Osmiornica & osmiornica)=delete;
	Osmiornica& operator=(const Osmiornica &)=delete;
};
struct NieKopiowalna
{
	NieKopiowalna()=default;
	NieKopiowalna(const NieKopiowalna & ) = delete;
	~NieKopiowalna()
	{

	}
};

class MaszynaMnozaca
{
public:
	float mX;
	MaszynaMnozaca(int x)
	:mX(x)
	{

	}

	 virtual float pomnoz(float y) final
	{
		return y*mX;
	}

	float pomnoz(int y)=delete;
	~MaszynaMnozaca()=default;
};

class MaszynaMnozacoDodajaca :public MaszynaMnozaca
{
public:
	float mY;
	MaszynaMnozacoDodajaca(float x, float y)
	:MaszynaMnozaca(x)
	,mY(y)
{

}
	float dodaj(float y)
	{
		return mY+y;
	}

//	float pomnoz(float x)
//		{
//			return mX*y;
//		}

};


#endif /* FISH_HPP_ */
