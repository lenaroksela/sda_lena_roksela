#include <iostream>
#include <string>
#include <cmath>
#include <vector>
#include <list>
#include <algorithm>

#include "Fish.hpp"

#define POTEGA(x) pow(2,x)
using namespace std;

constexpr int potega(int x)
{
	return pow(2, x);
}

constexpr int silnia(int n)
{
	return n <= 1 ? 1 : n * (silnia(n - 1));
}




int main()
{
	//nowe rzeczy w c++ 11
	//nullptr
	//long long int
	//constexpr - deklaruje ze wartosc zmiennej lub funkcji moze byc wyliczona na etapie kompilacj, moze przyspieszyc troche program przy automatycznych zmiennych

	//uzycie makra
	int x;
	for (int i = 1; i < 6; i++)
		{
			x = POTEGA(i);
		//	cout << x << endl;
		}

	//uzycie funkcji consexpr dla potegi
	int z;
	for (int i = 1; i < 6; i++)
		{
			z = potega(i);
			cout << z << endl;
		}

	//uzycie funkcji constexpr dla silni
	int y;
	for (int i = 0; i < 10; i++)
		{
			y = silnia(i);
		//	cout << "Silnia z " << i << ": " << y << endl;
		}


	Fish fish;
	GoldFish goldfish;

	//fish.swim(2);

	//goldfish.swim(1);

	Fish * fishy;
	GoldFish bob;

	fishy=&bob;
	//fishy->swim(3);

	MaszynaMnozaca maszyna(4);

	cout<<maszyna.pomnoz(3.0f)<<endl;
//	int X=4;
//	cout<<maszyna.pomnoz(X);

	Osmiornica osmironica;
	Osmiornica nowa;
	//osmironica = nowa;

	Osmiornica osmo;
	//osmo = Osmiornica(nowa);
//
//	MaszynaMnozacoDodajaca dodawanie(42.00f);
//	cout<<dodawanie.dodaj(2);

	//przeladowanie ta sama nazwa inne argumenty
	//przeciazaone
	//przeslanianie nadpisywanie nazw w klasach pochodnych

	//auto automatyczna dedukcja typu

	vector<int> liczby;

	for(int i=0;i<100;i++)
	{
		liczby.push_back(i+1);
	}

//	for(auto it=liczby.begin();it!=liczby.end();advance(it,2))
//	{
//		cout<<*it<<" ";
//	}
//	cout<<endl;


//	for(auto & val : liczby)
//		{
//			cout<<val<<" ";
//		}
//	cout<<endl;

	vector<float> zmiennoprzecinkowe;

	float tab[4]={1.1,1.2,1.3,1.4};

	for(float i=0.0f;i<10;i++)
	{
		zmiennoprzecinkowe.push_back(i);
	}
	for(int i=0;i<4;i++)
		{
			cout<<tab[i]<<" ";
		}
	cout<<endl;

	for(auto& i : tab)
	{
		cout<<i<<" ";
	}
	cout<<endl;

	for(auto it=zmiennoprzecinkowe.begin();it!=zmiennoprzecinkowe.end();it++)
	{
		cout<<*it<<" ";
	}
	cout<<endl;
	for(auto& i : zmiennoprzecinkowe)
	{
		cout<<i<<" ";
	}
	cout<<endl;

	list<int> calkowite;
	for(int i=0;i<20;i++)
	{
		calkowite.push_back(i);
	}


	for(auto& val : calkowite)
	{
		val=val*2;
	}

	for(auto& val : calkowite)
	{
		//cout<<val<<" ";
	}
	cout<<endl;


	//LAMBDA
	//to anonimowe funkcje
	//[capture lista] zmienne ktore lambda ma widziec
	//(parametry funckji)
	//decltype

	auto potega = [](int y)->int{return y*y;};

	cout<<potega(2)<<" ";
	cout<<potega(5);
	cout<<endl;

	auto hello=[]()->string{return "Hello world!";};
	cout<<hello();
	cout<<endl;

	auto dodaj = [](int y, int x){return x+y;};
	cout<<dodaj(2,4);
	cout<<endl;


	int mnoznik=5;
	[=](int z){cout<<mnoznik*z;}(3);

	//   slowa kluczowe override, overload,
	//nullptr stala oznaczajaca wskaznik zerowy
















	return 0;
}

























