#include <list>
#include <algorithm>
#include <string>
#include <iostream>


using namespace std;

void lustrzaneOdbicie(string & slowo)
{
	list<char> slowoPrzed;

	for(unsigned int i=0;i<slowo.size();i++)
	{
		slowoPrzed.push_back(slowo[i]);
	}

	list<char> slowoPo;
	slowoPo=slowoPrzed;
	slowoPo.reverse();

	slowoPrzed.splice(slowoPrzed.end(),slowoPo);

	for(list<char>::iterator it = slowoPrzed.begin(); it!=slowoPrzed.end() ;it++)
	{
		cout<<*it;
	}
}


int main()
{
	string slowo= "Kot";
	lustrzaneOdbicie(slowo);

	return 0;
}

