#include <string>
#include <iostream>
#include <memory>
#include <vector>
#include <algorithm>

using namespace std;

class Kurczak
{
public:
  string mImie;
  Kurczak () :
      mImie ("Nikt")
  {
    cout << "Kurczak" << endl;
  }
  ~Kurczak ()
  {
    cout << "~Kurczak" << endl;
  }
};
//
//void fun()
//{
//	Kurczak * ptr = new Kurczak();
//	unique_ptr<Kurczak> ptrUnq (new Kurczak());
//
//}

bool
wiekszodzer (int i)
{
  return i > 0;
}

bool
podzielna (int i)
{
  return i % 30 == 0;
}

int
main ()
{
//
//	vector<shared_ptr<Kurczak>> kurczaki;
//
//	shared_ptr<Kurczak> dzielony = make_shared<Kurczak>();
//
//	for (int i = 0; i < 4; i++)
//	{
//		kurczaki.push_back(dzielony);
//		cout << dzielony.use_count() << endl;
//	}
//
//	dzielony.reset();
//	cout<<kurczaki[0].use_count();

  vector<int> liczby (201);
  iota (liczby.begin (), liczby.end (), -100);

  for (auto&val : liczby)
    {
      cout << val << " ";
    }
  cout << endl;

  if (all_of (liczby.begin (), liczby.end (), [](int i)
    { return i>0;}))
    {
      cout << "Wszystkie dodatnie";
    }
  else
    {
      cout << "Nie wszystkie dodatnie";
    }
  cout << endl;



  if (any_of (liczby.begin (), liczby.end (), [](int i)
    { return i/30==0;}))
    cout << "Sa liczby podzielne przez 3,5, 30 ";


  cout << endl;
 // remove_if (liczby.begin (), liczby.end (), [](int i){ return i==0;});
  liczby.erase(remove(liczby.begin(),liczby.end(),0));

  if (none_of (liczby.begin (), liczby.end (), [] (int i)
    { return i==0;}))
    {
      cout << "Nie ma zera";
    }
  else
    {
      cout << "Jest zero";
    }
  cout << endl;

    if (is_sorted (liczby.begin (), liczby.end ()))
      cout << "Posortowana";
  cout << endl;




  vector<int> drugi (201);
  auto it = copy_if (liczby.begin (), liczby.end (), drugi.begin (), [](int i){ return (i>90 || i<-90);});
  drugi.resize (distance (drugi.begin (), it));

  for (auto&val : drugi)
    {
      cout << val << " ";
    }
  cout << endl;

  vector<int> kolejny (10);
  auto it2 = find_if (liczby.begin (), liczby.end (), [](int i) { return i==78;});
  copy_n (it2, 10, kolejny.begin ());

  for (auto&val : kolejny)
    {
      cout << val << " ";
    }
  cout << endl;
  return 0;
}

