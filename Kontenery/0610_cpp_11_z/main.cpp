#include <string>
#include <iostream>
#include <memory>
#include <vector>
#include <algorithm>
#include <regex>

#include <cassert>

using namespace std;

struct Song
{
	string mTitle;
	string mAuthor;
	int mLength;

	Song(string title, string author, int len)
	: mTitle(title)
	, mAuthor(author)
	, mLength(len)
	{}
};

class Player
{
public:
	void next()
	{
		auto it = find(playlist.begin(), playlist.end(), currentSong);

		if(it != playlist.end()-1)
		{
			currentSong = *(++it);
		}
		else
		{
			currentSong = *playlist.begin();
		}

	}

	void prev()
	{

	}

	void addSong(Song song)
	{
		playlist.push_back(make_shared<Song>(song));
	}

	bool features(string author)
	{
		return any_of(playlist.begin(), playlist.end(), [author](shared_ptr<Song> song){return song->mAuthor == author;});
	}

	void play()
	{
		if(currentSong == nullptr)
		{
			currentSong = *playlist.begin();
		}
		cout<< currentSong->mAuthor <<" " << currentSong->mTitle <<endl;
	}

	Player() = default;

private:
	vector<shared_ptr<Song>> playlist;
	shared_ptr<Song> currentSong;
};

int main()
{

	Player winamp;
	winamp.addSong({"Kogutek", "ADCD", 360});
	winamp.addSong(Song("Kurczaczek", "IronWidly", 400));
	winamp.addSong(Song("Kukulka", "BBB", 250));

	winamp.play();
	winamp.next();

	winamp.play();
	winamp.next();
	winamp.play();
	winamp.next();
	winamp.play();
	winamp.next();
	winamp.play();


	return 0;
}
