/*
 * odtwarzacz.hpp
 *
 *  Created on: 10.06.2017
 *      Author: RENT
 */

#ifndef ODTWARZACZ_HPP_
#define ODTWARZACZ_HPP_

#include <string>
#include <iostream>
#include <vector>
using namespace std;

class Piosenka
{
public:

	Piosenka(string tytul, string wykonawca, int dlugosc) :
			mTytul(tytul), mWykonawca(wykonawca), mDlugosc(dlugosc)
	{

	}
	Piosenka() :
			Piosenka("", "", 0)
	{

	}
	int getDlugosc() const
	{
		return mDlugosc;
	}

	const string& getTytul() const
	{
		return mTytul;
	}

	const string& getWykonawca() const
	{
		return mWykonawca;
	}

	void wypisz()
	{
		cout << mTytul << " " << mWykonawca << " " << mDlugosc << endl;
	}

private:
	string mTytul;
	string mWykonawca;
	int mDlugosc;

};

class Odtwarzacz
{
public:
	vector<shared_ptr<Piosenka>> mLista;
	shared_ptr<Piosenka> mAktualnaPiosenka;

	void NastepnaPiosenka()
	{
		auto it = find(mLista.begin(), mLista.end(), mAktualnaPiosenka);
		if (it != mLista.end() - 1)
		{
			mAktualnaPiosenka = *(++it);
		}
		else
		{
			mAktualnaPiosenka = *mLista.begin();

		}
	}

	void PoprzedniaPiosenka()
	{

	}

	void shuffle()
	{
		random_shuffle(mLista.begin(), mLista.end());
	}

	void szukaj(string wykonawca)
	{

	}

	void dodajPiosenka(Piosenka & piosenka)
	{
		mLista.push_back(make_shared<Piosenka>(piosenka));
	}

	void odtwarzaj()
	{
		if(mAktualnaPiosenka!=nullptr)
		{
			mAktualnaPiosenka= *mLista.begin();
		}
		mAktualnaPiosenka->wypisz();
	}

};

#endif /* ODTWARZACZ_HPP_ */
