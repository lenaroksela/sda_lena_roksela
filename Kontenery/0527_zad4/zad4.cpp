#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>

using namespace std;

string usunSpacje(string & slowo)
{
	vector<char> vectorSlowo;

	for(unsigned int i =0; i<slowo.size();i++)
	{
		if(slowo[i]!=' ')
		{
		vectorSlowo.push_back(slowo[i]);
		}
	}


	string wynik(vectorSlowo.begin(),vectorSlowo.end());
	return wynik;
}

string usunSpacjeLista(string & slowo)
{
	list<char> vectorSlowo;

	for(unsigned int i =0; i<slowo.size();i++)
	{
		if(slowo[i]!=' ')
		{
		vectorSlowo.push_back(slowo[i]);
		}
	}


	string wynik(vectorSlowo.begin(),vectorSlowo.end());
	return wynik;
}


int main()
{

	string slowo="nowe slowo ze spacjami";
	cout<<slowo<<" bez spacji: "<<usunSpacje(slowo);

	string noweSlowo ="duzo  spacji  ";
	cout<<noweSlowo<<" bez spacji: "<<usunSpacjeLista(noweSlowo);



return 0;
}
