#include <string>
#include <iostream>
#include <memory>


using namespace std;

class Produkt
{
public:
	Produkt(float cena, string nazwa) :
			mCena(cena), mNazwa(nazwa)
	{

	}
	Produkt()
	:Produkt(0,"")
	{

	}

	float mCena;
	string mNazwa;

	void wypisz()
	{
		cout << mNazwa << " " << mCena << endl;
	}

};

void fun()
{
	shared_ptr<Produkt> nowy = make_shared<Produkt>();
	shared_ptr<Produkt> kolejny = make_shared<Produkt>(22.42f,"Slomka");


}
int main()
{
	shared_ptr<Produkt> tab[2]={   {make_shared<Produkt>(22.42f,"Slomka")} ,    {make_shared<Produkt>(122.42f,"NieSlomka")} };

	for(int i=0;i<2;i++)
	{
		tab[i]->wypisz();
	}



	return 0;
}



