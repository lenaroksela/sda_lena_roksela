/*
 * main.cpp
 *
 *  Created on: 08.06.2017
 *      Author: RENT
 */

#include <iostream>
#include <string>
#include <tuple>
using namespace std;

enum class KolorKota
{
	Bialy,
	Czarny,
	TriColor
};

enum class KolorPsa
{
	Bialy,
	Czarny
};

tuple<string, float, int>PobierzDane(int id)
		{

			if(id==0)
			{
				return make_tuple("Maks",1.75,60);

			}
			else if(id==1)
			{
				return  make_tuple("NieMaks",1.5,600);
			}

		}

int main()
{

	KolorKota kot = KolorKota::Bialy;
	KolorPsa pies = KolorPsa::Czarny;

	auto krotka = PobierzDane(1);

	cout<<get<0>(krotka)<<endl;
	cout<<get<1>(krotka)<<endl;
	cout<<get<2>(krotka)<<endl;


	return 0;
}


