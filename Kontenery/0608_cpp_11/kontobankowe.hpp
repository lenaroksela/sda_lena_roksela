/*
 * kontobankowe.hpp
 *
 *  Created on: 08.06.2017
 *      Author: RENT
 */

#ifndef KONTOBANKOWE_HPP_
#define KONTOBANKOWE_HPP_
#include <string>
#include <iostream>
using namespace std;


class KontoBankowe
{
public:
	string mImieWlasciciela;
	float mStanKonta;
	float mOprocentowanie;

	KontoBankowe(string imie, float konto, float procent)
	:mImieWlasciciela(imie),mStanKonta(konto),mOprocentowanie(procent)
	{

	}

	KontoBankowe(string imie)
	:KontoBankowe(imie,0,0)
	{

	}

	KontoBankowe()
	:KontoBankowe("", 0,0)
	{

	}

	void wypisz()
	{
		cout<<"Imie: "<<mImieWlasciciela;
		cout<<" Stan Konta: "<<mStanKonta;
		cout<<" Procent: "<<mOprocentowanie<<endl;
	}
};

class KontoBankoweVip:public KontoBankowe
{
public:
	using KontoBankowe::KontoBankowe;

	void podejdzBezKolejki()
	{
		cout<<"Bez kolejki";
	}

};


#endif /* KONTOBANKOWE_HPP_ */
