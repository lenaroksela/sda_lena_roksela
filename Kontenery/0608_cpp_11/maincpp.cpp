/*
 * maincpp.cpp
 *
 *  Created on: 08.06.2017
 *      Author: RENT
 */
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include "kontobankowe.hpp"

using namespace std;

struct Functor
{
	Functor()
	{

	}

	void operator ()(int& y) {cout<<y<<" ";}

};

bool sortuj(int & jeden, int & dwa)
{
	return jeden>dwa;
}

int main()
{
	//przeciazenie == przeladowywanie == overloading -> funkcja ma kilka sygnatur w ramach jednego zasiegu np jedna funkcja dla kilku
	//typow np int wypisz float wypisz, ma kilka swoihc wersji

	//przeslanianie == nadpisanie = override - > metoda ma taka sama sygnature co klasa bazowa ale inne zachowanie

	vector<int> calkowite;
	for(int i=1;i<101;i++)
	{
		calkowite.push_back(i);
	}

	for_each(calkowite.begin(), calkowite.end(), [](int& i){cout<<i<<" ";});
	cout<<endl;

	for_each(calkowite.begin(), calkowite.end(), Functor());
	cout<<endl;


	for_each(calkowite.begin(), calkowite.end(), [](int& i){if(i<20)cout<<i<<" ";});
	cout<<endl;

	for_each(calkowite.begin(), calkowite.end(), [](int i){return i>4;});
	cout<<endl;

	auto dzielnik=7;
	auto dziel = [dzielnik](int i)
					{if ( i%dzielnik == 0)
						cout<<i<<" ";
					};

	for_each(calkowite.begin(), calkowite.end(),dziel);
	cout<<endl;

	auto wynik=0;
	auto suma = [&wynik](int i)
		{if(i%2==0)
			{
				wynik+=i;
			}

		};

	for_each(calkowite.begin(),calkowite.end(),suma);
	cout<<wynik<<endl;

	int sum=0;
	for_each(calkowite.begin(), calkowite.end(),[&sum](int &i){sum+=i;});
	cout<<sum;
	cout<<endl;

//	sort(calkowite.begin(), calkowite.end(),sortuj);

	sort(calkowite.begin(),calkowite.end(), [](int& i, int & j){return i>j;});

	for_each(calkowite.begin(), calkowite.end(), [](int& i){cout<<i<<" ";});
	cout<<endl;

	KontoBankowe jeden("Pierwsza",45.6f,3.0f);
	KontoBankowe dwa("Druga");
	KontoBankowe trzy;

	cout<<endl;
	jeden.wypisz();
	dwa.wypisz();
	trzy.wypisz();

	KontoBankoweVip jedenVip("VIP 1",10000.02f,4.0f);
	KontoBankoweVip dwaVip("VIP 2");
	KontoBankoweVip trzyVip;

	jedenVip.wypisz();
	dwaVip.wypisz();
	trzyVip.wypisz();


	vector<int> wektorInic={1,4,5,42,66};

	for_each(wektorInic.begin(),wektorInic.end(),[](int i){cout<<i<<" ";});

	for(auto & val : wektorInic)
	{
		cout<<val<<" ";
	}

	vector<KontoBankowe> konta  {{"Heniek",2,2},{"Mietek",2,2},{"Bolek",2,2}};
	for(auto it=konta.begin();it!=konta.end();it++)
	{
		(*it).wypisz();
	}

	for(auto & val : konta)
		{
			val.wypisz();
		}


	return 0;
}


