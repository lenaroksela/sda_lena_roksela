//============================================================================
// Name        : sobota5.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

int silnia(int n)
{
	if (n == 0 || n == 1)
	{
		return 1;
	}
	else
		return n * silnia(n - 1);
}

int main()
{
	std::cout << "Rekurencja" << std::endl;
	int n;
	std::cout << "Podaj n: ";
	std::cin >> n;
	std::cout << silnia(n) << std::endl;

	int m;
	int k = 1;
	std::cout << "Iteracja" << std::endl;
	std::cout << "Podaj m: ";
	std::cin >> m;

	for (int i = 1; i <= n; i++)
	{
		k *= i;
	}
	std::cout << k << std::endl;
	return 0;
}
