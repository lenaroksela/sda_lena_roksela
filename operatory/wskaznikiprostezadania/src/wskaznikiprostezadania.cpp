//============================================================================
// Name        : wskaznikiprostezadania.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

//przez  referencje
void zwiekszOPiec(int &liczba)
{
	liczba += 5;
}

//przez wskaznik
void kwadrat(int *liczba)
{
	*liczba *= *liczba;
}

int main()
{
	std::cout << "Podaj liczbe: " << std::endl;
	int liczba;
	std::cin >> liczba;

	zwiekszOPiec(liczba);
	kwadrat(&liczba);

	std::cout << "(liczba+5)^2 = " << liczba << std::endl;

	return 0;
}
