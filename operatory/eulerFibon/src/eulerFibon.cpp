//============================================================================
// Name        : eulerFibon.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

int fib(int n) {

	if (n == 1) {
		return 1;
	}

	return (fib(n)) + (fib(n - 1));

}

int main() {
	int n = 0;

	for (int i = 4; i >= 1; i--) {

		if ((fib(i)) % 2 == 0) {
			n+=fib(i);

		}
	}
	std::cout << n << std::endl;
	return 0;
}
