#include <iostream>



struct Stos
{
	int wierzcholek; // pozycja w tablicy "dane" - gdzie zapisa� nowy element
	int *dane;
};

void pokazRozmiar(const Stos& stosLiczb)
{
	// poka� rozmiar stosu
	std::cout << "Rozmiar: " << stosLiczb.wierzcholek << std::endl;
}

void pokazWierzcholek(const Stos& stosLiczb)
{
	// sprawd�, co jest na wierzchu stosu
	// je�li stos jest pusty, to poinformuj o tym
	if (stosLiczb.wierzcholek == 0)
	{
		std::cout << "Stos jest pusty!" << std::endl;
	}
	else
	{
		// w przeciwnym wypadku - poka� wierzcho�ek
		// ostatni wstawiony element znajduje si� o 1 ni�ej ni� miejsce na nowy element
		int wierzch = stosLiczb.dane[stosLiczb.wierzcholek - 1];
		std::cout << "Na gorze jest: " << wierzch << std::endl;
	}
}

void wstaw(Stos& stosLiczb, int nowaWartosc,const int& rozmiarr)
{
	if (stosLiczb.wierzcholek >= rozmiarr)
	{
		std::cerr << "Stos pelny!" << std::endl;
		return;
	}

	// wstawi� liczb� na szczyt stosu
	stosLiczb.dane[stosLiczb.wierzcholek] = nowaWartosc; // zapisz dane na wierzcho�ku stosu
	stosLiczb.wierzcholek += 1;			 				// przesuwam wierzcho�ek
}

int zdejmij(Stos& stosLiczb)
{
	if (stosLiczb.wierzcholek == 0)
	{
		std::cerr << "Proba zdjecia z pustego stosu!" << std::endl;
		return 0;
	}

	stosLiczb.wierzcholek -= 1; // przesuwam wierzcho�ek na pierwszy istniej�cy element
	int zdjete = stosLiczb.dane[stosLiczb.wierzcholek]; // odczytaj dane z wierzchu stosu
	return zdjete;
}

int main()
{
	int rozmiarr;
	std::cout<<"Podaj rozmiar stosu: ";
	std::cin>>rozmiarr;


	// nowy stos
	Stos liczby;
	liczby.wierzcholek =0;
	liczby.dane = new int[rozmiarr];

	pokazRozmiar(liczby);

	wstaw(liczby, 4, rozmiarr);

	pokazWierzcholek(liczby);

	pokazRozmiar(liczby);

	wstaw(liczby, 7, rozmiarr);

	pokazWierzcholek(liczby);

	pokazRozmiar(liczby);

	int zdjete1 = zdejmij(liczby);
	std::cout << "Zdjete: " << zdjete1 << std::endl;

	pokazWierzcholek(liczby);

	pokazRozmiar(liczby);

	int zdjete2 = zdejmij(liczby);
	std::cout << "Zdjete: " << zdjete2 << std::endl;

	pokazWierzcholek(liczby);

	pokazRozmiar(liczby);

	int zdjete3 = zdejmij(liczby);
	std::cout << "Zdjete: " << zdjete3 << std::endl;

	wstaw(liczby, 42, rozmiarr);
	wstaw(liczby, 128,rozmiarr);
	wstaw(liczby, -1,rozmiarr);
	wstaw(liczby, 8,rozmiarr);
	wstaw(liczby, -4,rozmiarr);
	wstaw(liczby, 10,rozmiarr);
	wstaw(liczby, 100,rozmiarr);

	pokazRozmiar(liczby);
	pokazWierzcholek(liczby);

	wstaw(liczby, 100, rozmiarr);

	delete[] liczby.dane;

	return 0;
}
