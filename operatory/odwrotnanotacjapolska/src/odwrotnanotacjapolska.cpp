#include <iostream>
#include <string>

const std::string RPN[] =
{ "3 2 + 4 -", "1 1 1 1 + + +", "5 1 2 + 4 * + 3 -",
		"1 1 + 2 + 3 1 1 + 7 - 9 / * -" };
const int wyniki[] =
{ 1, 4, 14, 4 };
int k = 0;

int wylicz(const std::string wyrazenie)
{

		int wierzcholek; // pozycja w tablicy "dane" - gdzie zapisać nowy element
		int dane[20];

		wierzcholek = 0;

	for (unsigned i = 0; i < wyrazenie.length(); i++)
	{
		if (wyrazenie[i] == ' ')
		{
			continue;
		}
		else if (wyrazenie[i] == '+')
		{
			//wierzcholek -= 1; // przesuwam wierzchołek na pierwszy istniejący element
			int zdjete = dane[wierzcholek -2]+dane[wierzcholek -1] ; // odczytaj dane z wierzchu stosu
			wierzcholek -= 2;
			//int zdjete2 = dane[wierzcholek]; // odczytaj dane z wierzchu stosu


			dane[wierzcholek] = zdjete; // zapisz dane na wierzchołku stosu
			wierzcholek += 1;


		}
		else if (wyrazenie[i] == '-')
		{
			int zdjete = dane[wierzcholek -2] - dane[wierzcholek -1] ; // odczytaj dane z wierzchu stosu
						wierzcholek -= 2;
						//int zdjete2 = dane[wierzcholek]; // odczytaj dane z wierzchu stosu


						dane[wierzcholek] = zdjete; // zapisz dane na wierzchołku stosu
						wierzcholek += 1;
		}
		else if (wyrazenie[i] == '*')
		{
			int zdjete = dane[wierzcholek -2] * dane[wierzcholek -1] ; // odczytaj dane z wierzchu stosu
									wierzcholek -= 2;
									//int zdjete2 = dane[wierzcholek]; // odczytaj dane z wierzchu stosu


									dane[wierzcholek] = zdjete; // zapisz dane na wierzchołku stosu
									wierzcholek += 1;
		}
		else if (wyrazenie[i] == '/')
		{
			int zdjete = dane[wierzcholek -2] / dane[wierzcholek -1] ; // odczytaj dane z wierzchu stosu
									wierzcholek -= 2;
									//int zdjete2 = dane[wierzcholek]; // odczytaj dane z wierzchu stosu


									dane[wierzcholek] = zdjete; // zapisz dane na wierzchołku stosu
									wierzcholek += 1;
		}
		else
		{
			k= wyrazenie[i] - '0';
			dane[wierzcholek] = k; // zapisz dane na wierzchołku stosu
			wierzcholek += 1;
		}

	}

	return dane[wierzcholek -1];
}

void sprawdz(const std::string rpn, int oczekiwanyWynik)
{
	int wynik = wylicz(rpn);
	if (wynik == oczekiwanyWynik)
	{
		std::cout << "Cacy ";
	}
	else
	{
		std::cout << "Be   ";
	}
	std::cout << rpn << " => " << wynik << std::endl;
}

int main(int argc, char* argv[])

{
	for (int i = 0; i < 4; ++i)
	{
		sprawdz(RPN[i], wyniki[i]);
	}

	return 0;
}
