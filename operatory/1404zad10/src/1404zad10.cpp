
#include <iostream>
#include <cmath>
#include <iomanip>

void heron(double& liczba)
{

	double b = 0.0;
	b = liczba;

	do {

	b = (b + (liczba/(b)) )/2.0;
	}while ((b-(liczba/b))>= 0.0000001);

	std::cout<<"Pierwiastek kwadratowy "<< b <<std::endl;
}

int main()
{
	std::cout<< std::setprecision (10);
	double liczba;
	std::cout << "Podaj nieujemna liczbe rzeczywista: "<< std::flush;
	std::cin>> liczba;

	heron(liczba);

	std::cout<<sqrt(liczba);


	return 0;
}
