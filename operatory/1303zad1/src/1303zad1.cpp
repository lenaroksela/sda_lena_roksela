#include <iostream>

void zapytaj(std::string tekst, int& liczba)
{
	std::cout << tekst << std::endl;
	std::cin >> liczba;
}

void moduloDziesiec(int* liczby, int liczba)
{
	liczby[0] = ((liczba) % 10);
}

void bityDrugiejCzworki(int* liczby, int liczba)
{
	liczby[1] = ((liczba) >> 4 & 0xF);
}

int main(int argc, char* argv[])
{
	int liczba;
	zapytaj("Podaj liczb�:", liczba);

	int liczby[2];
	moduloDziesiec(liczby, liczba);
	bityDrugiejCzworki(liczby, liczba);
	std::cout << "x            = " << liczba << std::endl;
	std::cout << "x % 10       = " << liczby[0] << std::endl;
	std::cout << "x >> 4 & 0xF = " << liczby[1] << std::endl;

	return 0;
}
