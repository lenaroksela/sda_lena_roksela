#include <iostream>

int fib(int n)
{
	if (n == 0)
	{
		return 0;
	}

	if (n == 1)
	{
		return 1;
	}
	return fib(n - 1) + fib(n - 2);
}

int main()
{
	std::cout << "Fibonacci: " << std::endl;
	std::cout << "Podaj liczbe n: " << std::endl;
	int n;
	std::cin >> n;

	std::cout << fib(n);
}
