//============================================================================
// Name        : 1503zad1.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

void wczytajTablice(int* tablica, const int& rozmiar)
{
	for (int i = 0; i < rozmiar; ++i)
	{
		do
		{
			std::cout << "Podaj liczb� " << i + 1 << ": ";
			std::cin >> tablica[i];
		} while (tablica[i] == 0);
	}
}

void mnozenie(int* liczby, const int& rozmiar, int& iloczyn)
{
	for (int i = 0; i < rozmiar; i++)
	{
		iloczyn *= liczby[i];
	}
}

int main()
{
	std::cout << "Podaj ilo�� liczb: " << std::flush;
	int rozmiar;
	std::cin >> rozmiar;

	int* liczby = new int[rozmiar];

	wczytajTablice(liczby, rozmiar);

	int iloczyn = 1;
	mnozenie(liczby, rozmiar, iloczyn);
	std::cout << "Wynik mnozenia podanych liczb to: " << iloczyn;

	delete[] liczby;
	return 0;
}
