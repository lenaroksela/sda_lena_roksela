#include <iostream>

int main()
{
	std::cout << "Podaj ilo�� liczb: " << std::endl;
	int ilosc;
	std::cin >> ilosc;


	int* tablica = new int[ilosc];
	int suma = 0;

	for (int i = 0; i < ilosc; i++)
	{
		std::cout << "Podaj liczbe " << i + 1 << ": ";
		std::cin >> tablica[i];
		suma += tablica[i];
	}

	std::cout << "Suma to: " << suma;

	delete[] tablica;
	return 0;
}
