//Otrzymuje na wej�ciu ilo�� liczb. Nast�pnie prosi o podanie tych liczb, zapisuje liczby w tablicy. Nast�pnie sortuje za pomoc� std::sort i pokazuje 50. i 95. percentyl wynik�w.
//<pozycja percentylu> = <pod�oga>( <maksymalny indeks tablicy> * <szukany procent> )
//�eby zmieni� wynik std::floor z double na int, potrzebnego do indeksowania tablicy, trzeba u�y� static_cast<int>().
//Sortowanie jest w nag��wku <algorithm>
//Wywo�anie:
//std::sort(wska�nik_do_tablicy, wska�nik_do_tablicy + rozmiar_tablicy);

#include <iostream>
#include <algorithm>
#include <cmath>

void percentyl(float szukany, int *rozmiar,int &perc)
{

	perc = static_cast<int>(std::floor((*rozmiar-1)*szukany));

	std::cout<<"Percentyl " << szukany << " " << "na pozycji " << (perc);


}
int main()
{
	int ilosc;
	std::cout << "Podaj ilosc liczb: "<<std::flush;
	std::cin>>ilosc;

	int tab[ilosc];
	for(int i=0; i<ilosc; i++)
	{
		std::cout<<"Podaj "<< i+1 <<" "<< "liczbe: ";
		std::cin>>tab[i];

	}

	for(int j=0; j<ilosc; j++)
		{
			std::cout<<tab[j]<<" ";
		}

	std::sort(tab, tab + ilosc);

	std::cout<<std::endl;

	for(int j=0; j<ilosc; j++)
	{
		std::cout<<tab[j]<<" ";
	}
	std::cout<<std::endl;

	int perc;
	percentyl(0.5, &ilosc, perc);

	std::cout<<" " <<tab[perc]<<std::endl;

	percentyl(0.95, &ilosc, perc);
	std::cout<<" " << tab[perc]<<std::endl;

	return 0;
}
