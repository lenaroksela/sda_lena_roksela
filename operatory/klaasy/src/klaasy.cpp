#include <iostream>
using namespace std;
int main()
{
	int t;
	cin >> t;
	for (int i = 0; i < t; i++)
	{
		int a, b;
		int suma = 0;
		cin >> a >> b;

		if (a <= b)
		{
			suma = a;
			while (suma % b)
			{
				suma += a;

			}
			cout << suma << endl;
		}
		else if (a > b)
		{
			suma = b;
			while (suma % a)
			{
				suma += b;

			}
			cout << suma << endl;
		}

	}
	return 0;
}
