#include <iostream>

void pokaz(float** m, int maxX, int maxY);

int main()
{
	const int stala[5][5] =
	{
	{ 1, 2, 3, 4, 5 },
	{ 6, 7, 8, 9, 10 },
	{ 11, 12, 13, 14, 15 },
	{ 16, 17, 18, 19, 20 },
	{ 21, 22, 23, 24, 25 } };

	float** macierz = NULL;

	// stworzy� dynamicznie dwuwymiarow� tablic� float�w
	macierz = new float*[5];
	for (int i = 0; i < 5; i++)
	{
		macierz[i] = new float[5];
	}

	// przepisa� warto��i ze sta�ej tablicy tablic, mno��c ka�d� warto�� przez 1.5f
	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			macierz[i][j] = stala[i][j] * (1.5);
		}
	}

	// pokaza� przemno�on� tablic� za pomoc� funkcji pokaz()
	pokaz(macierz, 5, 5);

	// zwolni� tablic� dwuwymiarow�
	for (int i = 0; i < 5; i++)
	{
		delete[] macierz[i];
	}
	delete[] macierz;

return 0;
}

void pokaz(float** m, int maxX, int maxY)
{
	for (int y = 0; y < maxY; ++y)
	{
		for (int x = 0; x < maxX; ++x)
		{
			std::cout << m[y][x] << ' ';
		}
		std::cout << std::endl;
	}

}
