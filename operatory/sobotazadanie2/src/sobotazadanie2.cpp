//============================================================================
// Name        : sobotazadanie2.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cmath>

void obliczenia(double a, double b, double c)
{
	if (a == 0)
	{
		std::cerr << "Wartosc a nie moze byc rowna zero";
	}
	else
	{
		double delta = (b * b) - 4 * a * c;
		double wynik1;
		double wynik2;

		if (delta < 0)
		{
			std::cerr << "Nie ma wyniku" << std::endl;
		}
		else if (delta == 0)
		{
			wynik1 = -b / (2 * a);
			std::cout << "Wynik 1 " << wynik1 << std::endl;
		}
		else
		{
			wynik1 = ((-b) + (sqrt(delta))) / (2 * a);
			wynik2 = ((-b) - (sqrt(delta))) / (2 * a);
			std::cout << "Wynik 1 " << wynik1 << std::endl;
			std::cout << "Wynik 2 " << wynik2 << std::endl;
		}
	}

}

int main()
{

	double a, b, c;
	std::cout << "Wprowadz a " << std::endl;
	std::cin >> a;
	std::cout << "Wprowadz b " << std::endl;
	std::cin >> b;
	std::cout << "Wprowadz c " << std::endl;
	std::cin >> c;

	obliczenia(a, b, c);

	return 0;
}
