//============================================================================
// Name        : sobotazad3.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

int NWD(int m, int n)
{
	if (m == 0)
	{
		return n;
	}
	else
	{
		int r = n % m;
		return (NWD(r, m));
	}
}

int main()
{

	int m = 0;
	int n = 0;

	std::cout << "Podaj 2 liczby naturalne: ";
	std::cin >> m;
	std::cin >> n;

	std::cout << "Wynik :" << NWD(m, n);

	return 0;
}
