#include <iostream>

void pokazMacierz(const int tablica[5][5])
{
	for (int y = 0; y < 5; ++y)
	{
		for (int x = 0; x < 5; ++x)
		{
			if (tablica[y][x] < 10)
			{
				std::cout << ' ';
			}
			std::cout << tablica[y][x] << ' ';
		}
		std::cout << '\n';
	}
	std::cout << std::flush;
}

int main()
{
	int w = 1;
	int pomocna;

	int macierz[5][5] =
	{
	{ 1, 2, 3, 4, 5 },
	{ 6, 7, 8, 9, 10 },
	{ 11, 12, 13, 14, 15 },
	{ 16, 17, 18, 19, 20 },
	{ 21, 22, 23, 24, 25 } };

	pokazMacierz(macierz);
	std::cout << std::endl;

	for (int k = 0; k <= 3; k++)
	{
		for (int j = w; j <= 4; j++)
		{
			pomocna = macierz[k][j];
			macierz[k][j] = macierz[j][k];
			macierz[j][k] = pomocna;

		}
		w++;
	}

	pokazMacierz(macierz);
	return 0;
}

