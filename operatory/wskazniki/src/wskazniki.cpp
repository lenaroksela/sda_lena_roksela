//============================================================================
// Name        : wskazniki.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

int main()
{
	int a = 10;
	int* x = NULL;

	x = &a;
	std::cout << *x << std::endl;

	std::cout << *x + 1 << std::endl;
	std::cout << *x << std::endl;

	int& y=a;
	std::cout << y<<std::endl;
	std::cout << y+1<<std::endl;
	std::cout << y<<std::endl;
	y=a+1;
	std::cout << y<<std::endl;

	int& z = y;
	std::cout << "z " << z <<std::endl;



	return 0;
}
