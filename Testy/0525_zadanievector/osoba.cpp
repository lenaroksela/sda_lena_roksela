#include <iostream>
#include <string>
#include <vector>
#include <fstream>
using namespace std;

struct Osoba
{
	string mImie;
	int mWiek;



	Osoba(string imie, int wiek) :
			mImie(imie), mWiek(wiek)
	{

	}


};

int main()
{
	fstream plikOsob;
	vector<Osoba> listaOsob;

	plikOsob.open("osoby.txt", ios::in);

	string imie;
	int wiek;
	while (!plikOsob.eof())
	{

		plikOsob >> imie >> wiek;
		listaOsob.push_back(Osoba(imie, wiek));

	}
	plikOsob.close();


	for (vector<Osoba>::iterator it = listaOsob.begin(); it != listaOsob.end();
			it++)
	{
		cout << it->mImie << " " << it->mWiek << endl;
	}

	return 0;
}
