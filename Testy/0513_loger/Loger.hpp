/*
 * Loger.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef LOGER_HPP_
#define LOGER_HPP_
#include <fstream>
#include <iostream>
#include <string>

using namespace std;

class FileLoger
{
private:

	ofstream mFile;
	static FileLoger * mInstance;
	static int mLicznik;
	FileLoger();

public:

	static int licznik();
	static FileLoger * getInstance();
	void log(string dataToLog, int licznik);
	void log(string dataToLog);

};



#endif /* LOGER_HPP_ */
