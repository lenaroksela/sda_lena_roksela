/*
 * Loger.cpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */
#include "Loger.hpp"

FileLoger * FileLoger::mInstance =0;
int FileLoger::mLicznik=0;

FileLoger::FileLoger()
	{
			mFile.open("log.txt");
	}

void FileLoger::log(string dataToLog, int licznik)
	{

		mFile <<licznik<<" "<< dataToLog<<endl;
	}

void FileLoger::log(string dataToLog)
	{

		mFile <<" "<< dataToLog<<endl;
	}

FileLoger * FileLoger::getInstance()
	{

			if(!mInstance)
			mInstance = new FileLoger;
			return mInstance;
	}

 int FileLoger::licznik()
 {
	 return ++mLicznik;
 }

//w singleton nie potrzebujemy destruktora

//FileLoger::~FileLoger()
//{
//	mFile.close();
//}
