#include <iostream>
#include <iostream>
#include <exception>

using namespace std;

//enum AircraftError
//{
//	WingsOnFire = 1, WingBroken = 2, NoRunway = 3, Crahed = 4
//};

class AircraftException: public exception
{
public:
	AircraftException(const char* errMessage) :
			m_ErrMessage(errMessage)
	{
	}
	// overriden what() method from exception class
	const char* what() const throw ()
	{
		return m_ErrMessage;
	}

private:
	const char* m_ErrMessage;
};

class WingsOnFire: public AircraftException
{
public:
	WingsOnFire(const char* errMessage) :
			AircraftException(errMessage)
	{
	}
};

class WingBroken: public AircraftException
{
public:
	WingBroken(const char* errMessage) :
			AircraftException(errMessage)
	{
	}
};

class NoRunway: public AircraftException
{
public:
	NoRunway(const char* errMessage) :
			AircraftException(errMessage)
	{
	}
};

class Crahed: public AircraftException
{
public:
	Crahed(const char* errMessage) :
			AircraftException(errMessage)
	{
	}

};

int main()
{
	try
	{
		throw WingsOnFire("WingsOnFire");
		throw WingBroken("WingBroken");
		throw NoRunway("NoRunway");
		throw Crahed("Crahed");
	}

		catch (WingBroken& e)
		{
			cout << e.what() << "\n";
		}
		catch (WingsOnFire& e)
				{
					cout << e.what() << "\n";

				}
		catch (NoRunway& e)
		{
			cout << e.what() << "\n";
		}

		catch (Crahed& e)
		{
			cout << e.what() << "\n";
		}
		catch (AircraftException& e)
		{
			cout << e.what() << "\n";
		}

	return 0;
}
