/*
 * Coffe.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef COFFE_HPP_
#define COFFE_HPP_
#include "Liquid.hpp"

class Coffee: public Liquid
{

	float mCoffein;
public:
	Coffee();
	Coffee(int amount, int coffein);
	~Coffee();


	virtual void add(int amount);
	virtual void remove(int amount);
	virtual void removeAll();

	int getAmount() const;
	void setAmount(int amount);
	float getCoffein() const;
	void setCoffein(float coffein);
};




#endif /* COFFE_HPP_ */
