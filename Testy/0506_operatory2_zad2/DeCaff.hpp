/*
 * DeCaff.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef DECAFF_HPP_
#define DECAFF_HPP_
#include "Coffe.hpp"

class DeCaff: public Coffee
{
public:
	DeCaff(int amount)
	:Coffee(amount, 0)
	{

	}

	void remove(int amount)
	{
		if (mAmount > amount)
		{
			mAmount -= amount;
		}
		else
		{
			mAmount = 0;
		}
	}

	void add(int amount)
	{
		mAmount += amount;
	}

	void removeAll()
	{
		mAmount = 0;
	}
};


#endif /* DECAFF_HPP_ */
