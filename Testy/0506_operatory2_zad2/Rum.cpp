/*
 * Rum.cpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#include "Rum.hpp"

Rum::Rum() :
		Liquid(0), mColor(Unknown)
{

}

Rum::Rum(int amount, Rum::Color color)
:Liquid(amount),mColor(color)
{

}

Rum::~Rum()
{

}

void Rum::remove(int amount)
{
	if (mAmount > amount)
	{
		mAmount -= amount;
	}
	else
	{
		mAmount = 0;
	}
}

void Rum::add(int amount)
{
	setAmount(mAmount += amount);
}

void Rum::removeAll()
{
	setAmount(0);
}

int Rum::getAmount() const
{
	return mAmount;
}

void Rum::setAmount(int amount)
{
	mAmount = amount;
}

