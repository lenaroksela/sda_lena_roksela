/*
 * Milk.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef MILK_HPP_
#define MILK_HPP_
#include "Liquid.hpp"

class Milk: public Liquid
{

	float mFat;
	
public:
	Milk();
	Milk(int amount, int fat);
	~Milk();



	virtual void add(int amount);
	virtual void remove(int amount);
	virtual void removeAll();

	int getAmount() const;
	void setAmount(int amount);
	float getFat() const;
	void setFat(float fat);
};



#endif /* MILK_HPP_ */
