/*
 * Cup.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef CUP_HPP_
#define CUP_HPP_
#include <cstdlib>
#include "Liquid.hpp"
#include "Milk.hpp"
#include "Coffe.hpp"
#include "DeCaff.hpp"
#include <iostream>

class Cup
{
	Liquid ** mContent;
	size_t mSize;


public:
	Cup();
	//Cup(Liquid * content, int amount);
	~Cup();


	void add(Liquid * liquid);
	void takeSip(int amount);
	void spill();

	void add(const Milk &liquid);
	void add(const Coffee &liquid);
	void add(const DeCaff &liquid);

	int calculateLiqAm();
	void clear();
	void resize();
	void wypisz();

	Cup& operator+=(int amount);
	int getSize() const;
	void setSize(int size);


};



#endif /* CUP_HPP_ */
