/*
 * Rum.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef RUM_HPP_
#define RUM_HPP_
#include <string>

#include "Liquid.hpp"

class Rum: public Liquid
{



public:
	enum Color
	{
		Dark,
		Light,
		Unknown
	};

	Color mColor;

	Rum();
	Rum(int amount, Rum::Color color);
	~Rum();

	virtual void add(int amount);
	virtual void remove(int amount);
	virtual void removeAll();

	int getAmount() const;
	void setAmount(int amount);
	const std::string& getColor() const;
	void setColor(const std::string& color);
};




#endif /* RUM_HPP_ */
