/*
 * Espresso.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef ESPRESSO_HPP_
#define ESPRESSO_HPP_
#include "Coffe.hpp"

class Espresso: public Coffee
{
public:
	Espresso(int amount)
	:Coffee(amount, 320)
	{

	}

	void remove(int amount)
	{
		if (mAmount > amount)
		{
			mAmount -= amount;
		}
		else
		{
			mAmount = 0;
		}
	}

	void add(int amount)
	{
		mAmount += amount;
	}

	void removeAll()
	{
		mAmount=0;
	}
};




#endif /* ESPRESSO_HPP_ */
