/*
 * Liquid.cpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#include "Liquid.hpp"

int Liquid::getAmount() const
{
	return mAmount;
}

void Liquid::setAmount(int amount)
{
	mAmount = amount;
}
