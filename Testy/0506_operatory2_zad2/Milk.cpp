/*
 * Milk.cpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#include "Milk.hpp"

Milk::Milk() :
		Liquid(0), mFat(0)
{

}
Milk::Milk(int amount, int fat)
:Liquid(amount), mFat(2.5)
{

}


Milk::~Milk()
{

}

void Milk::add(int amount)
{
	setAmount(mAmount += amount);
}
void Milk::remove(int amount)
{
	if (mAmount > amount)
	{
		mAmount -= amount;
	}
	else
	{
		mAmount = 0;
	}
}

void Milk::removeAll()
{
	mAmount=0;
}

int Milk::getAmount() const
{
	return mAmount;
}

void Milk::setAmount(int amount)
{
	mAmount = amount;
}

float Milk::getFat() const
{
	return mFat;
}

void Milk::setFat(float fat)
{
	mFat = fat;
}
