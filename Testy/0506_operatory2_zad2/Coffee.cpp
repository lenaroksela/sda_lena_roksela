/*
 * Coffee.cpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */
#include "Coffe.hpp"

Coffee::Coffee() :
		Liquid(0), mCoffein(0)
{

}

Coffee::Coffee(int amount, int coffein)
:Liquid(amount), mCoffein(coffein)
{

}

Coffee::~Coffee()
{

}



void Coffee::setCoffein(float coffein)
{
	mCoffein = coffein;
}

void Coffee::remove(int amount)
{
	if (mAmount > amount)
	{
		mAmount -= amount;
	}
	else
	{
		mAmount = 0;
	}
}

void Coffee::add(int amount)
{
	setAmount(mAmount += amount);
}

void Coffee::removeAll()
{
	setAmount(0);
}

int Coffee::getAmount() const
{
	return mAmount;
}

void Coffee::setAmount(int amount)
{
	mAmount = amount;
}

float Coffee::getCoffein() const
{
	return mCoffein;
}

