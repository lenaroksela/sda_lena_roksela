/*
 * Cup.cpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#include "Cup.hpp"
#include <iostream>

Cup::Cup() :
		mContent(0), mSize(0)
{

}

//Cup::Cup(Liquid * content, int amount)
//
//{
//
//}

Cup::~Cup()
{
	if (mSize != 0)
	{
//		delete [] mPunkt;
//		mSize=0;
//		for (size_t i = 0; i < mSize - 1; i++)
//		{
//			delete mContent[i];
//		}
//		delete[] mContent;
//		mSize = 0;
	}
}

void Cup::add(Liquid * liquid)
{
	resize();

	mContent[mSize - 1] = liquid;
}

void Cup::add(const Milk &liquid)
{
	resize();
	mContent[mSize - 1] = new Milk(liquid);
}

void Cup::add(const Coffee &liquid)
{
	resize();
	mContent[mSize - 1] = new Coffee(liquid);
}

void Cup::add(const DeCaff &liquid)
{
	resize();
	mContent[mSize - 1] = new DeCaff(liquid);
}

void Cup::takeSip(int amount)
{
	int liquidAmount = calculateLiqAm();

	if (liquidAmount > 0 && amount > 0)
	{
		if (liquidAmount <= amount)
		{
			spill();

		}
		else
		{
			for (size_t i = 0; i < mSize; i++)
			{
				float liquidRatio = (float) mContent[i]->getAmount() /(float)liquidAmount;
				mContent[i]->remove(liquidRatio * amount);
			}
		}
	}
	else
	{

	}

}

void Cup::spill()
{
	for (size_t i = 0; i < mSize; i++)
	{
		mContent[i]->removeAll();
	}
	clear();
}

void Cup::clear()
{
	for (size_t i = 0; i < mSize; i++)
	{
		delete mContent[i];
	}
	delete[] mContent;
	mSize = 0;
}

int Cup::getSize() const
{
	return mSize;
}

void Cup::setSize(int size)
{
	mSize = size;
}

//Cup& Cup::operator+=(int amount)
//{
//
//}

void Cup::wypisz()
{
	for (size_t i = 0; i < mSize; i++)
		{
			std::cout<<"["<<mContent[i]->getAmount()<<"]";
		}
}

void Cup::resize()
{
	if (mSize == 0)
	{
		mContent = new Liquid*[++mSize];
	}
	else
	{
		Liquid ** tmp = new Liquid*[++mSize];
		for (size_t i = 0; i < mSize - 1; i++)
		{
			tmp[i] = mContent[i];
		}
		delete[] mContent;
		mContent = tmp;
	}
}

int Cup::calculateLiqAm()
{
	int liquidAmount = 0;
	if (mSize == 0)
	{

	}
	else
	{

		for (size_t i = 0; i < mSize; i++)
		{
			liquidAmount += mContent[i]->getAmount();
		}
	}

	return liquidAmount;
}

