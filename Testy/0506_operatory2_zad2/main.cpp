/*
 * main.cpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#include "Liquid.hpp"
#include "Cup.hpp"
#include "Milk.hpp"
#include "Coffe.hpp"
#include "Rum.hpp"
#include "DeCaff.hpp"
#include "Espresso.hpp"

#include <iostream>

int main()
{

	Cup kubekKawy;
	kubekKawy.add(DeCaff(250));
	kubekKawy.add(Milk(100,2.5));

	kubekKawy.wypisz();
	kubekKawy.takeSip(49);
	kubekKawy.wypisz();

	kubekKawy.spill();
	kubekKawy.wypisz();

	return 0;
}


