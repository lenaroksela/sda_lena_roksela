/*
 * tablica.hpp
 *
 *  Created on: 16.05.2017
 *      Author: RENT
 */

#ifndef TABLICA_HPP_
#define TABLICA_HPP_
#include <cstdlib>
#include <iostream>
#include <string>

template<typename T>

class Tablica
{
private:
	size_t mRozmiar;
	T ** mTablica;

public:
	Tablica() :
			mRozmiar(0), mTablica()
	{

	}

	~Tablica()
	{
		clear();
	}

	int find(T typ)
	{
		for (unsigned int i = 0; i < mRozmiar; i++)
		{
			if (*mTablica[i] == typ)
			{
				std::cout << "Obiekt " << typ << " na pozycji " << i + 1
						<< ".\n";
				return 0;
			}
		}
		return 0;
	}

	void add(T typ)
	{
		resize();
		mTablica[mRozmiar - 1] = new T(typ);
	}

	void resize()
	{
		if (mRozmiar == 0)
		{
			mRozmiar++;
			mTablica = new T*[mRozmiar];
		}
		else
		{
			mRozmiar++;
			T** newUnitsArray = new T*[mRozmiar];

			for (unsigned int i = 0; i < mRozmiar - 1; i++)
			{
				newUnitsArray[i] = mTablica[i];
			}
			delete[] mTablica;
			mTablica = newUnitsArray;
		}
	}

	void resizeDown()
	{
		if (mRozmiar == 0)
		{
			std::cout << "tablica pusta" << std::endl;
		}
		else
		{
			T** newUnitsArray = new T*[mRozmiar - 1];
			for (unsigned int i = 0; i < mRozmiar - 1; i++)
			{
				newUnitsArray[i] = mTablica[i];
			}
			delete[] mTablica;
			mTablica = newUnitsArray;
			mRozmiar--;
		}
	}

	bool empty()
	{
		return (mRozmiar == 0) ? true : false;
	}

	void removeTyp(T typ)
	{
		for (unsigned int i = 0; i < mRozmiar; i++)
		{
			if (*mTablica[i] == typ)
			{
				remove(i);
			}
		}
	}
//
	void remove(int x)
	{
		mTablica[x] = mTablica[x + 1];
		for (unsigned int i = x; i < mRozmiar; i++)
		{
//			if(i+1 == mRozmiar)
//			{
//
//				break;
//			}
//			else
			mTablica[i] = mTablica[i + 1];

		}
		resizeDown();
	}

	void removeAll(T typ)
	{
		if (empty() == true)
		{
			std::cout << "Tablica pusta\n";
		}

		else
		{
			clear();

		}
	}

	T get(int x)
	{
		std::cout << "Obiekt na pozycji " << x + 1 << ": ";
		return *mTablica[x];
	}

	void clear()

	{
		for (unsigned int i = 0; i < mRozmiar; i++)
		{
			delete mTablica[i];
		}
		delete[] mTablica;
		mRozmiar = 0;
		mTablica = 0;
	}

	void wypisz()
	{
		std::cout << "[ ";
		for (unsigned int i = 0; i < mRozmiar; i++)
		{
			std::cout << *mTablica[i] << " ";
		}
		std::cout << "]" << std::endl;

	}
};

#endif /* TABLICA_HPP_ */
