/*
 * tablicaDynamiczna.hpp
 *
 *  Created on: 16.05.2017
 *      Author: RENT
 */

#ifndef TABLICADYNAMICZNA_HPP_
#define TABLICADYNAMICZNA_HPP_

template<typename typ>

class TablicaDynamiczna
{
	int mRozmiar;
	typ ** mTablica;

public:

	TablicaDynamiczna() :
			mRozmiar(), mTablica()
	{

	}
	TablicaDynamiczna(int rozmiar) :
			mRozmiar(rozmiar), mTablica()
	{

	}



	void resize()
	{
		if(mRozmiar==0)
		{
			mRozmiar++;
			mTablica=new TablicaDynamiczna*[mRozmiar];

		}
		else
		{
			mRozmiar++;
			TablicaDynamiczna** newUnitsArray = new TablicaDynamiczna*[mRozmiar];

			for(unsigned int i = 0; i<mRozmiar-1; i++)
			{
				newUnitsArray[i]=mTablica[i];
			}

			delete [] mTablica;
			mTablica = newUnitsArray;
		}
	}

	void add(typ* unit)
	{
		resize();
		mTablica[mRozmiar - 1] = unit;
	}

	void clear()
	{
		for (unsigned int i = 0; i < mRozmiar; i++)
		{
			delete mTablica[i];
		}
		delete[] mTablica;
		setSize(0);
		mTablica = 0;
	}

	void setSize(unsigned int size)
	{
		mRozmiar = size;
	}

};

#endif /* TABLICADYNAMICZNA_HPP_ */
