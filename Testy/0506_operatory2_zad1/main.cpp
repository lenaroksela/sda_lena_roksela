/*
 * main.cpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#include <iostream>

#include "Punkt_f.hpp"
#include "Zbior.hpp"

int main()
{

	PunktF p1(2.0, 2.1);
	p1.wypisz();

	p1 = p1 + 2.0;
	p1.wypisz();

	PunktF p2(1.9, 4.5);

	p2.wypisz();
	p2 = 3.1f + p2;

	p2.wypisz();

	PunktF p3(17.1, 99.0);
	p3.wypisz();

	PunktF p4(1.0, 2.0);

	p4 = p4 + p3;
	p4.wypisz();

	PunktF pkt1(1.0, 2.0);
	pkt1.wypisz();

	pkt1 = pkt1 * 42;
	pkt1.wypisz();

	PunktF pkt2(2.0, 33.0);
	pkt2.wypisz();

	pkt2 = 3 * pkt2;
	pkt2.wypisz();

	PunktF pkt3(5.0, 4.0);
	pkt3.wypisz();

	pkt3 = pkt3 * pkt1;
	pkt3.wypisz();
	std::cout << " \n";

	Zbior zbior;

	zbior + p1;
	zbior + p2;
	zbior + p3;

	zbior.wypisz();

	Zbior zbior2;
	zbior2 + p1;
	zbior2 + p2;
	zbior2 + p3;
	zbior2.wypisz();

	zbior + zbior2;
	zbior.wypisz();

	if (zbior)
	{
		std::cout << "Prawidlowy" << std::endl;
	}
	else
		std::cout << "Nie Prawidlowy" << std::endl;

	p1.wypisz();
	++p1;
	p1.wypisz();

	return 0;
}

