/*
 * Zbior.cpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */
#include "Zbior.hpp"

#include <iostream>

Zbior::Zbior() :
		mPunkt(0), mSize(0)
{

}
Zbior::~Zbior()
{
	if (mSize != 0)
	{
//		delete [] mPunkt;
//		mSize=0;
		for (size_t i = 0; i < mSize - 1; i++)
		{
			delete mPunkt[i];
		}
		delete[] mPunkt;
		mSize = 0;
	}
}

void Zbior::dodajPunkt(PunktF pkt)
{
	zmienrozmiar();

}

void Zbior::zmienrozmiar()
{
	if (getSize() == 0)
	{
		mPunkt = new PunktF*[++mSize];
	}
	else
	{
		PunktF ** tmp = new PunktF*[++mSize];
		for (size_t i = 0; i < mSize - 1; i++)
		{
			tmp[i] = mPunkt[i];
		}
		delete[] mPunkt;
		mPunkt = tmp;

	}

}

size_t Zbior::getSize() const
{
	return mSize;
}

void Zbior::setSize(size_t size)
{
	mSize = size;
}

void Zbior::operator+(const PunktF &punkt)
{
	zmienrozmiar();

	mPunkt[mSize - 1] = new PunktF(punkt);

}

PunktF Zbior::operator[](size_t indeks) const
{
	if (indeks < mSize)
	{
		return PunktF(*mPunkt[indeks]);
	}
	else
	{
		return PunktF(0, 0);
	}
}

void Zbior::operator+(const Zbior &zbior)
{

	for (size_t i = 0; i < zbior.getSize(); i++)
	{
		*this + zbior[i];
	}

}

Zbior::operator bool()
{
	return (this->getSize() != 0);
}

void Zbior::wypisz()
{
	std::cout << "<";
	for (size_t i = 0; i < this->getSize(); i++)
	{
		std::cout << "P" << i << "{";
		mPunkt[i]->wypisz();
		std::cout << "}";
	}
	std::cout << ">" << std::endl;
}

