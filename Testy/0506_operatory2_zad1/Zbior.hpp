/*
 * Zbior.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef ZBIOR_HPP_
#define ZBIOR_HPP_
#include <cstdlib>
#include "Punkt_f.hpp"

class Zbior
{
	PunktF ** mPunkt;
	size_t mSize;
	
	
	
public:
	Zbior();
	~Zbior();

	void dodajPunkt(PunktF pkt);
	void zmienrozmiar();
	void wypisz();

	void operator+(const PunktF &punkt);
	void operator+(const Zbior &zbior);
	operator bool();

	PunktF operator[]( size_t indeks)const;


	size_t getSize() const;
	void setSize(size_t size);
};



#endif /* ZBIOR_HPP_ */
