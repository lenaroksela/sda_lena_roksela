/*
 * Punkt_f.cpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#include "Punkt_f.hpp"
#include <iostream>

float PunktF::getX() const
{
	return mX;
}

void PunktF::setX(float x)
{
	mX = x;
}

float PunktF::getY() const
{
	return mY;
}

void PunktF::setY(float y)
{
	mY = y;
}

PunktF::PunktF(int x, int y) :
		mX(x), mY(y)
{

}

PunktF::PunktF() :
		mX(0), mY(0)
{

}

PunktF::PunktF(const PunktF &punkt) :
		mX(punkt.getX()), mY(punkt.getY())
{

}

PunktF PunktF::operator+(const float a) const
{
	PunktF wynik(0.0, 0.0);
	wynik.setX(mX + a);
	wynik.setY(mX + a);

	return wynik;
}

PunktF PunktF::operator+(const PunktF &punkt)
{
	PunktF wynik(0.0, 0.0);
	wynik.setX(this->getX() + punkt.getX());
	wynik.setY(this->getY() + punkt.getY());
	return wynik;
}

PunktF PunktF::operator*(float a)
{
	PunktF wynik(0.0, 0.0);
	wynik.setX(mX * a);
	wynik.setY(mX * a);

	return wynik;
}

PunktF PunktF::operator*(const PunktF &punkt) const
{
	PunktF wynik(0.0, 0.0);
	wynik.setX(this->getX() * punkt.getX());
	wynik.setY(this->getY() * punkt.getY());
	return wynik;
}

void PunktF::wypisz()
{
	std::cout << "[" << getX() << ", " << getY() << "]";
}

PunktF operator+(float a, PunktF pkt)
{
	PunktF wynik(0.0, 0.0);
	wynik.setX(pkt.getX() + a);
	wynik.setY(pkt.getY() + a);
	return wynik;

}

PunktF operator*(float a, PunktF pkt)
{
	PunktF wynik(0.0, 0.0);
	wynik.setX(pkt.getX() * a);
	wynik.setY(pkt.getY() * a);
	return wynik;

}

PunktF& PunktF::operator++()
{
	this->setX(mX+1);
	this->setY(mY+1);

	return *this;

}

PunktF PunktF::operator++(int)
{
	PunktF tmp(*this);
	this->operator ++();
	return tmp;

}
