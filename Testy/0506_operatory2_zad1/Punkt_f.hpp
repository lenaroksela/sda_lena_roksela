/*
 * Punkt_f.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef PUNKT_F_HPP_
#define PUNKT_F_HPP_

class PunktF
{
	float mX;
	float mY;

public:
	PunktF(int x, int y);
	PunktF();
	PunktF(const PunktF &punkt);

	float getX() const;
	void setX(float x);
	float getY() const;
	void setY(float y);

	PunktF operator+(const float a) const;
	PunktF operator+(const PunktF &punkt );

	PunktF operator*(float a);
	PunktF operator*(const PunktF &punkt )const;

	PunktF& operator++();
	PunktF operator++(int);

	void wypisz();


};

PunktF operator+(float a, PunktF pkt);
PunktF operator*(float a, PunktF pkt);


#endif /* PUNKT_F_HPP_ */
