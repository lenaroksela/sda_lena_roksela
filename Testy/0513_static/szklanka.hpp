/*
 * szklanka.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef SZKLANKA_HPP_
#define SZKLANKA_HPP_

class Szklanka
{

private:
	static int mCena;
	int mPojemnosc;
public:
	Szklanka();
	Szklanka(int pojemnosc);

	static int getCena();
	static void setCena( int cena);

	int getPojemnosc() const;
	void setPojemnosc(int pojemnosc);
};



#endif /* SZKLANKA_HPP_ */
