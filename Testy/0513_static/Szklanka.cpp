/*
 * Szklanka.cpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#include "szklanka.hpp"

int Szklanka::mCena = 20;

 int Szklanka::getCena()
{
	return mCena;
}

 void Szklanka::setCena(int cena)
{
	mCena = cena;
}

int Szklanka::getPojemnosc() const
{
	return mPojemnosc;
}

void Szklanka::setPojemnosc(int pojemnosc)
{
	mPojemnosc = pojemnosc;
}

Szklanka::Szklanka() :
		mPojemnosc(0)
{

}

Szklanka::Szklanka(int pojemnosc) :
		mPojemnosc(pojemnosc)
{

}

