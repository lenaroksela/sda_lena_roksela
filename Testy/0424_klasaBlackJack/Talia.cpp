#include "Talia.hpp"
#include <cstdlib>
#include <ctime>

Talia::Talia() :
		mKarty(0), mObecnaKarta(0), mKartaLicznik(0), mPoczatek(-1)
{
	mKartaLicznik = Karta::endKolor * Karta::endFigura;

	mKarty = new Karta[mKartaLicznik];

	for (int k = 0; k < Karta::endKolor; k++)
	{
		for (int f = 0; f < Karta::endFigura; f++)
		{
			mKarty[k * Karta::endFigura + f].setValues((Karta::Kolor) k,
					(Karta::Figura) f);
		}
	}
	std::srand(time(NULL));
}

Talia::~Talia()
{
	delete[] mKarty;
}

void Talia::Tasuj()
{
	for (int i = mKartaLicznik; i > 0; i--)
	{
		int pos = rand() % mKartaLicznik;

		Karta tym = mKarty[i - 1];
		mKarty[i - 1] = mKarty[pos];
		mKarty[pos] = tym;
	}
}

Karta Talia::PobierzKarte()
{
	Karta karta;
	if (mPoczatek < mKartaLicznik)
	{
		mPoczatek++;
		karta = mKarty[mPoczatek];
	}
	else
	{

	}

	return karta;
}
