
#include "gtest/gtest.h"
#include "Karta.hpp"
#include "Talia.hpp"


TEST(KartaTest, tworzenieKartyTest)
{
	Karta karta;
	EXPECT_EQ(0, karta.getWartosc());
	EXPECT_EQ(Karta::endKolor, karta.getKolor());
	EXPECT_EQ(Karta::endFigura, karta.getFigura());
}

TEST(KartaTest, kartSetValue)
{
	Karta karta;
	karta.setValues(Karta::Pik, Karta::As);
	EXPECT_EQ(11, karta.getWartosc());
	EXPECT_EQ(Karta::Pik, karta.getKolor());
	EXPECT_EQ(Karta::As, karta.getFigura());
}

TEST(TaliaTest, PobierzKarteTest)
{
	Talia talia;
	Karta karta = talia.PobierzKarte();
	EXPECT_EQ(Karta::Kier, karta.getKolor());
	EXPECT_EQ(0, karta.getFigura());

}

TEST(TaliaTest, PobierzKarteTest_one)
{
	Talia talia;
	Karta karta = talia.PobierzKarte();
	EXPECT_NE(0, karta.getWartosc());
	EXPECT_NE(Karta::endKolor, karta.getKolor());
	EXPECT_NE(Karta::endFigura, karta.getFigura());

}

int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
