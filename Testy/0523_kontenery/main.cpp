#include <iostream>
#include <string>
#include <list>
#include <iterator>
using namespace std;

bool sortowanieIntMal(const string& first, const string& second)
{
	return first.size() < second.size();
}

bool compare(const int& first, const int& second)
{
	return first < second;
}

bool usun(const int& first)
{
	return first > 5;
}

void wypisz(list<string>& lista)
{
	for (list<string>::iterator it = lista.begin(); it != lista.end(); it++)
	{
		cout << *it << " ";
	}
	cout << endl;
}

void wypisz(list<int>& lista)
{
	for (list<int>::iterator it = lista.begin(); it != lista.end(); it++)
	{
		cout << *it << " ";
	}
	cout << endl;
}
int main()
{
	list<string> listS;
	listS.push_back("Aa ");
	listS.push_back("Bb ");
	listS.push_back("Cc ");

	list<string> listSt;
	listSt.push_back("XX ");
	listSt.push_back("YY ");
	listSt.push_back("VV ");
	listSt.push_back("ZZ ");
//	for (list<string>::iterator it = listS.begin(); it != listS.end(); it++)
//			{
//				cout << *it;
//			}
//	cout<<endl;
	//listS.sort();
	//wypisz(listS);
	listS.sort(sortowanieIntMal);

//	wypisz(listS);

	listS.splice(listS.begin(), listSt, ++listSt.begin(), --listSt.end());
	wypisz(listS);
	wypisz(listSt);
	listS.remove("YY ");
	wypisz(listS);
	//listS.remove_if()

	list<int> listI;
	listI.push_back(3);
	listI.push_back(2);
	listI.push_back(3);
	listI.push_back(3);
	listI.push_back(5);
	listI.push_back(7);
	listI.push_back(7);

	wypisz(listI);

	listI.remove_if(usun);

	wypisz(listI);
	listS.sort();
	wypisz(listI);
	listI.unique();
	wypisz(listI);

	list<int> listInt;
	listInt.push_back(44);
	listInt.push_back(22);
	listInt.push_back(66);
	listInt.push_back(42);
	listInt.push_back(67);
	listInt.push_back(77);
	listInt.push_back(78);

	wypisz(listI);
	wypisz(listInt);

	listI.merge(listInt, compare);
//	listI.merge(listInt);
	wypisz(listI);
	wypisz(listInt);
	return 0;
}
