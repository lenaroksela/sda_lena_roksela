/*
 * main.cpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */
#include <iostream>

#include "kolejka.hpp"

int main()
{
	KolejkaPriorytetowa<int> kolejka;

	kolejka.push(15,5);
	kolejka.push(13,3);
	kolejka.push(12,2);
	kolejka.push(11,1);
	kolejka.push(14,4);

	for(size_t i=0; i<kolejka.size();i++)
	{
		std::cout<<kolejka.pop()<<" "<<kolejka.pop()<<" "<<kolejka.pop()<<" "<<kolejka.pop()<<" "<<kolejka.pop();
	}
	return 0;
}



