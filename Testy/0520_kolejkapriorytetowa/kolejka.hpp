/*
 * kolejka.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef KOLEJKA_HPP_
#define KOLEJKA_HPP_
#include <cstdlib>
#include <iostream>
template<typename T>
class KolejkaPriorytetowa
{
public:
	struct Node
	{
		T mValue;
		Node * mNext;
		int mPriority;

		Node(const T& value, int prio) :
				mValue(value), mNext(0), mPriority(prio)
		{
		}

	};

	size_t mSize;

	Node * mHead;

public:

	KolejkaPriorytetowa() :
			mSize(), mHead()
	{

	}

	void push(const T& value, int priorytet)
	{
		Node * nowy = new Node(value, priorytet);

		if (empty())
		{
			mHead = nowy;
			mSize++;
		}
		else if (mSize == 1)
		{
			if (mHead->mPriority > nowy->mPriority)
			{
				nowy->mNext = mHead;
				mHead = nowy;
				mSize++;
			}
			else
			{
				nowy->mNext = mHead->mNext;
				mHead->mNext = nowy;
				mSize++;
			}

		}
		else
		{
			Node* current = mHead;
			while (current->mNext != 0
					&& current->mNext->mPriority > nowy->mPriority)
			{
				current = current->mNext;
			}
			nowy->mNext = current->mNext;
			current->mNext = nowy;
			mSize++;
		}

	}

	T pop()
	{
		if(mSize==0)
		{
			std::cout<<"Puste\n";
			return 0;
		}
		else
		{
		T tmp= mHead->mValue;
		Node * tmpNode = mHead->mNext;
		delete mHead;
		mHead=tmpNode;
		mSize--;
		return tmp;
		}
	}

	size_t size()
	{
		return mSize;
	}

	bool empty()
	{
		return (mSize == 0) ? true : false;
	}

};

#endif /* KOLEJKA_HPP_ */
