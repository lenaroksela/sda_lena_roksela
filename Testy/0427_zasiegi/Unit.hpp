/*
 * Unit.hpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */

#ifndef UNIT_HPP_
#define UNIT_HPP_
#include <string>

class Group;

class Unit
{
	std::string mId;
	Group * mGroupPtr;

public:

	Unit(std::string id, Group * group);

	void printId();
	void replicate();
	void addToGroup(Group * group);

	const std::string& getId() const;
	void setId(const std::string& id);
};



#endif /* UNIT_HPP_ */
