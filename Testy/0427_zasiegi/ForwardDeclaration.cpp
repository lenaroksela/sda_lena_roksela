/*
 * ForwardDeclaration.cpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */
#include "Unit.hpp"
#include "Group.hpp"
#include <ctime>
#include <cstdlib>
#include <iostream>

int main()
{
	srand(time(NULL));

	Group armyOne;
	new Unit("1",&armyOne);
//	Unit* motherOne = new Unit("1",&armyOne);


//	armyOne.add(motherOne);
//	motherOne->addToGroup(&armyOne);

	new Unit("a", &armyOne);

//	armyOne.add(motherTwo);
//	motherTwo->addToGroup(&armyOne);

	armyOne.printUnits();
	std::cout<<"---------\n";

	armyOne.replicateGroup();
	armyOne.printUnits();
	std::cout<<"---------\n";

	armyOne.replicateGroup();
	armyOne.printUnits();
	std::cout<<"---------\n";
//
//	armyOne.replicateGroup();
//	armyOne.printUnits();
//	std::cout<<"---------\n";

	return 0;
}



