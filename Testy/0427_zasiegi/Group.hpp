

#ifndef GROUP_HPP_
#define GROUP_HPP_

class Unit;

class Group
{
	unsigned int mSize;
	Unit** mUnits;

	void resize();

public:

	Group();
	~Group();

	void add(Unit* unit);
	void clear();
	void replicateGroup();
	void printUnits();

	unsigned int getSize() const;
	void setSize(unsigned int size);
};



#endif /* GROUP_HPP_ */
