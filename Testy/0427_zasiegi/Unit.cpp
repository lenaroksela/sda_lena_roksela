/*
 * Unit.cpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */
#include "Unit.hpp"
#include "Group.hpp"
#include <iostream>
#include <ctime>
#include <cstdlib>

void Unit::printId()
{
	std::cout << "Unit's ID: " << getId() << std::endl;
}

void Unit::replicate()
{
	int newIdGenerate = rand() % 10;
	char newId=  newIdGenerate +'0';

//	Unit* newUnit = new Unit(mId + newId, mGroupPtr);
	new Unit(mId + newId, mGroupPtr);
//	newUnit->addToGroup(mGroupPtr);
//	mGroupPtr->add(newUnit);
}

void Unit::addToGroup(Group * group)
{
	mGroupPtr = group;
	mGroupPtr->add(this);
}

Unit::Unit(std::string id, Group * group) :
		mId(id)
{
	addToGroup(group);
}

const std::string& Unit::getId() const
{
	return mId;
}

void Unit::setId(const std::string& id)
{
	mId = id;
}
