#include <iostream>
#include "lista.hpp"
#include "Bunny.hpp"
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

using namespace std;

string getRandomName()
{
	static const size_t nameCount = 7;
	static string names[nameCount] = { "Bibi", "Fluffy", "Death", "Blood", "Candy", "Arni", "C" };
	int random = rand() % nameCount;

	return names[random];
}

bool checkIfMaleIsAvailable(TwoWayList<Bunny>& bunnyList)
{
	bool isMaleFound = false;
	Bunny* tmpBunny;
	for (size_t i = 0; i < bunnyList.size(); i++)
	{
		tmpBunny = bunnyList.getValue(i);
		if (Bunny::Male == tmpBunny->getSex() && tmpBunny->getAge() >= 2)
		{
			isMaleFound = true;
			break;
		}
	}

	return isMaleFound;
}

void breed(TwoWayList<Bunny>& bunnyList)
{
	if (checkIfMaleIsAvailable(bunnyList))
	{
		Bunny* tmpBunny;
		for (size_t i = 0; i < bunnyList.size(); i++)
		{
			tmpBunny = bunnyList.getValue(i);
			if (Bunny::Female == tmpBunny->getSex() && tmpBunny->getAge() >= 2)
			{
				bunnyList.addEnd(Bunny(getRandomName(), tmpBunny->getColor()));
				cout<<"Narodzil sie "<<bunnyList.getValue(bunnyList.size()-1)->getName()<<endl;
			}
		}
	}
}

int countMutated(TwoWayList<Bunny>& bunnyList)
{
	int count = 0;
	for (size_t i = 0; i < bunnyList.size(); i++)
	{
		if (bunnyList.getValue(i)->isMutant())
		{
			count++;
		}
	}

	return count;
}

void spreadMutation(TwoWayList<Bunny>& bunnyList)
{
	int mutantCount = countMutated(bunnyList);

	Bunny* tmpKrolik = 0;
	while(mutantCount>0)
	{
		size_t random = rand() % bunnyList.size();
		tmpKrolik = bunnyList.getValue(random);
		if(!tmpKrolik->isMutant())
		{
			tmpKrolik->setMutant(true);
			mutantCount--;
			cout<<"Krolik "<<tmpKrolik->getName()<<" zmutowa�!"<<endl;
		}
	}
}

void killHalf(TwoWayList<Bunny>& bunnyList)
{
	size_t bunniesToKill = bunnyList.size() / 2;
	cout << "Potezny glod panuje. Zdechli:" << endl;
	Bunny* tmpKrolik;
	while (bunniesToKill != 0)
	{
		size_t random = rand() % bunnyList.size();
		tmpKrolik = bunnyList.getValue(random);
		cout << tmpKrolik->getName() << " wiek " << tmpKrolik->getAge() << endl;
		bunnyList.deletePosition(random);
	}
}

void update(TwoWayList<Bunny>& bunnyList)
{
	if (bunnyList.size() > 1000)
	{
		killHalf (bunnyList);
	}

	for (size_t i = 0; i < bunnyList.size(); i++)
	{
		if (bunnyList.getValue(i)->happyBirthay())
		{
			cout << "Krolik " << bunnyList.getValue(i)->getName() << " zdech�."
					<< endl;
			i--;
		}
	}

	breed(bunnyList);
	spreadMutation(bunnyList);
}

int main()
{
	srand(time(NULL));

	TwoWayList<Bunny> kroliczkowo;
	kroliczkowo.addEnd(Bunny(getRandomName()));
	kroliczkowo.addEnd(Bunny(getRandomName()));
	kroliczkowo.addEnd(Bunny(getRandomName()));
	kroliczkowo.addEnd(Bunny(getRandomName()));
	kroliczkowo.addEnd(Bunny(getRandomName()));

	while (!kroliczkowo.empty())
	{
		update(kroliczkowo);
	}

	cout << "Nie ma kroliczkow :(" << endl;

	return 0;
}
