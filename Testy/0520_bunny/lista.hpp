/*
 * lista.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef LIST_HPP_
#define LIST_HPP_
#include <iostream>
#include <exception>
#include <cstdlib>
#include "Bunny.hpp"
template<typename Type>
class TwoWayList
{
	struct Node
	{
		Type* mValue;
		Node* mNext;
		Node* mPrevious;
		Node() :
				mValue(0), mNext(0), mPrevious(0)
		{
		}
		Node(Type* value) :
				mValue(value), mNext(0), mPrevious(0)
		{
		}
	};
	Node* mHead;
	Node* mTail;
	size_t mSize;
public:
	TwoWayList() :
			mHead(0), mTail(0), mSize(0)
	{
	}
	~TwoWayList()
	{
		clear();
	}

	size_t size()
	{
		return mSize;
	}

	const std::ostream& operator<<(std::ostream& os) const
		    {
		        if(mSize==0)
		        {
		            os<<"Empty!"<<std::endl;
		            return os;
		        }
		        else
		        {
		            Node* newNode=mHead;
		            std::cout<<"[ ";
		            for(size_t i=0; i<mSize; i++)
		            {
		               // os<<*newNode->mValue<<", ";
		                newNode=newNode->mNext;
		            }
		            std::cout<<" ]"<<std::endl;
		            return os;
		        }
		    }
	bool empty()
	{
		return  mSize==0?true:false;
	}
	void addFront(const Type& value)
	{
		Type* newValue = new Type(value);
		if (mHead == 0)
		{
			Node* newNode = new Node(newValue);
			mHead = newNode;
			mTail = newNode;
			mSize++;
		}
		else
		{
			Node* newNode = new Node(newValue);
			newNode->mNext = mHead;
			mHead->mPrevious = newNode;
			mHead = newNode;
			mSize++;
		}
	}
	void addEnd(const Type& value)
	{
		Type* newValue = new Type(value);
		if (mTail == 0)
		{
			Node* newNode = new Node(newValue);
			mHead = newNode;
			mTail = newNode;
			mSize++;
		}
		else
		{
			Node* newNode = new Node(newValue);
			mTail->mNext = newNode;
			newNode->mPrevious = mTail;
			mTail = newNode;
			mSize++;
		}
	}

	void deleteEnd()
	{
		if (mSize == 0)
		{
			std::cout << "Empty!" << std::endl;
		}
		else
		{
			Node* newNode = mTail;
			mTail->mPrevious->mNext = 0;
			mTail = mTail->mPrevious;
			delete newNode;
			mSize--;
		}
	}
	void deleteFront()
	{
		if (mSize == 0)
		{
			std::cout << "Empty!" << std::endl;
		}
		else
		{
			Node* newNode = mHead;
			mHead->mNext->mPrevious = 0;
			mHead = mHead->mNext;
			delete newNode;
			mSize--;
		}
	}
	void deletePosition(size_t position)
	{
		if (mSize == 0)
		{
			std::cout << "Empty!" << std::endl;
		}
		else if (position == 1)
		{
			deleteFront();
		}
		else if (position == mSize)
		{
			deleteEnd();
		}
		else if (position > mSize || position == 0)
		{
			throw std::out_of_range("deletePosition: out of range!");
		}
		else
		{
			Node* newNode = mHead;
			while (position--)
			{
				newNode = newNode->mNext;
			}
			newNode->mPrevious->mNext = newNode->mNext;
			newNode->mNext->mPrevious = newNode->mPrevious;
			delete newNode;
			mSize--;
		}
	}
	Type* getValue(size_t position) const
	{
		if (mSize == 0)
		{
			std::cout << "Empty!" << std::endl;
		}
		else if (mSize == position)
		{
			return mTail->mValue;
		}
		else if (position == 1)
		{
			return mHead->mValue;
		}
		else if (position < mSize && position != 0)
		{
			Node* newNode = mHead;
			while (--position)
			{
				newNode = newNode->mNext;
			}
			return newNode->mValue;
		}
		else
		{
			throw std::out_of_range("getValue: out of range!");
		}
		return 0;
	}

	void clear()
	{
		if (mSize == 0)
		{
			std::cout << "Empty!" << std::endl;
		}
		else
		{
			Node* newNode;
			while (mHead != 0)
			{
				newNode = mHead;
				mHead = mHead->mNext;
				delete newNode;
			}
			mSize = 0;
			mHead = 0;
			mTail = 0;
		}
	}
//  friend void operator<<(std::ostream& os, TwoWayList<Type>& newOne);

};
const std::ostream& operator<<(std::ostream& os, TwoWayList<Bunny>& newOne)
{
    return newOne<<os;
}

#endif /* LISTA_HPP_ */
