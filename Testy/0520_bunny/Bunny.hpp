/*
 * Bunny.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef BUNNY_HPP_
#define BUNNY_HPP_
#include <string>
#include <ctime>
#include <cstdlib>
#include "lista.hpp"
using namespace std;

class Bunny
{
public:
	enum Sex
	{
		Male, Female
	};

	enum Color
	{
		Black = 0, White, Spotted, Gray
	};

	Bunny(string name) :
			mAge(0), mName(name)
	{
		srand(time(NULL));
		mSex = randomizeSex();
		mColor = randomizeColor();
		mMutant = randomizeMutant();
	}

	Bunny(string name, Color color) :
			mColor(color), mAge(0), mName(name)
	{
		mSex = randomizeSex();
		mMutant = randomizeMutant();
	}

	bool happyBirthay()
	{
		if (isMutant())
		{
			return mAge == 50 ? true : false;
		}
		else
		{
			return mAge == 10 ? true : false;
		}
		//return true if bunny is dead
		//10 for normal
		//50 for mutant bunny
	}

	Color getColor() const
	{
		return mColor;
	}

	bool isMutant() const
	{
		return mMutant == true ? true : false;
	}

	bool isMale() const
	{
		return mSex == Male ? true : false;
	}

	void setMutant(bool mutant)
	{
		mMutant = mutant;
	}

	const string& getName() const
	{
		return mName;
	}

	Sex getSex() const
	{
		return mSex;
	}

	int getAge() const
		{
			return mAge;
		}
private:
	Sex mSex;
	Color mColor;
	unsigned int mAge;
	string mName;
	bool mMutant;

	Sex randomizeSex()
	{
		int losowa = 1 + rand() % 2;

		if (losowa == 1)
		{
			return Male;
		}
		else
		{
			return Female;

		}

	}
	bool randomizeMutant()
	{
		int losowa = 1 + rand() % 100;
		if (losowa == 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	Color randomizeColor()
	{
		int losowa = 1 + rand() % 4;
		if (losowa == 1)
		{
			return White;
		}
		else if (losowa == 2)
		{
			return Black;
		}
		else if (losowa == 3)
		{
			return Spotted;
		}
		else
		{
			return Gray;

		}
	}
};

#endif /* BUNNY_HPP_ */
