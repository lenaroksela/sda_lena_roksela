#include  <iostream>

#include "ser.hpp"
#include "ser_bialy.hpp"
#include "ser_zolty.hpp"

void sertuj(Ser** tab, int rozmiar)
{

	SerBialy** bialaTab = new SerBialy*[rozmiar];
	SerZolty** zoltaTab = new SerZolty*[rozmiar];

	int ileBialych = 0;
	int ileZoltych = 0;

	for (int i = 0; i < rozmiar; i++)
	{
		if (dynamic_cast<SerBialy*>(tab[i]))
		{
			bialaTab[ileBialych] = dynamic_cast<SerBialy*>(tab[i]);
			ileBialych++;
		}
		else if (dynamic_cast<SerZolty*>(tab[i]))
		{
			zoltaTab[ileZoltych] = dynamic_cast<SerZolty*>(tab[i]);
			ileZoltych++;
		}
	}
	std::cout << "Bialy ser - cena i rodzaj. Bialych serow mamy: " << ileBialych
			<< std::endl;
	for (int i = 0; i < ileBialych; i++)
	{

		bialaTab[i]->podajCene();
		bialaTab[i]->podajRodzaj();
	}
	std::cout << "Zolty  ser - cena i wiek. Zoltych serow mamy: " << ileZoltych
			<< std::endl;
	for (int i = 0; i < ileZoltych; i++)
	{
		zoltaTab[i]->podajCene();
		zoltaTab[i]->podajWiek();
	}

	delete [] bialaTab;
	delete [] zoltaTab;
}
int main()
{
	Ser* sery[8];

	sery[0] = new SerBialy(10, SerBialy::Chudy);
	sery[1] = new SerZolty(301, 23);
	sery[2] = new SerZolty(10, 1);
	sery[3] = new SerBialy(8, SerBialy::Tlusty);
	sery[4] = new SerBialy(9, SerBialy::Tlusty);
	sery[5] = new SerBialy(15, SerBialy::PolTlusty);
	sery[6] = new SerBialy(13, SerBialy::Chudy);
	sery[7] = new SerZolty(2000, 33);

	sertuj(sery, 8);

	for(int i=0;i<8;i++)
	{
		delete sery[i];
	}

	return 0;
}
