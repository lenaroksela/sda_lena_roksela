/*
 * ser_bialy.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef SER_BIALY_HPP_
#define SER_BIALY_HPP_
#include "ser.hpp"
#include <iostream>
#include <string>

class SerBialy: public Ser
{
public:
	enum Rodzaj
	{
		Chudy, PolTlusty, Tlusty
	};

	Rodzaj mRodzaj;
//	int mCena;
public:
	SerBialy() :
			Ser(), mRodzaj()
	{

	}
//	SerBialy(int cena, Rodzaj rodzaj)
//:Ser(cena),mRodzaj(rodzaj)
//	{
//
//	}

	SerBialy(int cena, Rodzaj rodzaj) :
			Ser(cena), mRodzaj(rodzaj)
	{

	}
	~SerBialy()
	{

	}
	void podajCene()
	{
		std::cout << "Cena " << mCena<<" ";
	}
	void podajRodzaj()
	{
		std::cout << "Rodzaj  " << wypiszRodzaj() <<" \n";
	}

	std::string wypiszRodzaj()
		{
			switch(mRodzaj)
			{
			case Chudy:
				return "Chudy";
				break;
			case PolTlusty:
				return "PolTlusty";
				break;
			case Tlusty:
				return "Tlusty";
				break;
			}
			return "";
		}


};

#endif /* SER_BIALY_HPP_ */
