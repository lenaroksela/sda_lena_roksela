/*
 * ser.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef SER_HPP_
#define SER_HPP_
#include <iostream>
class Ser
{
protected:
	int mCena;
public:
	Ser()
:mCena(0)
	{

	}
	Ser(int cena)
	:mCena(cena)
		{

		}
	virtual ~Ser()
	{

	}

	virtual void podajCene()
	{
		std::cout<<"Cena "<<mCena;
	}
};



#endif /* SER_HPP_ */
