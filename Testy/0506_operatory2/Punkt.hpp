/*
 * Punkt.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef PUNKT_HPP_
#define PUNKT_HPP_

class Punkt
{

	int mX;
	int mY;

public:

	Punkt(int x, int y);
	Punkt ();


	Punkt operator*(int x);
	void operator!();

	int getX() const;
	void setX(int x);
	int getY() const;
	void setY(int y);
	void wypisz();
};



#endif /* PUNKT_HPP_ */
