/*
 * singleton.cpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#include "singleton.hpp"

 Singleton * Singleton::mSingleton=0;

Singleton::Singleton()
:mValue(0)
{

}

int Singleton::getValue() const
{
	return mValue;
}

void Singleton::setValue(int value)
{
	mValue = value;
}

Singleton* Singleton::getInstance()
{
	if(!mSingleton)
	{
		mSingleton=new Singleton;
		mSingleton->setValue(1);
		return mSingleton;
	}

}
