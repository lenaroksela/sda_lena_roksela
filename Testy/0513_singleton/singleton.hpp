/*
 * singleton.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef SINGLETON_HPP_
#define SINGLETON_HPP_

class Singleton
{
	static Singleton * mSingleton;
	int mValue;
	Singleton();

public:
	static Singleton* getInstance();
	int getValue() const;
	void setValue(int value);
};



#endif /* SINGLETON_HPP_ */
