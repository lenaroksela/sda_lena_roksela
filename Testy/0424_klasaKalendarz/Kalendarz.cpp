/*
 * Kalendarz.cpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#include "Kalendarz.hpp"

Kalendarz::Kalendarz() :
		mDzien(0), mMiesiac(0), mRok(0), czyPoprawny(false)
{

}
bool Kalendarz::czyPoprawnaData(int dzien, int miesiac, int rok)
{
	if (czyPoprawny == true)
	{
		setDzien(dzien);
		setMiesiac(miesiac);
		setRok(rok);
		return true;
	}
	else
	{
		return false;
	}
}
bool Kalendarz::czyPoprawneDni(int dzien, int miesiac, int rok)
{
	bool zmienna;
	if (czyPrzestepny(rok))
	{
		if (miesiac <= 29)
		{
			zmienna = true;
		}
		else
		{
			zmienna = false;
		}
	}

	return zmienna;
}

int Kalendarz::getDzien() const
{
	return mDzien;
}

void Kalendarz::setDzien(int dzien)
{
	if (dzien < 0)
	{
		dzien = 1;
		mDzien = dzien;
		czyPoprawny = false;
	}
	else if (dzien > 31)
	{
		dzien = 1;
		mDzien = dzien;
		czyPoprawny = false;
	}
	else
	{

		mDzien = dzien;
		czyPoprawny = true;
	}
}

int Kalendarz::getMiesiac() const
{
	return mMiesiac;
}

void Kalendarz::setMiesiac(int miesiac)
{
	if (miesiac < 0)
	{
		miesiac = 1;
		mMiesiac = miesiac;
		czyPoprawny = false;
	}
	else if (miesiac > 12)
	{
		miesiac = 1;
		mMiesiac = miesiac;
		czyPoprawny = false;
	}
	else
	{
		mMiesiac = miesiac;
		czyPoprawny = true;

		mMiesiac = miesiac;
	}
}

int Kalendarz::getRok() const
{

	return mRok;
}

void Kalendarz::setRok(int rok)
{
	if (rok < 1900)
	{
		rok = 1970;
		mRok = rok;
		czyPoprawny = false;
	}

	else
	{
		mRok = rok;
		czyPoprawny = true;

		mRok = rok;
	}

}

bool sprawdzDzien()
{
	return true;
}
bool sprawdzMiesiac(int miesiac, int dzien, int rok)
{
	switch (miesiac)
	{
	case 1:
	case 3:
	case 5:
	case 7:
	case 8:
	case 10:
	case 12:
	{
		if (dzien <= 31)
		{
			return true;

		}
		break;
	}
	case 4:
	case 6:
	case 9:
	case 11:
	{
		if (dzien <= 30)
		{
			return true;
		}
		break;
	}

	case 2:
	{
		if (dzien <= 29)
		{
			return true;
		}
		break;
	}
	default:
	{
		return false;
		break;
	}
	}
return false;
}
bool Kalendarz::czyPrzestepny(int rok)
{

	if ((rok % 4 == 0) && (rok % 100 != 0))
	{
		return true;
	}
	else if (rok % 400 == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
