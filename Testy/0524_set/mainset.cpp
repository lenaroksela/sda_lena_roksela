#include <iostream>
#include <string>
#include <set>

using namespace std;

void wypisz(set<int>& setI)
{
	for (set<int>::iterator it = setI.begin(); it != setI.end(); it++)
	{
		cout << *it << " ";
	}
	cout << endl;
}

void wypisz(set<string>& setI)
{
	for (set<string>::iterator it = setI.begin(); it != setI.end(); it++)
	{
		cout << *it << " ";
	}
	cout << endl;
}



int main()
{
	set<int> setI;
	setI.insert(99);
	setI.insert(1);
	setI.insert(11);
	setI.insert(2);
	setI.insert(12);
	setI.insert(12);
	setI.insert(13);
	wypisz(setI);

//	setI.find(12);

	set<string> Imiona;
	Imiona.insert("Fluffy");
	Imiona.insert("Death");
	Imiona.insert("War");
	Imiona.insert("God");
	Imiona.insert("Kowalski");
	Imiona.insert("Dark Lord");
	Imiona.insert("Pimpus");
	wypisz(Imiona);

	string tabSTring[]={"Miau", "Hau", "Wow"};
	Imiona.insert(tabSTring, tabSTring+3);
	wypisz(Imiona);
	Imiona.find("Pimpus");

	return 0;
}



