#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main()
{
	vector<int> vectInt(10, 2);
	vector<string> vectString(14, "Vector ");

	vector<string> vectString2;
	vectString2.push_back("Ala ");
	vectString2.push_back("ma ");
	vectString2.push_back("kota");
	vectString2.push_back(".");

	cout << vectInt.capacity() << " " << vectInt.size() << " "
			<< vectInt.max_size() << " \n";

//	vectInt.reserve(1000);
//	cout << vectInt.capacity() << " " << vectInt.size() << " "
//				<< vectInt.max_size() << " \n";
//	vectInt.resize(140,12);
//	cout << vectInt.capacity() << " " << vectInt.size() << " "
//				<< vectInt.max_size() << " \n";
//
//	cout << vectString.capacity() << " " << vectString.size() << " "
//			<< vectString.max_size() << " \n";
//
//	cout << vectString2.capacity() << " " << vectString2.size() << " "
//			<< vectString2.max_size() << " \n";

	for (unsigned int i = 0; i < vectInt.size(); i++)
	{
		cout << vectInt[i];
	}
	cout << endl;
//
//	for (unsigned int i = 0; i < vectString.size(); i++)
//	{
//		cout << vectString[i];
//	}
//	cout << endl;
//
//	for (unsigned int i = 0; i < vectString2.size(); i++)
//	{
//		cout << vectString2.at(i);
//	}
//	cout << endl;

	cout << vectInt.empty();

	while (!vectInt.empty())
	{
		cout << vectInt.back() << " ";
		vectInt.pop_back();

	}

	while (!vectString2.empty())
	{
		cout << vectString2.back() << " ";
		vectString2.pop_back();
		cout << endl;

	}



	return 0;
}
