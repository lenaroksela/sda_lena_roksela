#include <iostream>
#include <string>
#include <stack>
#include <vector>

using namespace std;

string odwroc(string & slowo)
{
	stack<char, vector<char> > stos;
	string reverse;

	for (unsigned int i = 0; i < slowo.size(); i++)
	{
		stos.push(slowo[i]);
	}

	for (unsigned int i = 0; i < slowo.size(); i++)
	{
		reverse += stos.top();
		stos.pop();
	}

	return reverse;
}

int main()
{
	string napis = "nie panikuj";

	cout << napis << endl;
	cout << odwroc(napis) << endl;

	return 0;
}

