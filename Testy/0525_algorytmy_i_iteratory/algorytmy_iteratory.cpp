#include <iostream>
#include <string>
#include <ostream>
#include <deque>
#include <algorithm>
#include <iterator>
#include <list>

using namespace std;

void wypisz(deque<int>& lista)
{
	for (deque<int>::iterator it = lista.begin(); it != lista.end(); it++)
	{
		cout << *it << " ";
	}
	cout << endl;
}

void wypisz(list<float>& lista)
{
	for (list<float>::iterator it = lista.begin(); it != lista.end(); it++)
	{
		cout << *it << " ";
	}
	cout << endl;
}

void printInt(float x)
{
	cout << static_cast<int>(x) << " ";

}

void printElem(float x)
{
	cout << x << " ";

}

void castInt(float& x)
{
	x = static_cast<int>(x);

}
//functora ktory sumuje elemnty listy, swietnie dziala z for_each

struct Suma
{
	Suma():sum(0){}
	void operator()(float x)
			{
				sum+=x;
			}
	float sum;
};
int main()
{
	deque<int> liczbyMniejsze;
	deque<int> liczbyWieksze;
	deque<int> liczbyNaj;
	for (int i = 1; i < 6; i++)
	{
		liczbyMniejsze.push_back(i);
		liczbyWieksze.push_back(i * 10);
		liczbyNaj.push_back(i * 100);
	}

	front_insert_iterator<deque<int> > frontIt(liczbyMniejsze);
	copy(liczbyWieksze.begin(), liczbyWieksze.end(), frontIt);

//	wypisz(liczbyMniejsze);

	copy(liczbyMniejsze.begin(), liczbyMniejsze.end(),
			back_inserter(liczbyNaj));

//	wypisz(liczbyNaj);

	list<float> num;
	num.push_back(0.3);
	num.push_back(1.42);
	num.push_back(7.14);
	num.push_back(3.66);
	num.push_back(42.09009);
	num.push_back(2.11);

	for_each(num.begin(), num.end(), printInt);
	cout << endl;
	for_each(num.begin(), num.end(), printElem);
	cout << endl;
	for_each(num.begin(), num.end(), castInt);
	for_each(num.begin(), num.end(), printInt);
	cout << endl;

	Suma suma = for_each(num.begin(), num.end(), Suma());
	cout<<suma.sum;

	return 0;
}

