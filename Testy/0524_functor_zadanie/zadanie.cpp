#include <iostream>
#include <string>
#include <set>

using namespace std;

bool compare(const float& first, const float& second)
{
	return first != second;
}

struct Functor
{
	bool operator()(float first, float second)
	{
		return static_cast<int>(first) < static_cast<int>(second);
	}

};

int main()
{
	set<float, Functor> liczby;

	liczby.insert((1.2f));
	liczby.insert((2.2f));
	liczby.insert((3.1f));
	liczby.insert((3.2f));
	liczby.insert((3.9f));

	for (set<float>::iterator it = liczby.begin(); it != liczby.end(); it++)
	{
		cout << *it << " ";
	}
	cout << endl;

	set<float>::iterator it;
	it = liczby.find(1.2f);
	liczby.erase(it);

	for (set<float>::iterator it = liczby.begin(); it != liczby.end(); it++)
	{
		cout << *it << " ";
	}

	return 0;

}

