/*
 * BuforCykliczny.hpp
 *
 *  Created on: 17.05.2017
 *      Author: RENT
 */

#ifndef BUFORCYKLICZNY_HPP_
#define BUFORCYKLICZNY_HPP_
#include <cstdlib>
#include <iostream>
#include <string>
#include <exception>

template<typename T>

class Buffor
{
private:
	struct Node
	{
		T value;
		Node * next;

		Node(T value)
		{
			this->value = value;
			next = 0;
		}

		void over(T value)
		{
			this->value=value;
		}
	};

	Node * readPtr;
	Node * addPtr;

	size_t mSize;
	size_t mMaxSize;

public:

	Buffor(size_t maxsize) :
			readPtr(0), addPtr(0), mSize(0), mMaxSize(maxsize)
	{

	}
	void add(const T& element)
	{
		if (mSize == 0)
		{

			Node * tmpPtr = new Node(element);
			addPtr = tmpPtr;
			readPtr = tmpPtr;
			tmpPtr->next = tmpPtr;
			mSize++;
		}
		else if (mSize < mMaxSize)
		{
			Node * tmpPtr = new Node(element);
			tmpPtr->next = addPtr->next;
			addPtr->next = tmpPtr;
			addPtr = tmpPtr;
			mSize++;
		}
		else if (mSize == mMaxSize)
		{
			addPtr=addPtr->next;
			addPtr->over(element);
		}
		else
		{

		}
	}
	void clear()
	{

	}

	int maxSize()
	{
		return mMaxSize;
	}

	int size()
	{
		return mSize;
	}

	T& next()
	{
		if (readPtr == 0)
		{
			throw std::out_of_range("Pusta");
		}
		else
		{
			readPtr = readPtr->next;
			return readPtr->value;
		}
	}
};

#endif /* BUFORCYKLICZNY_HPP_ */
