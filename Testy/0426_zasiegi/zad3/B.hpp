/*
 * B.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef ZAD3_B_HPP_
#define ZAD3_B_HPP_

//B.h
#ifndef B_H
#define B_H

class A;
class B
{
    double _val;
    A* _a;
public:

    B(double val);
    ~B();
    void SetA(A *a);
    void Print();
};
#endif



#endif /* ZAD3_B_HPP_ */
