/*
 * A.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef ZAD3_A_HPP_
#define ZAD3_A_HPP_

//A.h
#ifndef A_H
#define A_H

class B;
class A
{
    int _val;
    B* _b;
public:

    A(int val);
    void SetB(B *b);
    void Print();
    ~A();
};
#endif








#endif /* ZAD3_A_HPP_ */
