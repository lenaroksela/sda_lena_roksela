/*
 * main_zad3.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */



//main.cpp
#include "A.hpp"
#include "B.hpp"
#include <iostream>

 void separator()
    {
    	std::cout<<"-----\n";
    }

int main_3(int argc, char* argv[])
{

    A a(10);
    B b(3.14);


    separator();
    a.Print();

    separator();
    a.SetB(&b);

    separator();
    b.Print();

    separator();
    b.SetA(&a);

    separator();
    return 0;
}


