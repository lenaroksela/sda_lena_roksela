/*
 * Kolor.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef ZAD4_KOLOR_HPP_
#define ZAD4_KOLOR_HPP_
#include <string>

namespace KolorowaGeometria
{
	namespace Kolor
	{
	 enum Kolor
	 {
		 czarny,
		 czarniejszy,
		 nieczarny
	  };

	 std::string convertToString(Kolor kolor);

	}
}
#endif /* ZAD4_KOLOR_HPP_ */
