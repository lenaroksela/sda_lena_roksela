/*
 * Figura.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef ZAD4_FIGURA_HPP_
#define ZAD4_FIGURA_HPP_

#include <string>
namespace KolorowaGeometria
{
	namespace Geometria
	{
		class Figura
		{
			std::string mKolor;
			std::string mNazwa;

		public:
			virtual void wypisz()=0;
			virtual ~Figura(){};
		};

	}
}
#endif /* ZAD4_FIGURA_HPP_ */
