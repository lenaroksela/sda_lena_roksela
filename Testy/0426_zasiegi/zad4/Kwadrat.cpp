/*
 * Kwadrat.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#include "Kwadrat.hpp"
#include <iostream>

KolorowaGeometria::Geometria::Kwadrat::Kwadrat ():mKolor("kolor"),mNazwa("nazwa"),mDlugoscBoku(0)
{}

KolorowaGeometria::Geometria::Kwadrat::Kwadrat (std::string kolor, std::string nazwa, int bok)
:mKolor(kolor),mNazwa(nazwa),mDlugoscBoku(bok)
{}

KolorowaGeometria::Geometria::Kwadrat::~Kwadrat()
{}

void KolorowaGeometria::Geometria::Kwadrat::wypisz()
	{
		std::cout << mKolor << " " << mNazwa << " " << pole() << std::endl;
	}

int KolorowaGeometria::Geometria::Kwadrat::pole()
	{
		return mDlugoscBoku*mDlugoscBoku;
	}
