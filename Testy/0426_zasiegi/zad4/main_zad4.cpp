/*
 * main_zad4.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#include <iostream>

#include "Kolor.hpp"
#include "Kwadrat.hpp"
#include "Kolo.hpp"


//using namespace Kolor;
using namespace KolorowaGeometria;
using namespace Geometria;





int main()
{
	std::string czarniejszy = Kolor::convertToString(Kolor::czarniejszy);
	Kwadrat kwadrat(czarniejszy, "Kwadrat", 10);
	kwadrat.wypisz();

	std::string czarny = Kolor::convertToString(Kolor::czarny);
	Kolo kolo(czarny, "Kolo", 10);
	kolo.wypisz();

	Figura *figura;

	figura = &kwadrat;
	figura->wypisz();

	figura = &kolo;
	figura->wypisz();

	//std::cout<<convertToString(czarniejszy);
	std::cout<<Kolor::convertToString(Kolor::czarniejszy);

	return 0;
}

