/*
 * Kwadrat.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef ZAD4_KWADRAT_HPP_
#define ZAD4_KWADRAT_HPP_

#include "Figura.hpp"
namespace KolorowaGeometria
{
	namespace Geometria
	{
		class Kwadrat : public KolorowaGeometria::Geometria::Figura
		{
			std::string mKolor;
			std::string mNazwa;
			int mDlugoscBoku;

		public:
			Kwadrat();
			Kwadrat(std::string kolor, std::string nazwa, int bok);
			~Kwadrat();
			void wypisz();
			int pole();
		};
	}
}
#endif /* ZAD4_KWADRAT_HPP_ */
