/*
 * Kolo.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef ZAD4_KOLO_HPP_
#define ZAD4_KOLO_HPP_

#include "Figura.hpp"

namespace KolorowaGeometria
{
	namespace Geometria
	{
		class Kolo : public KolorowaGeometria::Geometria::Figura
		{
			std::string mKolor;
			std::string mNazwa;
			int mPromien;

		public:
			Kolo();
			Kolo(std::string kolor, std::string nazwa, int promien);
			~Kolo();
			void wypisz();
			int pole();
		};
	}
}
#endif /* ZAD4_KOLO_HPP_ */
