/*
 * Kolor.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */
#include "Kolor.hpp"

std::string KolorowaGeometria::Kolor::convertToString(Kolor kolor)
{
	std::string kolorPomocna;
	switch (kolor)
	{
		case czarny:
		{
			kolorPomocna = "Czarny";
			break;
		}
		case czarniejszy:
		{
			kolorPomocna = "Czarniejszy";
			break;
		}

		case nieczarny:
		{
			kolorPomocna = "Najczarniejszy";
			break;
		}
		default:
		{
			kolorPomocna = "Brak koloru";
			break;
		}
	}
		return kolorPomocna;
}

