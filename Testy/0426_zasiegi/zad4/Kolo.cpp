/*
 * Kolo.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#include <iostream>
#include "Kolo.hpp"


using namespace KolorowaGeometria;
using namespace Geometria;

Kolo::Kolo ():mKolor("kolor"),mNazwa("nazwa"),mPromien(0)
{}

Kolo::Kolo (std::string kolor, std::string nazwa, int promien)
:mKolor(kolor),mNazwa(nazwa),mPromien(promien)
{}

Kolo::~Kolo()
{}

void Kolo::wypisz()
	{
		std::cout << mKolor << " " << mNazwa << " " << pole() << std::endl;
	}

int Kolo::pole()
	{
		return mPromien*mPromien*3.14;
	}



