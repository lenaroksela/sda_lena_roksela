/*
 * Przyklad.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef ZAD2_PRZYKLAD_HPP_
#define ZAD2_PRZYKLAD_HPP_

#include <string>

extern int globalIntLicznik;
extern float globalFloat;
extern std::string globalString;

void zmienString(std::string zmienna);
void zmienFloat(float zmienna);


#endif /* ZAD2_PRZYKLAD_HPP_ */
