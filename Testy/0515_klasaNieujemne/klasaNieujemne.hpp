/*
 * klasaNieujemne.hpp
 *
 *  Created on: 15.05.2017
 *      Author: RENT
 */

#ifndef KLASANIEUJEMNE_HPP_
#define KLASANIEUJEMNE_HPP_


class liczbaNieujemna
{
private:
	 int mLiczba;

public:
	liczbaNieujemna(int liczba);

	liczbaNieujemna operator-( const  liczbaNieujemna& liczba );
	liczbaNieujemna operator/(  const liczbaNieujemna& liczba );
	liczbaNieujemna operator*(  const liczbaNieujemna& liczba );

	int getLiczba() const;
	void setLiczba(int liczba);
};



#endif /* KLASANIEUJEMNE_HPP_ */
