/*
 * main.cpp
 *
 *  Created on: 15.05.2017
 *      Author: RENT
 */


#include <iostream>
#include <exception>
#include <stdexcept>
#include "klasaNieujemne.hpp"
#include "klasaWyjatek.hpp"

using namespace std;
int main()
{

	try
	{
		liczbaNieujemna a(0);
		liczbaNieujemna b(1);
		liczbaNieujemna c(19);
		liczbaNieujemna d(0);
		liczbaNieujemna e(0);
		liczbaNieujemna f(1);

			try
			{

				d = a - b;
				cout << d.getLiczba() << std::endl;
			}
			catch (zleArgumenty& blad)
			{
				cout<<blad.what()<<endl;
			}

			try
			{
				e = b * c;
				cout << e.getLiczba() << std::endl;
			}
			catch (zleArgumenty& blad)
			{
				//std::cout << "Wyjatek " << blad << std::endl;
				cout<<blad.what()<<endl;
			}

			try
			{
				f = c / f;
				cout << f.getLiczba() << std::endl;
			}
			catch (zleArgumenty& blad)
			{
				//std::cout << "Wyjatek " << blad << std::endl;
				cout<<blad.what()<<endl;
			}

	}
	catch (bladKontrukcji& blad)
	{
		cout<<blad.what()<<endl;
	}
	return 0;
}
