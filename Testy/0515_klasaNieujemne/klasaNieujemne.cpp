#include "klasaNieujemne.hpp"
#include <string>
#include <exception>
#include <stdexcept>
#include "klasaWyjatek.hpp"
liczbaNieujemna::liczbaNieujemna(int liczba)
{
	if (liczba<0)
		{
		//std::string blad = "Podales liczbe ujemna";

		throw bladKontrukcji();
		}
	else setLiczba(liczba);
}

int liczbaNieujemna::getLiczba() const
{
	return mLiczba;
}

void liczbaNieujemna::setLiczba(int liczba)
{
	mLiczba = liczba;
}

liczbaNieujemna liczbaNieujemna::operator-(  const liczbaNieujemna& liczba )
{
	if (liczba.getLiczba()>this->getLiczba())
	{
		//std::string blad = "1 Wynik bedzie liczba ujemna";
		throw zleArgumenty();
	}
	else
		return liczbaNieujemna(this->getLiczba()-liczba.getLiczba());
}


liczbaNieujemna liczbaNieujemna::operator/( const  liczbaNieujemna& liczba )
{
	if(liczba.getLiczba()==0)
	{
		//std::string blad = "2 Nie mozna dzielic przez 0";
		throw zleArgumenty();
	}
	else
		return liczbaNieujemna(this->getLiczba()/liczba.getLiczba());
}

liczbaNieujemna liczbaNieujemna::operator*(const   liczbaNieujemna& liczba )
	{
		return liczbaNieujemna(this->getLiczba()*liczba.getLiczba());
	}


