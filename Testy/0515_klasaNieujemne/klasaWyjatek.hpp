/*
 * klasaWyjatek.hpp
 *
 *  Created on: 15.05.2017
 *      Author: RENT
 */

#ifndef KLASAWYJATEK_HPP_
#define KLASAWYJATEK_HPP_

#include <exception>
#include <stdexcept>
#include <iostream>
#include <sstream>

class bladKontrukcji: public std::exception
{
public:

	virtual const char* what ()const throw()
		{
		return "Blad konstruktora.";
		}
};

class zleArgumenty: public std::exception
{
public:


	virtual const char* what ()const throw()
		{
		return  "Zle argumenty ";
		}
};


#endif /* KLASAWYJATEK_HPP_ */
