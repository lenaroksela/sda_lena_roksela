/*
 * Matematyka.cpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */
#include "Matematyka.hpp"

#include <cmath>

int Matematyka::Silnia(int liczba)
{
	 if(liczba <= 1)
	{
		return 1;
	}
	else
	{
		return liczba * Silnia(liczba - 1);
	}
}

int Matematyka::Fibonacci(int liczba)
{
	if(liczba <= 0)
	{
		return 0;
	}
	else if(liczba ==1)
	{
		return 1;
	}
	else
	{
	return Fibonacci(liczba - 1) + Fibonacci(liczba - 2);
	}
}


int Matematyka::NWD(int liczba1, int liczba2)
{
	int liczba3=0;

	while(liczba2 != 0)
	{
		liczba3 = liczba1 % liczba2;
		liczba1=liczba2;
		liczba2=liczba3;
	}
	return liczba1;
}

