/*
 * CPracujacy_Student.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#ifndef CPRACUJACY_STUDENT_HPP_
#define CPRACUJACY_STUDENT_HPP_

#include "CStudent.hpp"
#include "CPracownik.hpp"
#include "COsoba.hpp"



class CPracujacy_Student : public CPracownik, public CStudent
{
public:
	CPracujacy_Student();
	CPracujacy_Student(int pesel, int indeks, float pensja);

	virtual void uczSie();
	virtual void pracuj();
	virtual ~CPracujacy_Student();

};



#endif /* CPRACUJACY_STUDENT_HPP_ */
