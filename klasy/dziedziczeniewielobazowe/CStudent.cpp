/*
 * CStudent.cpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */
#include <iostream>
#include "CStudent.hpp"
#include "COsoba.hpp"

CStudent::CStudent(int indeks)
:mIndeks(indeks)
{

}

	CStudent::CStudent(int indeks, std::string kierunek, long pesel)
	:COsoba(pesel)
	,mIndeks(indeks)
	,mKierunek(kierunek)
	{

	}

	CStudent::CStudent(int indeks, std::string kierunek)
	:COsoba(0)
	,mIndeks(indeks)
	,mKierunek(kierunek)
	 {

	 }

	int CStudent::podajNumer()
	{
		return mIndeks;
	}

	void CStudent::uczSie()
	{
		std::cout<<"Ucz sie"<<std::endl;
	}
