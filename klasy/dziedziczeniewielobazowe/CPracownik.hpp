/*
 * CPracownik.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */
#include <string>
#ifndef CPRACOWNIK_HPP_
#define CPRACOWNIK_HPP_
#include "COsoba.hpp"


class CPracownik : virtual public COsoba
{
private:
	float mPensja;
	//std::string mZawod;
public:
	CPracownik();
	CPracownik(float pensja, long pesel);
	CPracownik(float pensja);

	virtual void pracuj();




};



#endif /* CPRACOWNIK_HPP_ */
