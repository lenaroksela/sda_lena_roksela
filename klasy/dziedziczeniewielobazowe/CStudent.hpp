/*
 * CStudent.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */
#include <string>
#ifndef CSTUDENT_HPP_
#define CSTUDENT_HPP_
#include "COsoba.hpp"


class CStudent:virtual public COsoba
{
protected:
	int mIndeks;
	std::string mKierunek;
public:
	int podajNumer();
	 void uczSie();

	CStudent();
	CStudent(int indeks);
	CStudent(int indeks, std::string kierunek, long pesel);
	CStudent(int indeks, std::string kierunek);




};


#endif /* CSTUDENT_HPP_ */
