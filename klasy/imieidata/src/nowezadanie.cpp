//============================================================================
// Name        : nowezadanie.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;


class DataUrodzenia
{
private:
	unsigned int mDzien;
	unsigned int mMiesiac;
	unsigned int mRok;
public:

	unsigned int getDzien() const
	{
		return mDzien;
	}

	void setDzien(unsigned int dzien)
	{
		if (dzien < 1)
			mDzien = 1;
		else if (dzien > 31)
			mDzien = 31;
		else
			mDzien = dzien;
	}

	unsigned int getMiesiac() const
	{
		return mMiesiac;
	}

	void setMiesiac(unsigned int miesiac)
	{

		if (miesiac < 1)
			mMiesiac = 1;
		else if (miesiac > 12)
			mMiesiac = 1;
		else
			mMiesiac = miesiac;
	}

	unsigned int getRok() const
	{
		return mRok;
	}

	void setRok(unsigned int rok)
	{
		if (rok < 1900)
			mRok = 1970;
		else if (rok > 2017)
			mRok = 1970;
		else
			mRok = rok;
	}

	DataUrodzenia(unsigned int dzien, unsigned int miesiac, unsigned int rok)

		{
		setDzien(dzien);
		setMiesiac(miesiac);
		setRok(rok);

		}
};

class Osoba
{
private:
	string mImie;
	string mNazwisko;
	DataUrodzenia dataurodzenia;
public:
	Osoba(string imie, string nazwisko, DataUrodzenia dataurodzenia) :
			mImie(imie), mNazwisko(nazwisko), dataurodzenia(dataurodzenia)
	{

	}

	const DataUrodzenia& getDataurodzenia() const
	{

//		dataurodzenia.getDzien();
//		dataurodzenia.getMiesiac();
//		dataurodzenia.getRok();
		return dataurodzenia;
	}

	const string& getImie() const
	{
		return mImie;
	}

	void setImie(const string& imie)
	{
		mImie = imie;
	}

	const string& getNazwisko() const
	{
		return mNazwisko;
	}

	void setNazwisko(const string& nazwisko)
	{
		mNazwisko = nazwisko;
	}
	void pokaz()
	{
		cout<<mImie<<endl;
		cout<<mNazwisko<<endl;
		cout<<dataurodzenia.getDzien()<< " ";
		cout<<dataurodzenia.getMiesiac()<< " ";
		cout<<dataurodzenia.getRok()<<endl;


	}
};

int main()
{
	DataUrodzenia data(40,7,1990);
	Osoba lena("Lena", "Roksela", data);
	lena.pokaz();

	Osoba lenor("M", "M", DataUrodzenia(1,2,1990));
	lenor.pokaz();


	return 0;
}
