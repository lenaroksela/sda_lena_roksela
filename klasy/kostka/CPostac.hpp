/*
 * CPostac.hpp
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef CPOSTAC_HPP_
#define CPOSTAC_HPP_

class CPostac
{
protected:
	int mZycie;
	int mAtak;
	int mSzybkosc;
	int mX;
	int mY;
	char mSymbol;

	CPostac(int zycie, int atak,int szybkosc,int x,int y,char symbol);
	CPostac();




	bool atak(int x, int y);
	bool ruch(int x, int y);


};



#endif /* CPOSTAC_HPP_ */
