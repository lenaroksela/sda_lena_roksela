#include <iostream>
using namespace std;
class A
{
public:
 explicit A(int myNumber) : myNumber_(myNumber)
 {
 std::cout<< "A\n";
 }
  virtual ~A()
{
 std::cout<< "~A\n";
 }
virtual void myFancyMethod()
{
 myNumber_++;
 std::cout<< "A" << myNumber_<< " ";
}

protected:
int myNumber_;
};

class B : public A
{
public:
 explicit B(int myNumber) : A(myNumber)
 {
 std::cout<< "B\n";
 }
virtual ~B()
{
 std::cout<< "~B\n";
 }
virtual void myFancyMethod()
{

 std::cout<< "B" << myNumber_<< " \n";
}

};
int main()
{
// A a(1); //zad1
// a.myFancyMethod();
//B b(1); //zad 2
//b.myFancyMethod();
A* a = new B(1); //zad 5
a->myFancyMethod();
//delete a;
//
// int a = 1; //zad 7
// const int& b = a;
// a++;
// std::cout<< b << " " << a << std::endl;
//int a = 1; //zad 8
//const int* b = &a;
//a++;
//std::cout << *b * 2 << " " << a << std::endl;
return 0;
}
