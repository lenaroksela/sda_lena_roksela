//============================================================================
// Name        : z_zad1.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
using namespace std;

class Samochod
{
protected:
	string mKolor;
	string mMarka;
	double mPojemnosc;

public:
	string virtual kolor()
	{
		return mKolor;
	}

	string virtual marka()
	{
		return mMarka;
	}

	double virtual pojemnosc()
	{
		return mPojemnosc;
	}
	virtual ~Samochod ()
			{

			}
};

class Opel: public Samochod
{
protected:
	string mKolor;
	string mMarka;
	double mPojemnosc;

public:
	string virtual kolor()
	{
		return mKolor;
	}

	string virtual marka()
	{
		return mMarka;
	}

	double virtual pojemnosc()
	{
		return mPojemnosc;
	}

	Opel() :
			mKolor("Bia�y"), mMarka("Astra"), mPojemnosc(2.00)
	{
	}
	~Opel()
	{}


};
	class Audi: public Samochod
	{
	protected:
		string mKolor;
		string mMarka;
		double mPojemnosc;

	public:
		string virtual kolor()
		{
			return mKolor;
		}

		string virtual marka()
		{
			return mMarka;
		}

		double virtual pojemnosc()
		{
			return mPojemnosc;
		}
		Audi() :
				mKolor("Czarny"), mMarka("A4"), mPojemnosc(3.00)
		{
		}
		Audi(string kolor, string marka, double pojemnosc):mKolor(kolor), mMarka(marka), mPojemnosc(pojemnosc)
		{

		}
		~Audi()
		{

		}

	};

int main()
{
		//Opel a;
		//Audi b("Czarny", "Q5", 2.00);

		Samochod *tablica[2];

		tablica[0] = new Opel();
		tablica[1] = new Audi("Czarny", "Q5", 2.00);


		for(int i=0; i<2; i++)
		{
			std::cout <<" "<< tablica[i]->kolor()<<" ";
			std::cout <<" "<< tablica[i]->marka()<<" ";
			std::cout <<" "<< tablica[i]->pojemnosc()<<endl;

		}

		for(int i=0; i<2; i++)
			{
					delete[] tablica[i];
			}

		return 0;
}
