/*
 * zadanie1.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */
#include "zadanie1.hpp"


int CalcASCII::Sumuj(std::string slowo)

{
	unsigned int wynik =0;
	for(unsigned int i=0; i<slowo.length();i++)
	{
		wynik += slowo[i];
	}


	return wynik;
}

CalcASCII::CalcASCII(std::string slowo)

{
	setNapis(slowo);
	getSuma();
}

const std::string& CalcASCII::getNapis() const
{
	return mNapis;
}

void CalcASCII::setNapis(const std::string& napis)
{

	mNapis = napis;
}

int CalcASCII::getSuma() const
{
	unsigned int mSuma =0;
		for(unsigned int i=0; i<mNapis.length();i++)
		{
			mSuma += mNapis[i];
		}
		return mSuma;
}

void CalcASCII::setSuma(int suma)
{
	mSuma = suma;
}
