/*
 * zadanie1.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef ZADANIE1_HPP_
#define ZADANIE1_HPP_
#include <string>

class CalcASCII
{
	std::string mNapis;
	int mSuma;

public:
	CalcASCII(std::string slowo);
	int Sumuj(std::string slowo);

	const std::string& getNapis() const;

	void setNapis(const std::string& napis);

	int getSuma() const;

	void setSuma(int suma);
};

#endif /* ZADANIE1_HPP_ */
