/*
 * KalkulatorKalorii.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef KALKULATORKALORII_HPP_
#define KALKULATORKALORII_HPP_

class Kalorie
{
private:
	float mWaga;
	float mWzrost;
	int mWiek;
	float mAktywnosc;

public:
	enum Plec
		{
			kobieta,
			mezczyzna,
			nieznany
		};
	Kalorie();
	float CalculateBMI(float waga, float wzrost);
	float CalculateCalorie(float waga, float wzrost, int wiek, Plec plec);

	float getAktywnosc() const;

	void setAktywnosc(float aktywnosc);

	float getWaga() const;

	void setWaga(float waga);

	int getWiek() const;

	void setWiek(int wiek);

	float getWzrost() const;

	void setWzrost(float wzrost);
};




#endif /* KALKULATORKALORII_HPP_ */
