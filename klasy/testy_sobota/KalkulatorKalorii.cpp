/*
 * KalkulatorKalorii.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "KalkulatorKalorii.hpp"

Kalorie::Kalorie() :
		mWaga(0), mWzrost(0), mWiek(0), mAktywnosc(0)
{

}
float Kalorie::CalculateBMI(float waga, float wzrost)
{
	return waga / (wzrost * wzrost);
}
float Kalorie::CalculateCalorie(float waga, float wzrost, int wiek, Plec plec)
{
	switch (plec)
	{
		case mezczyzna:
		{
			return 655.1 + (9.567 * waga) + (1.85 * wzrost) - (4.68 * wiek);
			break;
		}
		case kobieta:
		{
			return 66.47 + (13 * waga) + (5.0 * wzrost) - (6.76 * wiek);
			break;
		}
		default:
		{
			break;
			return 0;
		}
	}
	return 0;

}

float Kalorie::getAktywnosc() const
{
	return mAktywnosc;
}

void Kalorie::setAktywnosc(float aktywnosc)
{
	mAktywnosc = aktywnosc;
}

float Kalorie::getWaga() const
{
	return mWaga;
}

void Kalorie::setWaga(float waga)
{
	mWaga = waga;
}

int Kalorie::getWiek() const
{
	return mWiek;
}

void Kalorie::setWiek(int wiek)
{
	mWiek = wiek;
}

float Kalorie::getWzrost() const
{
	return mWzrost;
}

void Kalorie::setWzrost(float wzrost)
{
	mWzrost = wzrost;
}
