/*
 * KalkulatorKaloriiMain.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "KalkulatorKalorii.hpp"
#include "zadanie1.hpp"
#include "gtest/gtest.h"
#include <iostream>

#include "Kostka.hpp"

TEST(KalorieTest, KalkulatorTest) {
      Kalorie kobieta;
      EXPECT_EQ(0, kobieta.CalculateBMI(53.00, 1.52));
      EXPECT_EQ(0, kobieta.CalculateCalorie(52.00,152.00,27, Kalorie::kobieta));
      EXPECT_EQ(0, kobieta.CalculateBMI(89.00, 1.88));
      EXPECT_EQ(0, kobieta.CalculateCalorie(89.00,188.00,27, Kalorie::mezczyzna));
}

TEST(KostkaTest, LosowanieTest) {
     Kostka kostka;
     EXPECT_LT(6,kostka.Losuj(5));
     EXPECT_LE(6,kostka.Losuj(5));
}

TEST(TestASCII, SumowanieTest) {
     CalcASCII ascii("Lena");
     EXPECT_EQ(0,ascii.getSuma());
     CalcASCII napis("");
     EXPECT_EQ("a",napis.getNapis());
     EXPECT_EQ(0,ascii.Sumuj("a"));
     EXPECT_EQ(0,ascii.Sumuj("Lena"));
     EXPECT_EQ(13124,ascii.Sumuj("Write a program that takes in a string from the user (or if you want, a file!) and prints all the characters' ascii values summed up to the user."));

}
//22.5069
//1319.95

int main(int argc, char **argv)
{
	Kalorie kobieta;


//	std::cout<<kobieta.CalculateBMI(52.00,1.52)<<std::endl;
//
//	std::cout<<kobieta.CalculateCalorie(52.00,152.00,27, Kalorie::kobieta);


	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
