/*
 * Kalendarz.hpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#ifndef KALENDARZ_HPP_
#define KALENDARZ_HPP_

class Kalendarz
{
	int mDzien;
	int mMiesiac;
	int mRok;
	bool czyPoprawny;



public:
	Kalendarz();
	bool czyPoprawnaData(int dzien, int miesiac, int rok);
	bool czyPoprawneDni(int dzien, int miesiac, int rok);

	bool sprawdzDzien();
	bool sprawdzMiesiac(int miesiac);
	bool czyPrzestepny(int rok);

	int getDzien() const;

	void setDzien(int dzien);

	int getMiesiac() const;

	void setMiesiac(int miesiac);

	int getRok() const;

	void setRok(int rok);
};



#endif /* KALENDARZ_HPP_ */
