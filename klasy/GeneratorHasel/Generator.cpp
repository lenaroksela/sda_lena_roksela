/*
 * Generator.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "Generator.hpp"

Generator::Generator()
:mDlugosc(0)
,mHaslo ("")
{

}
	Generator::Generator(int dlugosc)

	{
		std::srand(std::time(NULL));
		setDlugosc(dlugosc);
	}


std::string Generator::Generuj()
{
	for(int i=0; i< getDlugosc();i++)
	{
		mHaslo += std::rand()%27 + 70;
	}
	return mHaslo;
}

int Generator::getDlugosc() const
{
	return mDlugosc;
}

void Generator::setDlugosc(int dlugosc)
{
	mDlugosc = dlugosc;
}

const std::string& Generator::getHaslo() const
{
	return mHaslo;
}

void Generator::setHaslo(const std::string& haslo)
{
	mHaslo = haslo;
}
