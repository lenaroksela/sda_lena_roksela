/*
 * GeneratorHasel.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "Generator.hpp"
#include "gtest/gtest.h"

#include <iostream>


TEST(TestGenerator, TestKonstruktor)
{
	Generator gen(3);
	EXPECT_EQ(3, gen.getDlugosc());
	EXPECT_NE("", gen.getHaslo());
}

TEST(TestGenerator, TestGenerowanie)
{
	Generator gen(3);
	EXPECT_NE("", gen.Generuj());
	EXPECT_NE ("", gen.Generuj());
}

TEST(TestGenerator, TestSeter)
{
	Generator gen;
	gen.setDlugosc(4);
	EXPECT_EQ(4, gen.getDlugosc());

}




//int main(int argc, char **argv)
//{
//
//	::testing::InitGoogleTest(&argc, argv);
//	return RUN_ALL_TESTS();
//}
//
//

int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();


}
