/*
 * Generator.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef GENERATOR_HPP_
#define GENERATOR_HPP_

#include <string>
#include <cstdlib>
#include <ctime>

class Generator
{
	std::string mHaslo;
	int mDlugosc;

public:
	Generator();
	Generator(int dlugosc);


	std::string Generuj();

	int getDlugosc() const;
	void setDlugosc(int dlugosc);
	const std::string& getHaslo() const;
	void setHaslo(const std::string& haslo);
};

#endif /* GENERATOR_HPP_ */
