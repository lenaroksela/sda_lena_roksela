/*
 * raiiPlik.cpp
 *
 *  Created on: 10.04.2017
 *      Author: RENT
 */
#include "raiiPlik.hpp"
#include <ctime>

using namespace std;

Logger::Logger(string nazwaPliku,int log_level=1) :
		mPlik(nazwaPliku.c_str(), ofstream::out), level(log_level)
{
	mPlik<<"Rozpoczecie pracy na pliku\n";
}
Logger::~Logger()
{
	mPlik<<"Koniec pracy na pliku\n";
	mPlik.close();
}

void Logger::Log(string message, int log_level=3)
{
	if(level<log_level) return;
	time_t t;
	t = time(NULL);

	struct tm* current = localtime(&t);

	mPlik << "\n" << current->tm_year + 1900 << "." << current->tm_mon + 1
			<< "." << current->tm_mday << " " << current->tm_hour << ":"
			<< current->tm_min << ":" << current->tm_sec << "     " << message;

//	mPlik <<"[" << t << "]" <<message;



}

void Logger::LogLiczba (int liczba1)
{

	time_t t;
	t = time(NULL);

	struct tm* current = localtime(&t);
	mPlik << "\n" << current->tm_year + 1900 << "." << current->tm_mon + 1
			<< "." << current->tm_mday << " " << current->tm_hour << ":"
			<< current->tm_min << ":" << current->tm_sec << "     " << liczba1;

//	mPlik <<"[" << t << "]" <<message;

}

