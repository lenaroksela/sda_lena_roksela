/*
 * raiiPlik.hpp
 *
 *  Created on: 10.04.2017
 *      Author: RENT
 */

#ifndef RAIIPLIK_HPP_
#define RAIIPLIK_HPP_

#include <fstream>
#include <string>

using namespace std;

class Logger
{
private:
	ofstream  mPlik;
	int level;


public:
	Logger(string nazwaPliku, int log_level);
	~Logger();

	void Log (string message,int log_level);
	void LogLiczba (int liczba);

};




#endif /* RAIIPLIK_HPP_ */
