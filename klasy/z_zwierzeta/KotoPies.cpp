/*
 * KotoPies.cpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#include "KotoPies.hpp"

KotoPies::KotoPies() :
		Pies(), Kot()
{

}

KotoPies::KotoPies(string kot, string pies)
:Kot(kot)
,Pies(pies)
{

}

void KotoPies::dajGlos()
{
	cout<<Kot::mImie<<" ";
	cout<<Pies::mImie<<" ";
	cout << "Miau Hau\n";
}

