/*
 * zwierzeta.cpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#include "DzikieZwierze.hpp"
#include "Kot.hpp"
#include "Pies.hpp"
#include "KotoPies.hpp"
#include <iostream>

using namespace std;

int main()
{
	Pies pies;
	pies.dajGlos();

	Kot kot;
	kot.dajGlos();
	int n=5;
	DzikieZwierze * tab[n];

	tab[0] = new Kot("Ragnar");
	tab[1] = new Pies("Azorek");
	tab[2] = new Kot("Kreska");
	tab[3] = new Pies("Murzyn");
	tab[4]= new KotoPies("KOTO","Pies");

	for (int i = 0; i < n; i++)
	{
		tab[i]->dajGlos();
	}

	for (int i = 0; i < n; i++)
	{
		delete tab[i];
	}

	cout<<"----------------------------\n";
	KotoPies kotopies;
	kotopies.dajGlos();

	return 0;
}

