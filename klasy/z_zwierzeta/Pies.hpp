/*
 * Pies.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef PIES_HPP_
#define PIES_HPP_

#include "DzikieZwierze.hpp"

class Pies: public virtual DzikieZwierze
{
public:
	string mImie;

	Pies();
	Pies(string pies);
	void dajGlos();

};

#endif /* PIES_HPP_ */
