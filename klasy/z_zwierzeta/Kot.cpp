/*
 * Kot.cpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#include "Kot.hpp"

Kot::Kot() :
		mImie("Kicia")
{
	cout << "Kicia\n";
}

Kot::Kot(string kot) :
		mImie(kot)
{

}

void Kot::dajGlos()
{
	cout << mImie << " ";
	cout << "Miau Miau\n";
}
