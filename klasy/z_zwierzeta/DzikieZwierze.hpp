/*
 * DzikieZwierze.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef DZIKIEZWIERZE_HPP_
#define DZIKIEZWIERZE_HPP_
#include <iostream>
#include <string>
using namespace std;

class DzikieZwierze
{
public:
	string mImie;

	virtual void dajGlos()=0;
	DzikieZwierze();
	virtual ~DzikieZwierze();

};

#endif /* DZIKIEZWIERZE_HPP_ */
