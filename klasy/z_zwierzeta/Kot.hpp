/*
 * Kot.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef KOT_HPP_
#define KOT_HPP_
#include "DzikieZwierze.hpp"

class Kot: virtual public DzikieZwierze
{
protected:
	string mImie;
public:
	Kot();
	Kot(string kot);

	void dajGlos();

};

#endif /* KOT_HPP_ */
