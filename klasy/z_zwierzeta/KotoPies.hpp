/*
 * KotoPies.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef KOTOPIES_HPP_
#define KOTOPIES_HPP_

#include "Pies.hpp"
#include "Kot.hpp"

class KotoPies: public Pies, public Kot
{
public:
	KotoPies();
	KotoPies(string kot, string pies);

	void dajGlos();

};

#endif /* KOTOPIES_HPP_ */
