/*
 * Grawitacja.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef GRAWITACJA_HPP_
#define GRAWITACJA_HPP_
#define G 6.67e-11

class Grawitacja
{


	float mMass;
	float mDistance;
	float mRadius;
	float mVolume;
	float mDensity;
	float mForce;

public:
	float Volume(float radius);
	 float Mass(float density);
	 float Force(float mass1, float radius);

	 float getMass() const;

	void setMass(float mass);

	float getRadius() const;

	void setRadius(float radius);

	float getVolume() const;

	void setVolume(float volume);

	float getForce() const;


	void setForce(float force);

};



#endif /* GRAWITACJA_HPP_ */
