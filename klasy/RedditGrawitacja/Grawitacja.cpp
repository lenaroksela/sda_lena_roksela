/*
 * Grawitacja.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */
#include "Grawitacja.hpp"
#include <cmath>

float Grawitacja::Volume(float radius)
{
	setRadius(radius);
	float volume = ((4/3)*3.14*(radius*radius*radius));

	setVolume(volume);
	return mVolume;
}

float Grawitacja::Mass(float density)
{
	float mass = mVolume * density;
	setMass(mass);
	return mMass;
}
float Grawitacja::Force(float mass1, float radius)
{
	//long long g = 6.67 * (std::pow(10, -11));

	float force = G * ((mass1 * mMass ) / (radius * radius));
	setForce(force);
	return mForce;
}

void Grawitacja::setMass(float mass)
	{
		mMass = mass;
	}

float Grawitacja::getRadius() const
	{
		return mRadius;
	}

	void Grawitacja::setRadius(float radius)
	{
		mRadius = radius;
	}

	float Grawitacja::getVolume() const
	{
		return mVolume;
	}

	void Grawitacja::setVolume(float volume)
	{
		mVolume = volume;
	}

	float Grawitacja::getForce() const
	{
		return mForce;
	}

	void Grawitacja::setForce(float force)
	{
		mForce = force;
	}
