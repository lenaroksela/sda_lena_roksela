/*
 * Karta.cpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#include "Karta.hpp"




Karta::Karta()
:mKolor(endKolor)
,mFigura (endFigura)
,mWartosc(0)
{

}

int Karta::PodajWartosc(Figura figura)
{
	int wartosc = 0;

	switch(figura)
	{

	case Dwa:
	case Trzy:
	case Cztery:
	case Piec:
	case Szesc:
	case Siedem:
	case Osiem:
	case Dziewiec:
	case Dziesiec:
		wartosc = 2 + figura;
		break;
	case Krol:
	case Krolowa:
	case Walet:
		wartosc = 10;
		break;
	case As:
		wartosc =11;
		break;
	default:
		wartosc = 0;
		break;

	}
	return wartosc;
}


void Karta::setValues(Kolor kolor, Figura figura)
	{
		mKolor = kolor;
		mFigura = figura;
		mWartosc = PodajWartosc(figura);
	}
