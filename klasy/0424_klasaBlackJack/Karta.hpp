/*
 * Karta.hpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#ifndef KARTA_HPP_
#define KARTA_HPP_
#include <string>
using namespace std;

class Karta
{
public:

	enum Kolor
	{
		Kier = 0, Karo, Pik, Trefl, endKolor
	};

	enum Figura
	{
		Dwa = 0,
		Trzy,
		Cztery,
		Piec,
		Szesc,
		Siedem,
		Osiem,
		Dziewiec,
		Dziesiec,
		Walet,
		Krolowa,
		Krol,
		As,
		endFigura

	};

	Figura getFigura() const
	{
		return mFigura;
	}

	Kolor getKolor() const
	{
		return mKolor;
	}

	int getWartosc() const
	{
		return mWartosc;
	}

private:
	Kolor mKolor;
	Figura mFigura;

	int mWartosc;

	int PodajWartosc(Figura figura);

public:

	Karta(Kolor kolor, Figura figura);
	Karta();

	void setValues(Kolor kolor, Figura figura);

};

#endif /* KARTA_HPP_ */
