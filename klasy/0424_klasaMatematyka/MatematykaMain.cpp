/*
 * MatematykaMain.cpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#include "gtest/gtest.h"
#include "Matematyka.hpp"

TEST(MatematykaTest1, SilniaTestDodatnie)
{
	Matematyka a;
	EXPECT_EQ(24, a.Silnia(4));
	EXPECT_EQ(1, a.Silnia(1));
	EXPECT_EQ(1, a.Silnia(0));
	EXPECT_EQ(720, a.Silnia(6));
}

TEST(MatematykaTest1, SilniaTestUjemne)
{
	Matematyka b;
	EXPECT_EQ(1, b.Silnia(-1));
	EXPECT_EQ(1, b.Silnia(0));
	EXPECT_EQ(1, b.Silnia(-10));
	EXPECT_EQ(1, b.Silnia(-2));
}

TEST(MatematykaTest2, FibonacciTestDodatni)
{
	Matematyka a;
	EXPECT_EQ(8, a.Fibonacci(6));
	EXPECT_EQ(89, a.Fibonacci(11));
	EXPECT_EQ(1, a.Fibonacci(1));
	EXPECT_EQ(0, a.Fibonacci(0));
}

TEST(MatematykaTest2, FibonacciTestUjemny)
{
	Matematyka b;
	EXPECT_EQ(0, b.Fibonacci(-1));
	EXPECT_EQ(0, b.Fibonacci(-10));
	EXPECT_EQ(0, b.Fibonacci(0));
	EXPECT_EQ(0, b.Fibonacci(-2));
}

TEST(MatematykaTest3, NWDTest)
{
	Matematyka a;
	EXPECT_EQ(6, a.NWD(12,6));
	EXPECT_EQ(1, a.NWD(1,21));
	EXPECT_EQ(6, a.NWD(6,12));
	EXPECT_EQ(3, a.NWD(9,21));
	EXPECT_EQ(3, a.NWD(21,9));

}



int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}


