/*
 * reszta.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef RESZTA_HPP_
#define RESZTA_HPP_
#include "pieniadz.hpp"

class Automat
{
public:
	double mKwota;
	double mReszta;
	double mNominaly[];
	Pieniadz pieniadz;



	Automat(double kwota);
	double reszta(double kwota);




};



#endif /* RESZTA_HPP_ */
