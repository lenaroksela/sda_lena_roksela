/*
 * PodstawionyTekst.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef PODSTAWIONYTEKST_HPP_
#define PODSTAWIONYTEKST_HPP_


#include <iostream>
#include <string>
#include "Szyfrowany.hpp"
#include "wypisywalny.hpp"


class PodstawionyTekst: public Szyfrowany
{
public:
	PodstawionyTekst();
	PodstawionyTekst(std::string tekst);
	~PodstawionyTekst();

	void szyfruj();
	void wypisz();



};



#endif /* PODSTAWIONYTEKST_HPP_ */
