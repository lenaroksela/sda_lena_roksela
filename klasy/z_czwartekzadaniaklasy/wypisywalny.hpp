/*
 * wypisywalny.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef WYPISYWALNY_HPP_
#define WYPISYWALNY_HPP_

#include <string>
#include <iostream>


using namespace std;


class Wypisywalny
{
protected:
	string mLancuch;

public:
	Wypisywalny();
	Wypisywalny(string& lancuch);

void szyfruj();
 void  wypisz();

	~Wypisywalny();

	const string& getLancuch() const;


	void setLancuch( string& lancuch);
};



#endif /* WYPISYWALNY_HPP_ */
