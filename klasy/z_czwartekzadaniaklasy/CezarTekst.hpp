/*
 * CezarTekst.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef CEZARTEKST_HPP_
#define CEZARTEKST_HPP_

#include <iostream>
#include <string>
#include "Szyfrowany.hpp"
#include "wypisywalny.hpp"

class CezarTekst: public Szyfrowany
{
public:
	CezarTekst();
	CezarTekst(std::string lancuch);
	~CezarTekst();

	void szyfruj();
	void wypisz();


};



#endif /* CEZARTEKST_HPP_ */
