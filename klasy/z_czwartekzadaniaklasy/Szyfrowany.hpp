/*
 * Szyfrowany.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef SZYFROWANY_HPP_
#define SZYFROWANY_HPP_

#include <string>
#include <iostream>
#include "wypisywalny.hpp"

using namespace std;

class  Szyfrowany: public Wypisywalny
{

protected:
	bool mCzSzyfrowany;
public:
	virtual void szyfruj();
	virtual void wypisz();
	Szyfrowany();
	Szyfrowany(string& lancuch);

	bool czyszyfrowany();


	virtual ~Szyfrowany();

	bool isCzSzyfrowany() const;

};



#endif /* SZYFROWANY_HPP_ */
