/*
 * Palindrom.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef PALINDROM_HPP_
#define PALINDROM_HPP_



class Palindrom
{
protected:
	int mLiczba;
	int mLiczbaPalindrom;
	int mLicznik;

public:

	Palindrom();
	Palindrom(int liczba);

	int NaPalindrom();
	int odwaracanieLiczby(int liczba);
	bool czyPalindrom(int liczba);

	void setLiczbaPalindrom(int liczbaPalindrom);
	void setLiczba(int liczba);

	int getLiczbaPalindrom() const;
	int getLiczba() const;

};



#endif /* PALINDROM_HPP_ */
