/*
 * Palindrom.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "Palindrom.hpp"
#include <cmath>
#include <iostream>

	Palindrom::Palindrom()
	:mLiczba(0)
	,mLiczbaPalindrom(0)
	,mLicznik(0)
	{

	}

	Palindrom::Palindrom(int liczba)
	:mLiczba(liczba)
	,mLiczbaPalindrom(0)
	,mLicznik(0)
	{

	}

	//funckja zmieniajaca podana liczbe na palindrom

	int Palindrom::NaPalindrom()
	{
		odwaracanieLiczby(getLiczba());


		int pomocnyPalindrom=0;

		do{
			pomocnyPalindrom = mLiczba + mLiczbaPalindrom;
			++mLicznik;
			int pomocna= mLiczbaPalindrom;
			odwaracanieLiczby(pomocnyPalindrom);
			//pomocnyPalindrom+=pomocna;

		}while(czyPalindrom(pomocnyPalindrom));

		std::cout<<"Liczba "<< mLiczba <<" ma palindrom "<<pomocnyPalindrom<<" w tylu krokach"<<mLicznik;
		return 0;
	}

	int Palindrom::odwaracanieLiczby(int liczba)
	{
		int LiczbaCyfr = floor( log10(liczba)+1);
		    int pomocnicza = 0;
		    for( int i=0; i<LiczbaCyfr; i++)
		    {
		    		pomocnicza+=pow(10.0, LiczbaCyfr - 1 - i )*(liczba%10);
		 			liczba=liczba/10;
		    }
		    setLiczbaPalindrom(pomocnicza);
		    return mLiczbaPalindrom;

	}


	bool Palindrom::czyPalindrom(int liczba)
	{
		if((liczba - odwaracanieLiczby(liczba)) == 0)
		{
			return true;
		}
		else
			return false;
	}


	void Palindrom::setLiczbaPalindrom(int liczbaPalindrom)
	{
		mLiczbaPalindrom = liczbaPalindrom;
	}
	void Palindrom::setLiczba(int liczba)
	{
		mLiczba = liczba;
	}

	int Palindrom::getLiczbaPalindrom() const
	{
		return mLiczbaPalindrom;
	}
	int Palindrom::getLiczba() const
	{
		return mLiczba;
	}

