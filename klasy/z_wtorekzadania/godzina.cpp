/*
 * godzina.cpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */


#include "godzina.hpp"
#include <iostream>

 Godzina::Godzina()
 :mGodzina(0)
 ,mMinuta(0)
 ,mSekunda(0)
 {

 }
Godzina::Godzina(int godzina, int minuty, int sekundy)
:mGodzina(godzina)
,mMinuta(minuty)
,mSekunda(sekundy)
{

}

void Godzina::wypisz()
{
	std::cout<<mGodzina<<":"<<mMinuta<<":"<<mSekunda<<std::endl;
}

int Godzina::getGodzina() const
{
return mGodzina;
}

void Godzina::setGodzina(int godzina)
{
mGodzina = godzina;
}

int Godzina::getMinuta() const
{
return mMinuta;
}

void Godzina::setMinuta(int minuta)
{
mMinuta = minuta;
}

int Godzina::getSekunda() const
{
return mSekunda;
}

void Godzina::setSekunda(int sekunda)
{
mSekunda = sekunda;
}

void Godzina::przesunGodzina(int godzina)
{

	mGodzina += godzina;
}

void Godzina::przesunMinuta(int minuta)
{

	if (mMinuta + minuta > 60)
	{
		int pomoc;
		mMinuta = (mMinuta + minuta) % 61;
		pomoc = (mMinuta + minuta) /60;
		przesunGodzina(pomoc);
	}
	else
		mMinuta += minuta;
}

void Godzina::przesunSekunda(int sekunda)
{
	if ((mSekunda + sekunda) > 61)
	{
		int pomoc;
		mSekunda = (mSekunda + sekunda) % 61;
		pomoc = (mSekunda + sekunda)/60;
		przesunMinuta(pomoc);
	}
	else
		mSekunda += sekunda;
}

