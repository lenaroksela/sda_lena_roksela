/*
 * klasData.hpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#ifndef KLASDATA_HPP_
#define KLASDATA_HPP_
#include <iostream>
#include <cmath>

class Data
{
private:
	unsigned int mDzien;
	unsigned int mMiesiac;
	unsigned int mRok;
public:

	unsigned int getDzien() const;

	void setDzien(unsigned int dzien);

	unsigned int getMiesiac() const;

	void setMiesiac(unsigned int miesiac);

	unsigned int getRok() const;

	void setRok(unsigned int rok);

	Data(unsigned int dzien, unsigned int miesiac, unsigned int rok);

	void wypisz();

	void przesunDzien(int dzien);
	void przesunMiesiac(int miesiac);
	void przesunRok(int rok);

	Data roznica(Data& nowa);
	Data();

};



#endif /* KLASDATA_HPP_ */
