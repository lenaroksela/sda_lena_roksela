/*
 * Czas.hpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#ifndef CZAS_HPP_
#define CZAS_HPP_
#include <iostream>
#include "klasData.hpp"
#include <cmath>

unsigned int Data::getDzien() const
{
	return mDzien;
}

void Data::setDzien(unsigned int dzien)
{
	if (dzien < 1)
		mDzien = 1;
	else if (dzien > 31)
		mDzien = 31;
	else
		mDzien = dzien;
}

unsigned int Data::getMiesiac() const
{
	return mMiesiac;
}

void Data::setMiesiac(unsigned int miesiac)
{

	if (miesiac < 1)
		mMiesiac = 1;
	else if (miesiac > 12)
		mMiesiac = 1;
	else
		mMiesiac = miesiac;
}

unsigned int Data::getRok() const
{
	return mRok;
}

void Data::setRok(unsigned int rok)
{
	if (rok < 1900)
		mRok = 1970;
	else if (rok > 2017)
		mRok = 1970;
	else
		mRok = rok;
}

Data::Data(unsigned int dzien, unsigned int miesiac, unsigned int rok)

{
	setDzien(dzien);
	setMiesiac(miesiac);
	setRok(rok);
}

void Data::wypisz()
{
	std::cout << mDzien << " ";
	std::cout << mMiesiac << " ";
	std::cout << mRok << std::endl;
}

void Data::przesunRok(int rok)
{

	mRok += rok;
}

void Data::przesunMiesiac(int miesiac)
{

	if (mMiesiac + miesiac > 12)
	{
		mMiesiac = (mMiesiac + miesiac) % 12;
		przesunRok(mMiesiac);
	}
	else
		mMiesiac += miesiac;
}

void Data::przesunDzien(int dzien)
{
	if ((mDzien + dzien) > 31)
	{
		mDzien = (mDzien + dzien) % 31;
		przesunMiesiac(mDzien);
	}
	else
		mDzien += dzien;
}

Data Data::roznica(Data& nowa)
{
	nowa.mDzien = std::abs(nowa.mDzien - mDzien);
	nowa.mMiesiac = std::abs(nowa.mMiesiac - mMiesiac);
	nowa.mRok = std::abs(nowa.mRok - mRok);

	return nowa;
}
Data::Data() :
		mDzien(0), mMiesiac(0), mRok(0)
{

}

#endif /* CZAS_HPP_ */
