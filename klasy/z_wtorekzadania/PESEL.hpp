/*
 * PESEL.hpp
 *
 *  Created on: 03.04.2017
 *      Author: RENT
 */

#ifndef PESEL_HPP_
#define PESEL_HPP_
#include <string>
#include "klasData.hpp"
using namespace std;



class PESEL
{
public:
	int* mPESEL;
	Data data;

	PESEL();
	PESEL(int *pesel);

	Data pobierzData(int * pesel);
	string string(int * pesel);
	int sprPesel(int* pesel);



};



#endif /* PESEL_HPP_ */
