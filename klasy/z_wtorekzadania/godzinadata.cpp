/*
 * godzinadata.cpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#include "godzinaidata.hpp"

	DataGodzina::DataGodzina(Data data, Godzina godzina)
	:Data::Data(data)
	,Godzina::Godzina(godzina)
	{

	}

const Data& DataGodzina::getData() const
	{
		return mData;
	}

	void DataGodzina::setData(const Data& data)
	{
		mData = data;
	}

	const Godzina& DataGodzina::getGodzina() const
	{
		return mGodzina;
	}

	void DataGodzina::setGodzina(const Godzina& godzina)
	{
		mGodzina = godzina;
	}
	void DataGodzina::wypiszDG()
	{
		Data::wypisz();
		Godzina::wypisz();
	}

	void DataGodzina::przesunGD(DataGodzina& nowy)
	{

		Data::przesunDzien(nowy.getDzien());
		Data::przesunMiesiac(nowy.getMiesiac());
		Data::przesunRok(nowy.getRok());
		Godzina::przesunSekunda(nowy.getSekunda());
		Godzina::przesunMinuta(nowy.getMinuta());
		Godzina::przesunGodzina(nowy.Godzina::getGodzina());
	}

//	DataGodzina::DataGodzina(int dzien, int miesiac, int rok, int godzina, int minuta, int sekunda)
//	:Data::mDzien(dzien)
//	,Data::mMiesiac(miesiac)
//	,Data::mRok(rok)
//	,Godzina::mGodzina(godzina)
//	,Godzina::mMinuta(minuta)
//	,Godzina::mSekunda(sekunda)
//	{
//
//	}
