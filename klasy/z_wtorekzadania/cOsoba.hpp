/*
 * cOsoba.hpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef COSOBA_HPP_
#define COSOBA_HPP_
#include <string>
#include "klasData.hpp"
#include "PESEL.hpp"

class Osoba: public Data, public PESEL
{
public:
	const Data& getData() const;
	const std::string& getImie() const;
	void setImie(const std::string& imie);

	const std::string& getNazwisko() const;
	void setNazwisko(const std::string& nazwisko);

//	const int* getPesel() const;

//	void setPesel(int* pesel);
//	int sprPesel(int* pesel);
	Osoba();
	//Osoba(int* pesel);
//	void wyliczDate(int* pesel);
	Osoba(int * pesel, std::string imie, std::string nazwisko,std::string plec);
private:
	std::string mImie;
	std::string mNazwisko;
	PESEL mPESEL;
	Data data;
	enum Plec
	{
		kobieta, mezczyzna
	};
	std::string mPlec;

};

#endif /* COSOBA_HPP_ */
