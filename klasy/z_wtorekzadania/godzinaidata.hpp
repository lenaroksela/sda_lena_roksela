/*
 * godzinaidata.hpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef GODZINAIDATA_HPP_
#define GODZINAIDATA_HPP_
#include "godzina.hpp"
#include "klasData.hpp"

class DataGodzina: public Data, public Godzina
{
private:
	Data mData;
	Godzina mGodzina;
public:
	DataGodzina();
	DataGodzina(int dzien, int miesiac, int rok, int godzina, int minuta, int sekunda);
	DataGodzina(Data data, Godzina godzina);

	const Data& getData() const;

	void setData(const Data& data);

	const Godzina& getGodzina() const;

	void setGodzina(const Godzina& godzina);

	void wypiszDG();

	void przesunGD(DataGodzina& nowy);

};



#endif /* GODZINAIDATA_HPP_ */
