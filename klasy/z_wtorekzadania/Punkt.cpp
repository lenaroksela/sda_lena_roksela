/*
 * Punkt.cpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#include "Punkt.hpp"
#include <iostream>
#include <cmath>


	Punkt::Punkt() :mX(0), mY(0)
	{ }
	Punkt::Punkt(int x, int y) :mX(x), mY(y)
	{ }

	int Punkt::getX() const
	{
		return mX;
	}

	void Punkt::setX(int x)
	{
		mX = x;
	}

	int Punkt::getY() const
	{
		return mY;
	}

	void Punkt::setY(int y)
	{
		mY = y;
	}

	void Punkt::wypisz()
	{
		std::cout<<"[ "<< getX()<< ", "<<getY()<<" ]";
	}

	void Punkt::przesunPunkt(Punkt& nowy)
	{
		przesunXY(nowy.getX(), nowy.getY());

	}
	void  Punkt::przesunX(int x)
	{
		przesunXY(x,0);
	}
	void Punkt::przesunY(int y)
	{
		przesunXY(0,y);
	}
	void Punkt::przesunXY(int x, int y)
	{
		mX+=x;
		mY+=y;

	}

	double Punkt::obliczOdleglosc(Punkt nowy)
	{
		return std::sqrt (std::pow((nowy.mX-mX),2) + std::pow((nowy.mY - mY),2));
	}

	double Punkt::obliczOdleglosc(int x, int y)
	{
		return std::sqrt (std::pow((x-mX),2) + std::pow((y - mY),2));
	}
