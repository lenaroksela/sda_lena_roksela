/*
 * godzina.hpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#ifndef GODZINA_HPP_
#define GODZINA_HPP_

class Godzina
{
private:
	int	mGodzina;
	int mMinuta;
	int mSekunda;

public:
	 Godzina();
	 Godzina(int godzina, int minuty, int sekundy);

	 void wypisz();

	int getGodzina() const;
	void setGodzina(int godzina);

	int getMinuta() const;
	void setMinuta(int minuta);

	int getSekunda() const;
	void setSekunda(int sekunda);

	void przesunGodzina(int godzina);
	void przesunMinuta(int minuta);
	void przesunSekunda(int sekunda);
};



#endif /* GODZINA_HPP_ */
