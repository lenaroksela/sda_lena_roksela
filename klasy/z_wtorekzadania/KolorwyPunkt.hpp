/*
 * KolorwyPunkt.hpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#ifndef KOLORWYPUNKT_HPP_
#define KOLORWYPUNKT_HPP_
#include <string>
#include "Punkt.hpp"
class PunktKolorowy :public Punkt
{
public:
	enum Kolor
	{
		red,
		green,
		black
	};

	PunktKolorowy(int x, int y,Kolor kolor);

	void wypiszKolor();

protected:
	Kolor mKolor;

};


#endif /* KOLORWYPUNKT_HPP_ */
