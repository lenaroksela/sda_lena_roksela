/*
 * Punkt.hpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#ifndef PUNKT_HPP_
#define PUNKT_HPP_

class Punkt
{
private:
	int mX;
	int mY;

public:
	Punkt();
	Punkt(int x, int y);

	int getX()const;

	void setX(int x);

	int getY()const;

	void setY(int y);

	void wypisz();

	void przesunPunkt(Punkt& nowy);
	void przesunX(int x);
	void przesunY(int y);
	void przesunXY(int x, int y);

	double obliczOdleglosc(Punkt nowy);
	double obliczOdleglosc(int x, int y);


};



#endif /* PUNKT_HPP_ */
