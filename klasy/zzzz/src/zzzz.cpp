//============================================================================
// Name        : zzzz.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

class A
{
public:
	A()
	{
		cout << "A\n";
	}

	~A()
	{
		cout << "~A\n";
	}

	void pokaz()
	{
		cout<<"pokaz A\n";
	}
};

class B: public A
{
public:
	B()
	{
		cout << "B\n";
	}

	~B()
	{
		cout << "~B\n";
	}
};

class C: public A
{
public:
	C()
	{
		cout << "C\n";
	}


	virtual void pokaz()
	{
		cout<<"pokaz C\n";
	}
	virtual ~C()
		{
			cout << " virtualny ~C\n";
		}
};


int main()
{
	int a=1;
	const int *b=&a;
	a++;
	std::cout<<*b*2<<a<<"\n";



	return 0;
}
