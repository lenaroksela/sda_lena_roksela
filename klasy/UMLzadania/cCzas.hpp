/*
 * cCzas.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#ifndef CCZAS_HPP_
#define CCZAS_HPP_

#include <string>
#include <iostream>

using namespace std;


class Time
{
private:
	int mHour;
	int mMinute;
	int mSecond;

public:
	Time(int hour, int minute, int second);

	int getHour() const;
	int getMinute() const;
	int getSecond() const;

	void setHour(int hour);
	void setMinute(int minute);
	void setSecond(int second);

	void setTime(int hour, int minute, int second);

	string toString();

	Time nextSecond();
	Time previousSecond();

	void pokaz();
};



#endif /* CCZAS_HPP_ */
