/*
 * Pralka.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#ifndef PRALKA_HPP_
#define PRALKA_HPP_

#include <string>
#include <iostream>

using namespace std;

class Sensor
{
public:
	virtual bool check()=0;
	virtual ~Sensor();
};

class DoorSensor :public Sensor
{
public:
	bool check();
};

class TempSensor :public Sensor
{
public:
	bool check();
};

class WaterSensor :public Sensor
{
public:
	int currentLevel;
	int desiredLevel;
	bool check();
};

class Machine
{
public:
	virtual void turnOn() = 0;
	virtual void turnOff() = 0;

	virtual	~Machine()
	{

	}
};


class Engine :  public Machine
{
int rotation;
public:
void turnOn();
void turnOff();
};



class Timer
{
private:
	int value;
	int duration;

public:
	int setDuration(int in);
	void start();
	int count();
	int getValue();
	int getDuartion();

};

class WashOption
{
private:
	int washSeletion;
public:
	int getwashSelection();//dodana zmienna int option
};

class WashingMachine : public Machine
{
private:
	int washTime;
	int rinseTime;
	int spinTime;
public:
	void mainWashingMachine();
	void wash();
	void rains();
	void spin();
	void fill();
	void empty();
	void standardWash();
	void twiceRinse();

	void turnOn();
	void turnOff();
};







#endif /* PRALKA_HPP_ */
