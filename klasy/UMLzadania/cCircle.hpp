/*
 * cCircle.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#ifndef CCIRCLE_HPP_
#define CCIRCLE_HPP_

#include <string>
#include <iostream>

using namespace std;

class Circle
{
private:
	double mRadius;
	string mColor;

public:
	Circle();
	Circle(double radius);
	Circle(double radius, string color);

	double getRadius();
	void setRadius(double radius);

	string setColor();
	void getColor(string color);

	double getArea();
	string toString();

};

class Cylinder: public Circle
{
private:
	double mHeight;

public:
	Cylinder();
	Cylinder(double radius);
	Cylinder(double radius, double height);
	Cylinder(double radius, double height, string color);

	double getHeight();
	void setHeight(double height);

	double getVolume();

};




#endif /* CCIRCLE_HPP_ */
