/*
 * cCzas.cpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#include "cCzas.hpp"
#include <sstream>

Time::Time(int hour, int minute, int second) :
		mHour(hour), mMinute(minute), mSecond(second)
{

}

int Time::getHour() const
{
	return mHour;
}

int Time::getMinute() const
{
	return mMinute;
}

int Time::getSecond() const
{
	return mSecond;
}

void Time::setHour(int hour)
{
	if (hour > 24)
		{
		hour = 0;
		}
	else if (hour < 0)
		{
			hour = 0;
		}
	else
	mHour = hour;
}

void Time::setMinute(int minute)
{
	if (minute > 60)
		{
			minute = 0;
		}
		else if (minute < 0)
		{
			minute = 0;
		}
		else
	mMinute = minute;
}

void Time::setSecond(int second)
{
	if (second > 60)
	{
		second = 0;
	}
	else if (second < 0)
	{
		second = 0;
	}
	else
	mSecond = second;
}

void Time::setTime(int hour, int minute, int second)
{
	setSecond(second);
	setMinute(minute);
	setHour(hour);
}

string Time::toString()
{
	ostringstream oczas;
	if (mSecond < 10)
	{

		oczas << mHour << ":" << mMinute << ":0" << mSecond;
		string str = oczas.str();
	}
	else
		oczas << mHour << ":" << mMinute << ":" << mSecond;
		string str = oczas.str();



	return str;
}

Time Time::nextSecond()
{
	mSecond+=1;
	Time time(mHour,mMinute,mSecond);

	return time;

}
Time Time::previousSecond()
{
	mSecond-=1;
	Time time(mHour,mMinute,mSecond);

	return time;
}

void Time::pokaz()
{
	cout<<mHour<<":"<<mMinute<<":"<<mSecond<<endl;;
}

