/*
 * ZapisDoPliku.hpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef ZAPISDOPLIKU_HPP_
#define ZAPISDOPLIKU_HPP_


#include "Plik.hpp"
#include <fstream>
#include <iostream>
using namespace std;

class ZapisDoPliku: public OperacjaNaPliku
{
protected:
	std::fstream mPlik;
public:
	ZapisDoPliku();
	void wpisz(string lancuch);
};

#endif /* ZAPISDOPLIKU_HPP_ */
