/*
 * CzytaniePliku.hpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef CZYTANIEPLIKU_HPP_
#define CZYTANIEPLIKU_HPP_

#include "Plik.hpp"
#include <fstream>
#include <iostream>
using namespace std;

class CzytaniePliku : public  OperacjaNaPliku
{
protected:
	std::fstream mPlik;
public:
	CzytaniePliku();
	void pobierz();


};



#endif /* CZYTANIEPLIKU_HPP_ */
