/*
 * Plik.hpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef PLIK_HPP_
#define PLIK_HPP_
#include <fstream>
#include <string>

class OperacjaNaPliku
{
public:
	std::string mSciezka;
	std::string mStanPliku;

	std::fstream mPlik;

	enum StanPliku
	{
		otwarty,
		zamkniety,
		blad
	};
public:

	OperacjaNaPliku();

	virtual void OtworzPlik();
	virtual void ZamknijPlik();

	//virtual ~OperacjaNaPliku();

};




#endif /* PLIK_HPP_ */
