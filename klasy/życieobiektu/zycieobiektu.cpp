
#include <iostream>
#include <string>
using namespace std;
class Procesor
{
public:
    virtual void process() =0;
    virtual ~Procesor()
    {
        cout << "~Procesor()\n";
    }
};
class ProcesorAMD: public Procesor
{
public:
    void process()
    {
        cout << "AMD procesuje\n";
    }
    ~ProcesorAMD()
    {
        cout << "~ProcesorAMD()\n";
    }
};
class ProcesorINTEL: public Procesor
{
public:
    void process()
    {
        cout << "Intel procesuje\n";
    }
    ~ProcesorINTEL()
    {
        cout << "~ProcesorINTEL()\n";
    }
};
class Cooler
{
public:
    virtual void cool() =0;
    virtual ~Cooler()
    {
        cout << "~Cooler()\n";
    }
};
class CoolerAMD: public Cooler
{
public:
    void cool()
    {
        cout << "Cool()AMD\n";
    }
    ~CoolerAMD()
    {
        cout << "CoolAMD()\n";
    }
};
class CoolerINTEL: public Cooler
{
public:
    void cool()
    {
        cout << "Cool()INTEL\n";
    }
    ~CoolerINTEL()
    {
        cout << "CoolAMD()\n";
    }
};
class AbstractSemiconductorFactory
{
public:
    virtual Procesor * createProcesor() = 0;
    virtual Cooler * createCooler() =0;
    virtual ~AbstractSemiconductorFactory()
    {}
};
class AMDFactory: public AbstractSemiconductorFactory
{
public:
    Procesor * createProcesor()
    {
        return new ProcesorAMD();
    }
    Cooler * createCooler()
    {
        return new CoolerAMD();
    }
    ~AMDFactory()
    {
    }
};
class INTELFactory: public AbstractSemiconductorFactory
{
public:
    Procesor * createProcesor()
    {
        return new ProcesorINTEL();
    }
    Cooler * createCooler()
    {
        return new CoolerINTEL();
    }
    ~INTELFactory()
    {
    }
};
class Computer
{
    string mName;
    Procesor * mProcesor;
    Cooler * mCooler;
public:
    Computer(string nazwa, AbstractSemiconductorFactory & factory)
:mName(nazwa)
    {
        mProcesor = factory.createProcesor();
        mCooler = factory.createCooler();
    }
    ~Computer()
    {
        delete mProcesor;
        delete mCooler;
    }
    void run()
    {
        cout << "Nazwa:" << mName << endl;
        mProcesor->process();
        mCooler->cool();
    }
};
int main()
{
    AMDFactory amdFactory;
    INTELFactory intelFactory;
    Computer intelPC("PC1", intelFactory);
    Computer amdPC("PC2", amdFactory);
    intelPC.run();
    amdPC.run();
    return 0;
}
