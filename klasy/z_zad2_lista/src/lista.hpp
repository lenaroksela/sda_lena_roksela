/*
 * lista.hpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef LISTA_HPP_
#define LISTA_HPP_
#include <string>
#include <iostream>

using namespace std;


class Lista
{
private:
	class Wezel
	{
	public:
		string mDane;
		Wezel * mNastepny;

		Wezel(string dane)
		{
			mDane=dane;
			mNastepny=NULL;
		}
		Wezel()
		:mDane()
		,mNastepny()
		{

		}
	};

private:
	Wezel * mPierwszy;
	Wezel * mOstatni;

	int mRozmiar;


public:

	void wstaw(string dane);
	void usun();
	void usun(int n);
	void pobierz(int n);
	void wyczysc();
	void wyswietl();
	bool czyPusta();

	Lista();


};




#endif /* LISTA_HPP_ */
