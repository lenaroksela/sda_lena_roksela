/*
 * kataCalcTesty.cpp
 *
 *  Created on: 20.04.2017
 *      Author: RENT
 */

#include "gtest/gtest.h"
#include "kataCalc.hpp"

TEST(CalcStringTest, EmptyString)
{
	CalcString x;
	EXPECT_EQ(0, x.Add(""));

}

TEST(CalcStringTest, OneString)
{
	CalcString x;
	EXPECT_EQ(1, x.Add("1"));

}

TEST(CalcStringTest, TwoString)
{
	CalcString x;
	EXPECT_EQ(3, x.Add("1,2"));
	EXPECT_EQ(33, x.Add("12,21"));
	EXPECT_EQ(153, x.Add("141,12"));

}

TEST(CalcStringTest, UnknowTest)
{
	CalcString x;
	EXPECT_EQ(3, x.Add("1,2,3"));
	EXPECT_EQ(12, x.Add("1,2,4,5"));
	EXPECT_EQ(156, x.Add("141,12,1,1,1"));

}


int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
