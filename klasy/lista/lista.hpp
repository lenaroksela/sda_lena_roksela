/*
 * lista.hpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef LISTA_HPP_
#define LISTA_HPP_
#include <string>
#include <iostream>

using namespace std;
//Lista struktura danych z sekwencyjnie ulozonymi danymi (w odroznieniu od tablic)
//ma specjalny wskaznik wskazujacy na poczatek listy
//do wskazywania konca jest NULL albo wskaznik na koniec (tak jak wskaznik na poczatek)

//Dodaawanie do listy
/*
 na poczatek albo na koniec
 na poczatek(zobaczyc na co wskazuje glowa, skopiowac wartosc, zmienic glowe)


 */


class Lista
{
private:
	class Wezel
	//posiada wartosc
	//posiada wskaznik na kolejny elemnt(czyli nakolejny wezel)
	{
	public:
		int mDane;
		Wezel* mNastepny;

		//konstruktor jednoargumentowy przekazujacy wartosc
		//przesuwa od razu wskaznik na nastepny na NULL
		Wezel(int dane)
		{
			mDane=dane;
			mNastepny=NULL;
		}
	};

private:
	Wezel * mPierwszy;
	Wezel * mOstatni;




public:

	Lista();
	void wyswietl();
	void dodajPoczatek(int dane);
	void dodajKoniec(int dane);
	int pobierz (int indeks);
	bool czyPusta();

	void wyczysc();
	void usun (int indeks);
	int znajdzPozycje(int dane);

};



#endif /* LISTA_HPP_ */
