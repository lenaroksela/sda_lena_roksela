/*
 * lista.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */
#include "lista.hpp"

Lista::Lista()

{
	mPierwszy = 0;
	mOstatni = 0;

}

void Lista::wyswietl()
{
	Wezel* tmp = mPierwszy;
	std::cout << "( ";
	while (tmp != NULL)
	{
		std::cout << "[" << tmp->mDane << "] ";
		tmp = tmp->mNastepny;
	}
	std::cout << " )" << std::endl;
}

void Lista::dodajPoczatek(int dane)
{
	if (mPierwszy != 0)
	{
		Wezel* tmp = new Wezel(dane);
		tmp->mNastepny = mPierwszy;
		mPierwszy = tmp;

	}
	else
	{
		mPierwszy = new Wezel(dane);
		mOstatni = mPierwszy;
	}
}

void Lista::dodajKoniec(int dane)
{
	if (mPierwszy != 0)
	{
		Wezel* tmp = new Wezel(dane);
		mOstatni->mNastepny = tmp;
		mOstatni = tmp;

	}
	else
	{
		mPierwszy = new Wezel(dane);
		mOstatni = mPierwszy;
	}
}

int Lista::pobierz(int indeks) //funkcja zwracajaca wartosc elemetnu pod pozycja n
{
//	if (indeks == 1)
//	{
//		return mPierwszy->mDane;
//		//cout<<"Pierwszy "<<mPierwszy->mDane<<endl;
//	}
//	else
//	{
//		Wezel * nowy;
//		nowy = mPierwszy;
//		while (--indeks)
//		{
//			nowy = nowy->mNastepny;
//		}
//		return nowy->mDane;
//	}
//
//	return -1; //ma zwracac wartosc wezla

	Wezel *tmp = mPierwszy;
	int licznik=0;
	while(tmp!=0)
	{
		if(licznik==indeks)
		{
		licznik++;
		return tmp->mDane;
		}
		else
		{
			tmp=tmp->mNastepny;
		}
	}
	return 0;

}

int Lista::znajdzPozycje(int dane)
{
	return 0;
}

bool Lista::czyPusta() // funkcja zwracajaca true jesli lista jest pusta
{
	if (mPierwszy == 0)
	{
		return true;
	}
	return false; //ma zwracac faktyczna wartosc
}

void Lista::wyczysc() //funkcja usuwajace wszystkie Wezly z listy
{
//	if(mPierwszy==0)
//	{
//		cout<<"Lista Pusta\n";
//	}
//	while(mPierwszy != 0)
//	{
//		Wezel * tmp;
//		tmp=mPierwszy;
//		mPierwszy=mPierwszy->mNastepny;
//		delete tmp;}

	Wezel *tmp=0;

	while(tmp != NULL)
	{
		mPierwszy=tmp->mNastepny;
		delete tmp;
		tmp=mPierwszy;
	}
	delete mOstatni;
	mPierwszy=0;
	mOstatni=0;

}
void Lista::usun(int indeks) //funkcja ususwajace element na podanej pozycji od head
{
	Wezel * tmp;
	int licznik =0;
	tmp = mPierwszy;
	while(tmp!=0)
	{
		licznik++;
		if(licznik == (indeks))
		{
			tmp=mPierwszy;
			mPierwszy=mPierwszy->mNastepny;

		}delete tmp;
	}
}

