/*
 * Kolo.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */
#include "Figura.hpp"

#ifndef KOLO_HPP_
#define KOLO_HPP_

class Kolo : public Figura
{
private:
	float mPromien;
	const float pi=3.1416;
public:
	Kolo(float promien);
	float pole();

};



#endif /* KOLO_HPP_ */
