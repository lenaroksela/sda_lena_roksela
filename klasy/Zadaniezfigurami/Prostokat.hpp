/*
 * Prostokat.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */
#include "Figura.hpp"

#ifndef PROSTOKAT_HPP_
#define PROSTOKAT_HPP_

class Prostokat :public Figura
{
private:
float mA;
float mB;

public:
Prostokat(float a, float b);
float pole();
};

#endif /* PROSTOKAT_HPP_ */
