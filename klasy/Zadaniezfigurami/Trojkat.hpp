/*
 * Trojkat.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#ifndef TROJKAT_HPP_
#define TROJKAT_HPP_
#include "Figura.hpp"


class Trojkat : public Figura
{
private:
float mH;
float mA;

public:

Trojkat(float h, float a);
float pole();


};

#endif /* TROJKAT_HPP_ */
