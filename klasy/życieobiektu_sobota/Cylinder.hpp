/*
 * Cylinder.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef CYLINDER_HPP_
#define CYLINDER_HPP_

#include "Circle.hpp"

class Cylinder : public Circle
{
private:
	string mNazwa;
	int mPromien;
	const float mPI= 3.14;
	float mObjetosc;
	float mWysokosc;

public:
	float Pole();
	void Objetosc();
	string getNazwa();

	Cylinder()
		{
			cout<<"Konstruktor cylinder\n";
		}
		~Cylinder()
		{
			cout<<"~Destruktor cylinder\n";
		}

};



#endif /* CYLINDER_HPP_ */
