/*
 * Shape.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef SHAPE_HPP_
#define SHAPE_HPP_
#include <iostream>
#include <string>

using namespace std;


class Shape
{
public:
	string mNazwa;
	virtual float Pole() = 0;
	virtual string getNazwa() =0;

	Shape()
	{
		std::cout<<"konstruktor Shape\n";
	}

	virtual ~Shape()
	{
		cout<<"~Destruktor shape\n";
	}

};



#endif /* SHAPE_HPP_ */
