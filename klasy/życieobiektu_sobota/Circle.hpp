/*
 * Circle.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef CIRCLE_HPP_
#define CIRCLE_HPP_

#include "Shape.hpp"

#include <iostream>
#include <string>

using namespace std;

class Circle : public Shape
{
private:
	string mNazwa;
	int mPromien;
	const float mPI= 3.14;

public:
	float Pole();
	string getNazwa();
	Circle()
	{
		cout<<"Konstruktor circle\n";
	}
	~Circle()
	{
		cout<<"~Destruktor Circle\n";
	}

};



#endif /* CIRCLE_HPP_ */
