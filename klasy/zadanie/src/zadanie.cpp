//============================================================================
// Name        : zadanie.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

class Poczatek
{
protected:
	int x;
public:
	Poczatek() :
			x()
	{
		cout << "Poczatek: konstruktor" << endl;
	}
	~Poczatek()
	{
		cout << "Poczatek: destruktor" << endl;
	}

	void Czesc()
	{
		cout << "Klasa: Poczatek" << endl;
	}

	void Zmien()
		{
				x = 1;
		}

	void Pokaz()
	{
			cout << "Poczatek X ="<< x << endl;
	}

};


class Srodek: protected Poczatek
{
protected:
	int x;
public:
	Srodek() :
			x(10)
	{
		cout << "Srodek: konstruktor" << endl;
	}
	~Srodek()
	{
		cout << "Srodek: destruktor" << endl;
	}

	void Czesc()
	{
		cout << "Srodek" << endl;
	}

	void Zmien()
			{
					x = 12;
					Poczatek::x=11;
			}

		void Pokaz()
		{
				cout << "Klasa: Srodek X ="<< x << endl;
				cout << "Srodek::Poczatek X ="<< Poczatek::x << endl;

		}
};


class Koniec: protected Srodek
{
protected:
	int x;
public:
	Koniec() :
			x()
	{
		cout << "Koniec: konstruktor" << endl;
	}
	~Koniec()
	{
		cout << "Koniec: destruktor" << endl;
	}

	void Czesc()
	{
		cout << "Koniec" << endl;
	}
	void Zmien()
			{
					x = 4;
					Srodek::x=5;
					Srodek::Poczatek::x=6;
			}

		void Pokaz()
		{
				cout << "Klasa: Koniec X ="<< x << endl;
				cout << "Koniec::Srodek X ="<< Srodek::x << endl;
				cout << "Koniec::Srodek::Poczatek X ="<< Srodek::Poczatek::x << endl;

		}
};


int main()
{
	Poczatek p;
	Srodek s;
	Koniec k;

	p.Pokaz();
	p.Zmien();
	p.Pokaz();

	s.Pokaz();
	s.Zmien();
	s.Pokaz();

	k.Zmien();
	k.Pokaz();

	return 0;
}
